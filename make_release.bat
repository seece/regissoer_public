@echo on
setlocal

IF [%1] EQU [] goto noparam

set project=%1
set "dlls=sdk\bass24\bass.dll sdk\SDL2-2.0.3\lib\x86\SDL2.dll sdk\msvcr120.dll"

set archivename=peisik_demo.zip
set tempdir=releasepack
set extradir=%project%\meta\extrafiles
set assetdir=%project%\data
set binarypath=Release\demo.exe
REM the final binary name
set binaryname=peisikdemo.exe
set releasedir=releasepack

if not exist %binarypath% goto nobinary

goto compress
:compress_done
echo Demo packaged to %tempdir%/
exit /B

:compress

rmdir /Q /S %tempdir%
if not exist %tempdir% mkdir %tempdir%
if not exist %extradir% mkdir %extradir%
if not exist %tempdir%\data mkdir %tempdir%\data
xcopy %extradir% %tempdir% /Y /E
xcopy %assetdir% %tempdir%\data /Y /E
copy %binarypath% %tempdir%\%binaryname%

REM copy all the required dlls
for %%i in (%dlls%) do (
    echo Copying %%i...
	copy %%i %tempdir% /Y 
  
 )

if not exist %releasedir% mkdir %releasedir%

goto compress_done

exit /B

pause
exit /B

:nobinary
echo No release binary found.
exit /B

:noparam
echo Usage: make_release.bat ^<PROJECTNAME^>
exit /B