@echo off
setlocal
pushd %~dp0
IF NOT EXIST sdk\NUL GOTO nosdk
:continue

set project=%1
IF [%1] EQU [] set project=demo 

echo project: %project%
set "dlls=sdk\bass24\bass.dll sdk\SDL2-2.0.3\lib\x86\SDL2.dll"
set "targets=%project% Debug Release DebugMemory DebugNoGL"

for %%t in (%targets%) do (
	  mkdir %%t 2> NUL
)

(
 for %%i in (%dlls%) do (
  echo Copying %%i...
  for %%t in (%targets%) do (
	REM Copy the DLL only if it doesn't already exist.
	if not exist %%t\%%~nxi copy %%i %%t 
	  
   )
  
 )
)

popd
exit /B

:nosdk
REM echo Please download sdk.zip and extract it as sdk/ directory first.
REM echo.
REM echo URL: https://bitbucket.org/seece/regissoer/downloads/sdk.zip
echo Downloading SDK first...
call get_sdk.bat
goto continue
exit /B


:noparam
echo Usage: copy_dlls.bat ^<PROJECTNAME^>
exit /B