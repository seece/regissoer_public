@echo off
tasklist /FI "IMAGENAME eq sync_editor.exe" 2>NUL | findstr .exe
if "%ERRORLEVEL%" == "1" goto launch_rocket
exit /B
:launch_rocket
echo launching gnu rocket
start "" %~dp0\sdk\rocket-0.9\bin\sync_editor.exe