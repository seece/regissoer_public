@echo off
setlocal
pushd %~dp0
if exist sdk goto good_sdk

set sdk_url=https://dl.dropboxusercontent.com/u/5753422/demo/sdk/sdk12102014.zip
set temporary=buildtools\temp
set tools=buildtools\tools
set wgetexe=buildtools\tools\wget.exe
set unzipexe=buildtools\tools\unzip.exe
set sdkzip="%temporary%\sdk.zip"

echo Downloading build tools...
call buildtools\download_tools.bat
echo Downloading %sdk_url%
mkdir %temporary% 2> NUL

"%wgetexe%" %sdk_url% -O %sdkzip% 

REM We assume the sdk zip archive contains a single folder, sdk, under which all content lies.
%unzipexe% %sdkzip%
popd
exit /B

:error
echo SDK install was not succesful.
exit /B

:good_sdk
echo SDK directory: OK!
popd
exit /B