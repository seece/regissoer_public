import bpy
import mathutils
from mathutils import Vector, Quaternion
import os


def export_obj(obj, path):
    bpy.ops.object.select_all(action='DESELECT')
    obj.select = True
    bpy.ops.export_scene.obj(filepath=str((path)), 
        use_selection=True, use_normals=True, use_materials=False, 
        use_triangles=True, axis_forward='-Z', axis_up="Y")

#print (repr(meshes))
# The following transformation should be done first
# x,y,z => x,z,-y
def stringify(val):
    if type(val) is Vector:
        if len(val) == 3:
            return "%.8f %.8f %.8f" % (val.x, val.y, val.z)
        if len(val) == 4:
            return "%.8f %.8f %.8f %.8f" % (val.x, val.y, val.z, val.w)
        
    if type(val) is Quaternion:
        return "%.8f %.8f %.8f %.8f" % (val.w, val.x, val.y, val.z)
    
    if type(val) is str:
        return '"' + val + '"'
    
    if type(val) is float:
        return "%.8f" % (val)
    
    if type(val) is tuple:
        return " ".join(map(str, list(val)))
        
    return str(val)

def build_sken_item(values):
    out = "{\n"
    
    for key, value in values.items():
        
        out += "%s %s\n" % (str(key), stringify(value))
        
    out += "}"
    return out
        
def swizzle_vec3(inp):
    v = Vector()
    v.x = inp.x
    v.y = inp.z
    v.z = -inp.y
    return v

        
def clean_vec3(inp):
    v = Vector()
    v.x = inp.x
    v.y = inp.y
    v.z = inp.z
    return v

def swizzle_quat(inp):
    q = Quaternion()
    q.x = inp.x
    q.y = inp.z
    q.z = -inp.y
    q.w = inp.w
    return q


def parse_object_name(name):
    """ _aaaba.001 --> _aaaba """
    if len(name.split(".")) > 1:
        return ".".join(name.split(".")[:-1])
    
    return name
    
def is_mesh_name_exportable(name):
    if name[0] == "_":
        return False
    return True
    
def build_meshname(scene, mesh, extension = True): 
    if mesh.name[0] == "_":
        name = parse_object_name(mesh.name)
        if not extension:
           return name
        return name + ".obj"
    
    if not extension:
        return "%s_%s" % (scene.name, mesh.name)
    return "%s_%s.obj" % (scene.name, mesh.name)

def export_meshes(scene, meshes, dir):
    for mesh in meshes:
        name = build_meshname(scene, mesh)
        
        #if os.path.isfile("%s.obj" % (mesh.name)) and namechars[0] == "_":
        #    continue
        
        filepath = dir + name
        
        # meshes named '_something.obj' are not automatically exported
        if os.path.isfile(filepath) and not is_mesh_name_exportable(name):
            continue
        
        print ("    Saving to %s" % (filepath))
        export_obj(mesh, filepath)
        

# dir should be the path to asset dir, e.g. demo/data/. NOT the mesh folder!
def export_scene(scene, dir):
    
    filename = dir + "\\scenes\\" + scene.name + ".sken"
    print ("Exporting scene to %s" % (filename))
    fp = open(filename, "w")
    fp.write("#SKEN1\n")
    fp.write("#%s\n\n" % (scene.name))
   
    objects = scene.objects
    cameras = [o for o in objects if o.type == "CAMERA"]
    meshes = [o for o in objects if o.type == "MESH"]
    lamps = [o for o in objects if o.type == "LAMP"]
    empties = [o for o in objects if o.type == "EMPTY"]
    
    print("Scene objects:")
    for obj in objects:
        print ("    %s %s" % (obj.type, obj.name))
        
    for mesh in meshes:
        if mesh.hide_render:
            continue
        
        oldmode = mesh.rotation_mode
        mesh.rotation_mode = "XYZ" 
        mat = "diffuse"
        
        if mesh.active_material:
            mat = mesh.active_material.name
        
        fields = {
            "name" :        mesh.name,
            "mesh" :        build_meshname(scene, mesh, False),
            "position" :    swizzle_vec3(mesh.location),
            "orientation" : swizzle_vec3(mesh.rotation_euler),
            "scale" :       mesh.scale, # TODO swizzle scale too, somehow
            "material" :    mat
        }
        
        inst = "%s\n%s\n\n" % ("instance", build_sken_item(fields))
        mesh.rotation_mode = oldmode
        fp.write(inst)
        
    for camera in cameras:
        
        fields = {
            "name" :        camera.name,
            "position" :    swizzle_vec3(camera.location),
            "orientation" : clean_vec3(camera.rotation_euler),
            "clip_start" :  camera.data.clip_start,
            "clip_end" :    camera.data.clip_end,
            "angle_y" :       camera.data.angle_y,
            "type" :        camera.data.type
        }

        cam = "%s\n%s\n\n" % ("camera", build_sken_item(fields))
        fp.write(cam)
        
    for lamp in lamps:
        if not lamp.data.type == "SPOT":
            continue
         
        col = lamp.data.color
        shadow = 0
        
        if not lamp.data.shadow_method == "NOSHADOW":
            shadow = 1 
            
        enabled = 1
        if lamp.hide_render:
            enabled = 0
            
        direction = Vector((0.0, 0.0, -1.0)) * lamp.matrix_local.to_3x3().inverted()
        
        fields = {
            "name" :        lamp.name,
            "position" :    swizzle_vec3(lamp.location),
            "orientation" : clean_vec3(lamp.rotation_euler),
            "dir" :         swizzle_vec3(direction),
            "color" :       (col[0], col[1], col[2]),
            "energy" :      lamp.data.energy,
            "falloff":      lamp.data.distance,
            "shadow" :      shadow,
            "spot_size" :   lamp.data.spot_size,
            "enabled" :     enabled 
        }    
        
        fp.write("%s\n%s\n\n" % ("light", build_sken_item(fields)))
        
    fp.close()
    
    export_meshes(scene, meshes, dir + "meshes\\")
    #export_materials(materials, dir)

#print (build_sken_item(test))
#print (str(cameras[0].location) + " => " + str(swizzle_vec3(cameras[0].location)))
#export_obj(meshes[0], "E:\\temp\\" + meshes[0].name + ".obj")

print("saving")
# Remember the trailing slash/backslash
export_scene(bpy.context.scene, "C:\\dev\\regs2\\demo\\data\\")