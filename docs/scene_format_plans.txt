# REGISS�R2 SCENE FORMAT
version 1

http://blender.stackexchange.com/questions/5382/export-multiple-objects-to-obj

## Comments

Lines beginning with # are treated as comments

## Instances
instance_name
model 
position
orientation
scale
material

## Cameras
camera_name
position
rotation
(camera needs no scale)
field of view in degrees (vertical or horizontal?)
clipping distance
(depth of field focus point)


## Lamps TODO!
Point, spot and directional lights
	-> shadow maps only in spot and directional lights
	
## Nodes
Just empty nodes for effect placement and such.
location
orientation
scale
visibility

## Camera paths
Not yet

## Example file

	#SKEN1
	# the first line is a version identifier

	instance
	{
	name:"kuubio"
	mesh:"kube.obj" 
	position:[0.00002, 0.9999991, 0.0]
	orientation:[0.22121, 0.02312, 0.021, 0.023131]
	scale:[1.00000, 1.00000, 1.00000]
	material:"glass"
	}

	camera
	{
	name:"maincam"
	position:[0.00002, 0.9999991, 0.0]
	orientation:[0.22121, 0.02312, 0.021, 0.023131]
	scale:[1.0, 1.0, 1.0]
	fov:60.0
	fmin:0.1
	fmax:100.0
	}

## Loaderi
main.tcl scans the scene folder for data files
each datafile is parsed by a tcl script that creates a Scene object
	the scene object is populated with the parsed instances and other entities
these scene objects are placed in a c++ container (just a vector?) 
this container is passed to the DemoInterface compatible pointer
	.loadScenes(std::vector<Scene>& scenelist)

the scenes can be reloaded from the script
	this fires a .reloadScenes(std::vector<Scene>& scenelist)
	the old scenelist is hopefully garbage collected
		try to force the deletion from tcl side


## Exported Blender data

### Cameras
name
clip_start
clip_end
c.type PERSP or ORTHO (or PANO)  
angle_y (vertical FOV in radians)
location
rotation_quaternion (rotation_mode needs to be changed before this is written to)

### Objects
is_visible 
location
rotation_quaternion