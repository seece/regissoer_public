REGISS�R2 Architecture
======================

Writing an effect should be easy, and it should not mean writing a custom class.
A class definition means writing everything twice for the header and the source file, and this is not something we want.

All assets are loaded automatically on program startup and the developer should be able to reload them on the fly. All assets are stored in maps where keys are file names without extensions. These maps should be accessable easily from anywhere in the demo project.

Parameter automation and sync is done using GNU Rocket. The interpolated parameters are handed in to the demo render-function as a string -> float map. All parameters beginning with p_ are passed to the shaders as float uniforms, if declared in shader.

All effects render to offscreen buffers that are passed to a single shading pass that calculates lighting according to current parameters. The parameters include at least

- the rocket parameter map
- all currently active lights
- separate shadow buffers for all shadow casting lights
	 
The shadow buffers contents depend on the rendering effect, it can be empty if there are no shadow casting lights being active in the scene.

Effect functions
----------------

An effect must follow these definitions

	struct EffectInput;
	struct EffectOutput;

	typedef void EffectFunc_t(ParameterMap& params, EffectInput in, EffectOutput* output);

See `effect.h` for details.

If shadow maps are desired, then the whole effect function needs to be rendered multiple times. If the effect renders a scene, it should assign a pointer to it in output.scene so the lighting pass can work correctly.



Rendering loop
--------------

Each effect function is given an `int` identifier, which used to determine the scene to render according to the `scene` parameter passed in from GNU Rocket. Below is a pseudo code of the rendering loop.

	FBO framebuffer
	FBO shadedbuffer

	scenes[0] = grey_cubes_effect
	scenes[1] = teal_orange_fractal_effect

	while (demo_running):
		params = get_updated_params()
		EffectInput input
		EffectOutput output

		current_effect_func = scenes[params["scene"]]
		input.framebuffer = framebuffer
		input.light_index = -1

		current_effect_func(params, input, &output)

		if output.lights != nullptr:
			foreach light in get_shadow_casting_lights(output.lights):
				input.framebuffer = get_shadow_fbo(light.index)	
				input.light_index = light.index
				input.framebuffer.prepareViewport()
				current_effect_func(params, input, &output)

		apply_shading(input, output.lights, &shadedbuffer)
		apply_post_process(shadedbuffer)

Common functionality between effects
------------------------------------

Since each effect is just a function, many effects can exist in the same namespace if needed, and also easily share some helper functions.

Here is an example helper function that picks the correct light and sets the scenes camera to the correct light.

	void set_camera_to_light(int light_index, Scene& scene) 
	{
		if (light_index == -1)
			return;

		Light& light = scene.lights[light_index];
		camera.setLightView(light)
	}
		

And one would use this helper function like this

	void megaefu(ParameterMap& params, EffectInput input, EffectOutput* output)
	{
		scene = scenemap["trainstation"];
		set_camera_to_light(input.light_index, scene);

		// Render the scene contents with the set camera.
		// All scene objects have a material applied to them that specifies
		// the shader and parameters.
		scene.render();

		// Rendering procedural geometry requires a bit more work.
		MatrixStack stack;
		stack.loadIdentity();
		stack.translate(0.0f, 0.0f, -4.0f);

		// Use a custom transformation, but keep the set camera direction
		// and light information.
		Pipeline p;
		p.modelmatrix = *stack.top();
		p.setShader(input.shader);
		p.updateUniforms(scene.camera, scene.lights);

		// Render the custom mesh.
		p.render(input.meshes["rk-62"]);
	}



