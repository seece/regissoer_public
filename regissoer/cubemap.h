#pragma once

#include "GL/gleks.h"
#include "system/filesystem.h"
#include "Vertex.h"
#include "texture.h"

class Cubemap {
	public:
	Cubemap(std::string path, bool flip_y = true, GLint internalformat = GL_SRGB8_ALPHA8);	
	~Cubemap();
	protected:
};