#pragma once
/* A material information structure. 
 * Used in Entity class. Pipeline reads these values and binds them as shader 
 * uniforms for rendering.
 */

#include <glm/vec3.hpp>
#include "Shader.h"
#include "Texture.h"

class Pipeline;

class Material {
	public:
	Material();
	Material(std::string name, Shader* shader, Texture* texture, Texture* emitmap, float exponent, float multiplier, glm::vec3 specularColor);
	~Material();
	void applyUniforms(Pipeline& p);

	Shader* shader;
	Texture* texture;
	Texture* emitmap;
	float exponent;
	glm::vec3 specularColor;
	float multiplier;
	bool fullbright;
	std::string name;
};