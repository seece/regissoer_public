#include <cassert>
#include <string>
#include "GL/glnames.h"
#include "system/filesystem.h"
#include "shader.h"
#include "demoplayer.h"

using eks::logger;

ShaderPreprocessor* Shader::processor = nullptr;
ShaderCache Shader::shaderCache;

#ifdef _DEBUG
static const bool useShaderCache = true;
#else
static const bool useShaderCache = false;
#endif

Shader::Shader(const std::string& filepath) :
	filepath(filepath),
	file(filepath),
	program(nullptr)
{
	if (!processor) {
		processor = new ShaderPreprocessor();
	}

	program = createProgram(file);

	#ifdef _RELEASE
	if (!program->isValid()) {
		LOG_ERROR("Couldn't compile shader '%s'", filepath.c_str());
	}
	#endif

	updateUniformSlots();
}

std::string Shader::getFilepath()
{
	return filepath;
}

std::string Shader::getBasename()
{
	return file.getBasename();	
}

void Shader::reload(bool forceReload)
{
	if (!forceReload && !file.beenModified()) {
		LOG_INFO("Reload: %s not modified, skipping", file.getFilePath().c_str());
		return;
	}

	file.reload();
	ShaderProgram* loaded = createProgram(file);

	if (!loaded->isValid()) {
		delete loaded;
		LOG_INFO("Reload: %s compilation failed", file.getFilePath().c_str());
		return;
	}

	if (program != nullptr) {
		delete program;
	}

	program = loaded;
	uniformLocations.clear();
	blockLocations.clear();

	updateUniformSlots();

	LOG_INFO("Reload: %s last modified: %llu", file.getFilePath().c_str(), file.getLastModified());
}

Shader::~Shader()
{
}

ShaderProgram* Shader::loadCachedProgramBinary(fsys::File& shaderfile, const std::string& expandedSource)
{
	ShaderProgram* prog = shaderCache.load(file.getBasename(), expandedSource);

	if (!prog)
		return nullptr;

	if (!prog->isValid()) {
		LOG_INFO("Couldn't upload cached shader, compiling instead.");
		return nullptr;
	}

	return prog;

}

ShaderProgram* Shader::createProgram(fsys::File& shaderfile)
{
	assert(processor); // Preprocessor is created in constructor if needed.
	
	std::string expandedSource;
	bool saveToCache = false;
	ShaderProgram* shader;

	processor->process(file.getFilePath(), expandedSource); 

	if (!useShaderCache) {
		return new ShaderProgram(expandedSource.c_str());
	}

	shader = loadCachedProgramBinary(shaderfile, expandedSource);

	if (shader) {
		LOG_INFO("Found shader %s from cache.", shaderfile.getFilePath().c_str());
		return shader;
	}

	shader = new ShaderProgram(expandedSource.c_str());
	GLenum format = 0;
	std::vector<unsigned char> data;

	if (!shader->isValid()) {
		LOG_ERROR("Couldn't compile shader %s", shaderfile.getFilePath().c_str());
	}

	if (shader->getProgramBinary(&format, data)) {
		shaderCache.saveShader(data, shaderfile.getBasename(), expandedSource);
	} else {
		LOG_WARN("Couldn't fetch shader bytes for %s binary cache.", shaderfile.getFilePath().c_str());
	}

	return shader;
}

void Shader::applyParameters(const ParameterMap& params) 
{
	if (!program)
		return;

	// The name must be of form p_variable to be uploaded as uniform.
	for (auto& param : params) {
		const char* name = param.first.c_str();

		if (strlen(name) < 3) 
			continue;

		// Must begin with p_
		if (!checkIfNameIsParameter(name))
			continue;

		int location = getUniformLocation(name);

		// Name does not exist, do not try to apply the uniform.
		if (location == -1) 
			continue;

		glUniform1f(location, param.second);
	}
}

GLint Shader::getUniformLocation(const std::string& name)
{
	if (uniformLocations.find(name) == uniformLocations.end()) {
		GLuint loc = program->getUniformLocation(name.c_str());
		uniformLocations[name] = loc;
		return loc;
	}

	return uniformLocations[name];
}

GLint Shader::getUniformBlockIndex(const std::string& name)
{

	if (blockLocations.find(name) == blockLocations.end()) {
		GLuint loc = program->getUniformBlockIndex(name.c_str());
		blockLocations[name] = loc;
		return loc;
	}

	return blockLocations[name];
}

void Shader::floatUniform(const std::string& name, float value)
{
	GLuint location = getUniformLocation(name);

	if (location == -1)
		return;

	glUniform1f(location, value);
}

void Shader::updateUniformSlots()
{
	GLuint programId = program->getId();
	GLint activeUniforms = 0;

	glGetProgramiv(programId, GL_ACTIVE_UNIFORMS, &activeUniforms);
	LOG_DEBUG("%d uniforms", activeUniforms);

	std::vector<GLchar> nameData(256);
	uniformSlots.clear();

	for (int i = 0 ; i < activeUniforms ; ++i) {
		GLint arraySize = 0;
		GLenum type = 0;
		GLsizei actualLength;

		glGetActiveUniform(programId, i, nameData.size(), &actualLength, &arraySize, &type, &nameData[0]);
		std::string name((char*)&nameData[0], actualLength); // actualLength does not include the null terminator character

		UniformSlot slot = {type, i, arraySize};
		uniformSlots[name] = slot;

		// Register matching uniforms as parameters.
		if (checkIfNameIsParameter(name)) 
			DemoPlayer::instance->addParameter(name);
		//LOG_DEBUG("\t %s\t%s", eks::gl::getDatatypeName(type), name.c_str());
	}
}

bool Shader::checkIfNameIsParameter(const char* name) {
	return checkIfNameIsParameter(std::string(name));
}

bool Shader::checkIfNameIsParameter(std::string name)
{
	if (name.length() < 2) {
		return false;
	}

	return name[0] == 'p' && name[1] == '_';
}