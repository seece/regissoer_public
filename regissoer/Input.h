/*
 * The Input class reads mouse and keyboard input. 
 * SDL needs to be initialized before instantiating this class.
 */
#pragma once
#include <vector>
#include <cstdint>
#include <SDL.h>

class Input {
	public:
	struct MousePosition {
		int x;
		int y;
	};

	struct MouseState {
		MousePosition pos;
		Uint32 state;
	};

	static const int MOUSE_LEFT = 1;
	static const int MOUSE_MID = 2;
	static const int MOUSE_RIGHT = 3;

	Input();
	~Input();

	bool keyDown(int key);
	bool keyHit(int key);
	bool mouseHit(int button);
	bool mouseDown(int button);
	MousePosition mousePos();
	void processEvents();
	const std::vector<char>& getTextInputChars() const;
	int getMouseWheelSpeed() const;

	private:
	void initKeyboard();
	bool checkKeyValid(int key);
	void saveKeyboardState();

	int keyCount;
	uint8_t* oldKeyboardState;
	const uint8_t* keyboardState;
	int mouseWheel;
	MouseState mouse;
	MouseState oldMouse;
	std::vector<char> textInputChars; // List of text input characters reported by SDL during the last frame.
};