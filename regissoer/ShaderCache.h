#pragma once

#include <string>
#include <unordered_map>
#include <unordered_set>

class ShaderProgram;

class ShaderCache {
public:
	ShaderCache();
	~ShaderCache();

	// Reads the file names from cacheDir. Creates the cache directory if necessary.
	void initialize();
	void purge();
	// Returns nullptr if a cached shader is not found.
	ShaderProgram* load(const std::string& filename, const std::string& source);
	void saveShader(const std::vector<unsigned char>& data, const std::string& filename, const std::string& source);
	// Writes the cached file name of a shader to out_cachePath. Returns false if no file with that name is found from the cache.
	bool getCachedName(const std::string& filename, const std::string& source, std::string* out_cachePath);

	const static std::string cacheDir;
	static std::string fullCacheDir;

protected:
	void updateCache();
	void calculateHash(const std::string& input, std::string* output);
	std::string makeFilename(const std::string& filename, const std::string& hash);
	std::unordered_set<std::string> filenames;
};