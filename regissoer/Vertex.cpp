#include "Vertex.h"

using eks::gl::Vertex;
using eks::gl::VertexFormat;

const VertexFormat eks::gl::FORMAT_INDEX_COLOR = {	
	{true, true, false, false},	// vertex and color buffers are active
	{true},	// indexed
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) sizeof(Vertex().pos)}, // color
		{0},	// normal
		{0}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_INDEX_NORMAL = {	
	{true, false, true, false},	// vertex and normal buffers
	{true},	
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{0},
		{3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, normal)},		
		{0}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_INDEX_UV = {	
	{true, false, false, true},	// vertex and uv buffers are active
	{true},	// indexed
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{0},	// color
		{0},	// normal
		{2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, uv)}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_INDEX_NORMAL_UV = {	
	{true, false, true, true},	// vertex, normal and uv buffers are active
	{true},	// indexed
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{0},	// color
		{3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, normal)},		
		{2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, uv)}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_INDEX_COLOR_UV = {	
	{true, true, false, true},	// vertex color, and uv buffers are active
	{true},	// indexed
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, color)}, // color
		{0},	// normal
		{2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, uv)}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_NORMAL = {	
	{true, false, true, false},	// vertex and normal buffers
	{false},	
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{0},
		{3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, normal)},		
		{0}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_COLOR = {	
	{true, true, false, false},		// vertex and color buffer is active
	{false},	// non-indexed
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, color)}, // color
		{0},	// normal
		{0}		// uv
	}
};

const VertexFormat eks::gl::FORMAT_NORMAL_UV = {	
	{true, false, true, true},	// vertex and normal buffers
	{false},	
	{
		{4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) 0},	// vertex 
		{0},
		{3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, normal)},		
		{2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*) offsetof(Vertex, uv)}		// uv
	}
};
