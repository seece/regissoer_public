#define GLM_SWIZZLE GLM_SWIZZLE_RGBA
#include <glm/glm.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/string_cast.hpp>
#include "light.h"
#include "math/eks_math.h"
#include "system/logger.h"

Light::Light(glm::vec3 pos, glm::vec3 intensity, glm::vec3 dir, float falloff, bool enabled) :
	pos(pos), intensity(intensity), dir(dir), falloff(falloff), enabled(enabled), castsShadows(false)
{
}

Light::Light() : 
	pos(glm::vec3(0.0f, 0.0f, 0.0f)),
	intensity(glm::vec3(0.0f, 0.0f, 0.0f)),
	dir(glm::vec3(0.0f, 0.0f, 1.0f)),
	falloff(0.5f),
	enabled(false),
	castsShadows(false)
{
}

Light::Light(glm::vec3 pos, glm::vec3 dir, glm::vec3 color, float energy, float falloff, float spot_size, bool shadow, bool enabled) :
	pos(pos), dir(dir), falloff(falloff), enabled(enabled), castsShadows(shadow)
{
	//dir = orientation * glm::vec3(0.0f, 0.0f, -1.0f);
	//dir = glm::mat3x3(glm::yawPitchRoll(eulerangles.z, (eulerangles.x - glm::half_pi<float>()), -eulerangles.y)) * glm::vec3(0.0f, 0.0f, -1.0f);
	//glm::eulerAngleXY(eulerangles.x, eulerangles
	//glm::mat4x4 rotation = 
	//dir = glm::mat3x3(glm::yawPitchRoll(eulerangles.z, (eulerangles.x - glm::half_pi<float>()), eulerangles.y)) * glm::vec3(0.0f, 0.0f, -1.0f);
	//dir = glm::mat3x3(glm::yawPitchRoll(eulerangles.z, (eulerangles.x - glm::half_pi<float>()), -eulerangles.y)) * glm::vec3(0.0f, 0.0f, -1.0f);
	intensity = color.rgb*energy;
	LOG_DEBUG("light argument: %f, %s\ndir: %s\nintensity: %s\n", falloff, glm::to_string(pos).c_str(), glm::to_string(dir).c_str(), 
		glm::to_string(intensity).c_str());
}

Light::~Light()
{

}

void Light::lookAt(glm::vec3 camera, glm::vec3 target)
{
	pos = camera;	
	dir = target - camera;
	dir = glm::normalize(dir);
}

void Light::setPosition(const glm::vec3& newpos) 
{
	pos = newpos;			
}

void Light::setScale(const glm::vec3& newscale)
{
	// a no op
}

void Light::setOrientation(const glm::quat& neworientation)
{
	// TODO implement orientation setting		
}

glm::vec3 Light::getPosition() const 
{
	return pos;		
}

glm::vec3 Light::getScale() const 
{
	return glm::vec3(1.0f);
}

glm::quat Light::getOrientation() const
{
	// TODO not implemented
	LOG_ERROR("Light orientation not implemented");
	return glm::quat();
};

glm::mat4& Light::getTransformation()  
{
	// TODO implement proper transformation calculation
	return transform;
}

/*
void Light::saveViewMatrix(eks::math::Mat4& output) const
{
	output = eks::math::lookAt(pos, pos + dir, glm::vec3(0.0f, 1.0f, 0.0f));
}
*/