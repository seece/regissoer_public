#pragma once
/* A wrapper for BASS play routines. */

#include <string>
#include "system/filesystem.h"

class Song {
	public:
	enum SongType {SONG_STREAM = 0, SONG_TRACKER = 1};

	Song(std::string path, double bpm, SongType songtype, int rows_per_beat = 8);
	virtual ~Song();
	void play();
	void pause();
	void stop();
	void toggle();
	double seek(double seconds);
	double getTime();
	double getLength();
	double getBPM();

	double getBeat();
	double getRow();
	void setRow(double row);

	private:
	fsys::File file;
	unsigned long stream; // HSTREAM is a DWORD that in turn is an unsigned long integer
	double bpm;
	int rpb; // rows per beat
	double latency;	// BASS audio latency in seconds
	SongType songtype;
};