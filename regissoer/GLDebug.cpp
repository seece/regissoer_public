#include <conio.h>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
#include "GL/gleks.h"
#include "Vertex.h"
#include "GLDebug.h"
#include "system/logger.h"
 
const char* gl_type_to_string (unsigned int type) {
  if (GL_FLOAT == type) {
    return "GL_FLOAT";
  }
  if (GL_FLOAT_VEC2 == type) {
    return "GL_FLOAT_VEC2";
  }
  if (GL_FLOAT_VEC3 == type) {
    return "GL_FLOAT_VEC3";
  }
  if (GL_FLOAT_VEC4 == type) {
    return "GL_FLOAT_VEC4";
  }
  if (GL_FLOAT_MAT2 == type) {
    return "GL_FLOAT_MAT2";
  }
  if (GL_FLOAT_MAT3 == type) {
    return "GL_FLOAT_MAT3";
  }
  if ( GL_FLOAT_MAT4 == type) {
    return "GL_FLOAT_MAT4";
  }
  if (GL_INT == type) {
    return "GL_INT";
  }
  if (GL_BOOL == type) {
    return "GL_BOOL";
  }
  if (GL_SAMPLER_2D == type) {
    return "GL_SAMPLER_2D";
  }
  if (GL_SAMPLER_3D == type) {
    return "GL_SAMPLER_3D";
  }
  if (GL_SAMPLER_CUBE == type) {
    return "GL_SAMPLER_CUBE";
  }
  if (GL_SAMPLER_2D_SHADOW == type) {
    return "GL_SAMPLER_2D_SHADOW";
  }
  return "OTHER";
}

// from http://www.opengl.org/discussion_boards/showthread.php/172950-Catalyst-10-12-and-std140-uniform-blocks?p=1212146&viewfull=1#post1212146
void eks::gl::print_uniform_block_info(GLuint prog, GLint block_index, std::string const &indent)
{
	// Fetch uniform block name:
	GLint name_length;
	glGetActiveUniformBlockiv(prog, block_index, GL_UNIFORM_BLOCK_NAME_LENGTH, &name_length);
	std::string block_name(name_length, 0);
	glGetActiveUniformBlockName(prog, block_index, name_length, NULL, &block_name[0]);
 
	// Fetch info on each active uniform:
	GLint active_uniforms = 0;
	glGetActiveUniformBlockiv(prog, block_index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &active_uniforms);
 
	std::vector<GLuint> uniform_indices(active_uniforms, 0);
	glGetActiveUniformBlockiv(prog, block_index, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, reinterpret_cast<GLint*>(&uniform_indices[0]));
 
	std::vector<GLint> name_lengths(uniform_indices.size(), 0);
	glGetActiveUniformsiv(prog, uniform_indices.size(), &uniform_indices[0], GL_UNIFORM_NAME_LENGTH, &name_lengths[0]);
 
	std::vector<GLint> offsets(uniform_indices.size(), 0);
	glGetActiveUniformsiv(prog, uniform_indices.size(), &uniform_indices[0], GL_UNIFORM_OFFSET, &offsets[0]);
 
	std::vector<GLint> types(uniform_indices.size(), 0);
	glGetActiveUniformsiv(prog, uniform_indices.size(), &uniform_indices[0], GL_UNIFORM_TYPE, &types[0]);
 
	std::vector<GLint> sizes(uniform_indices.size(), 0);
	glGetActiveUniformsiv(prog, uniform_indices.size(), &uniform_indices[0], GL_UNIFORM_SIZE, &sizes[0]);
 
	std::vector<GLint> strides(uniform_indices.size(), 0);
	glGetActiveUniformsiv(prog, uniform_indices.size(), &uniform_indices[0], GL_UNIFORM_ARRAY_STRIDE, &strides[0]);
 
	// Build a string detailing each uniform in the block:
	std::vector<std::string> uniform_details;
	uniform_details.reserve(uniform_indices.size());
	for(std::size_t i = 0; i < uniform_indices.size(); ++i)
	{
		GLuint const uniform_index = uniform_indices[i];
 
		std::string name(name_lengths[i], 0);
		glGetActiveUniformName(prog, uniform_index, name_lengths[i], NULL, &name[0]);
 
		std::ostringstream details;
		details << std::setfill('0') << std::setw(4) << offsets[i] << ": " << std::setfill(' ') << std::setw(5) << gl_type_to_string(types[i]) << " " << name;
 
		if(sizes[i] > 1)
		{
			details << "[" << sizes[i] << "]";
		}
 
		details << "\n";
		uniform_details.push_back(details.str());
	}
 
	// Sort uniform detail string alphabetically. (Since the detail strings 
	// start with the uniform's byte offset, this will order the uniforms in 
	// the order they are laid out in memory:
	std::sort(uniform_details.begin(), uniform_details.end());
 
	// Output details:
	std::cout << indent << "Uniform block \"" << block_name << "\":\n";
	for(auto detail = uniform_details.begin(); detail != uniform_details.end(); ++detail)
	{
		std::cout << indent << "  " << *detail;
	}
}


namespace {
	GLchar* getErrorString(GLenum errorCode) 
	{
		/* GL Errors */
		if (errorCode == GL_NO_ERROR) {
			return (GLchar*) "no error";
		}
		else if (errorCode == GL_INVALID_VALUE) {
			return (GLchar *) "invalid value";
		}
		else if (errorCode == GL_INVALID_ENUM) {
			return (GLchar *) "invalid enum";
		}
		else if (errorCode == GL_INVALID_OPERATION) {
			return (GLchar *) "invalid operation";
		}
		else if (errorCode == GL_STACK_OVERFLOW) {
			return (GLchar *) "stack overflow";
		}
		else if (errorCode == GL_STACK_UNDERFLOW) {
			return (GLchar *) "stack underflow";
		}
		else if (errorCode == GL_OUT_OF_MEMORY) {
			return (GLchar *) "out of memory";
		}

		return (GLchar*) "unknown";
	}
}

bool eks::gl::checkContextExists()
{
	HGLRC context = wglGetCurrentContext();
	return context != NULL;
}

// modified from http://openglbook.com/the-book/chapter-4-entering-the-third-dimension/
void eks::gl::assertGlError(const char * error_message, bool halt /* = true */)
{
	/*
	#ifdef _DEBUG
	const GLenum ErrorValue = glGetError();

	if (ErrorValue == GL_NO_ERROR)
		return;

	const char* APPEND_DETAIL_STRING = ": %s\n";
	const size_t APPEND_LENGTH = strlen(APPEND_DETAIL_STRING) + 1;
	const size_t message_length = strlen(error_message);
	char* display_message = (char*)malloc(message_length + APPEND_LENGTH);

	memcpy(display_message, error_message, message_length);
	memcpy(&display_message[message_length], APPEND_DETAIL_STRING, APPEND_LENGTH);

	logger->error(display_message, getErrorString(ErrorValue));

	free(display_message);

	if (halt) {
		_getch();
		exit(EXIT_FAILURE);
	}
	#endif
	*/
}

void formatDebugOutputAMD(char outStr[], size_t outStrSize, GLenum category, GLuint id,
                         GLenum severity, const char *msg)
{
    char categoryStr[32];
    const char *categoryFmt = "UNDEFINED(0x%04X)";
    switch(category)
    {
    case GL_DEBUG_CATEGORY_API_ERROR_AMD:          categoryFmt = "API_ERROR"; break;
    case GL_DEBUG_CATEGORY_WINDOW_SYSTEM_AMD:      categoryFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_CATEGORY_DEPRECATION_AMD:        categoryFmt = "DEPRECATION"; break;
    case GL_DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD: categoryFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_CATEGORY_PERFORMANCE_AMD:        categoryFmt = "PERFORMANCE"; break;
    case GL_DEBUG_CATEGORY_SHADER_COMPILER_AMD:    categoryFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_CATEGORY_APPLICATION_AMD:        categoryFmt = "APPLICATION"; break;
    case GL_DEBUG_CATEGORY_OTHER_AMD:              categoryFmt = "OTHER"; break;
    }
    _snprintf(categoryStr, 32, categoryFmt, category);

    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_AMD:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_AMD: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_AMD:    severityFmt = "LOW";    break;
    }
    _snprintf(severityStr, 32, severityFmt, severity);

    _snprintf(outStr, outStrSize, "OpenGL: %s [category=%s severity=%s id=%d]",
        msg, categoryStr, severityStr, id);
}

// see http://www.altdevblogaday.com/2011/06/23/improving-opengl-error-messages/
void CALLBACK debugCallbackAMD(GLuint id, GLenum category, GLenum severity, GLsizei length,
                     const GLchar *message, GLvoid *userParam)
{
    (void)length;
    FILE *outFile = (FILE*)userParam;
    char finalMsg[256];
    formatDebugOutputAMD(finalMsg, 256, category, id, severity, message);
	LOG_DEBUG(finalMsg);
}

void formatDebugOutputARB(char outStr[], size_t outStrSize, GLenum source, GLenum type,
    GLuint id, GLenum severity, const char *msg)
{
    char sourceStr[32];
    const char *sourceFmt = "UNDEFINED(0x%04X)";
    switch(source)

    {
    case GL_DEBUG_SOURCE_API_ARB:             sourceFmt = "API"; break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM_ARB:   sourceFmt = "WINDOW_SYSTEM"; break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER_ARB: sourceFmt = "SHADER_COMPILER"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY_ARB:     sourceFmt = "THIRD_PARTY"; break;
    case GL_DEBUG_SOURCE_APPLICATION_ARB:     sourceFmt = "APPLICATION"; break;
    case GL_DEBUG_SOURCE_OTHER_ARB:           sourceFmt = "OTHER"; break;
    }

    _snprintf(sourceStr, 32, sourceFmt, source);
 
    char typeStr[32];
    const char *typeFmt = "UNDEFINED(0x%04X)";
    switch(type)
    {

    case GL_DEBUG_TYPE_ERROR_ARB:               typeFmt = "ERROR"; break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_ARB: typeFmt = "DEPRECATED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_ARB:  typeFmt = "UNDEFINED_BEHAVIOR"; break;
    case GL_DEBUG_TYPE_PORTABILITY_ARB:         typeFmt = "PORTABILITY"; break;
    case GL_DEBUG_TYPE_PERFORMANCE_ARB:         typeFmt = "PERFORMANCE"; break;
    case GL_DEBUG_TYPE_OTHER_ARB:               typeFmt = "OTHER"; break;
    }
    _snprintf(typeStr, 32, typeFmt, type);

 
    char severityStr[32];
    const char *severityFmt = "UNDEFINED";
    switch(severity)
    {
    case GL_DEBUG_SEVERITY_HIGH_ARB:   severityFmt = "HIGH";   break;
    case GL_DEBUG_SEVERITY_MEDIUM_ARB: severityFmt = "MEDIUM"; break;
    case GL_DEBUG_SEVERITY_LOW_ARB:    severityFmt = "LOW"; break;
    }

    _snprintf(severityStr, 32, severityFmt, severity);
 
    _snprintf(outStr, outStrSize, "OpenGL: %s [source=%s type=%s severity=%s id=%d]",
        msg, sourceStr, typeStr, severityStr, id);
}

void CALLBACK debugCallbackARB(GLenum source, GLenum type, GLuint id, GLenum severity,
                     GLsizei length, const GLchar *message, GLvoid *userParam)
{
    (void)length;
    FILE *outFile = (FILE*)userParam;
    char finalMessage[256];
    formatDebugOutputARB(finalMessage, 256, source, type, id, severity, message);
	LOG_DEBUG(finalMessage);
}

#define DEBUG_CATEGORY_API_ERROR_AMD                    0x9149
#define DEBUG_CATEGORY_WINDOW_SYSTEM_AMD                0x914A
#define DEBUG_CATEGORY_DEPRECATION_AMD                  0x914B
#define DEBUG_CATEGORY_UNDEFINED_BEHAVIOR_AMD           0x914C
#define DEBUG_CATEGORY_PERFORMANCE_AMD                  0x914D
#define DEBUG_CATEGORY_SHADER_COMPILER_AMD              0x914E
#define DEBUG_CATEGORY_APPLICATION_AMD                  0x914F
#define DEBUG_CATEGORY_OTHER_AMD                        0x9150

#define DEBUG_SEVERITY_HIGH_AMD                         0x9146
#define DEBUG_SEVERITY_MEDIUM_AMD                       0x9147
#define DEBUG_SEVERITY_LOW_AMD                          0x9148

void eks::gl::setupDebugOutput()
{
	if (glext_AMD_debug_output) {
		LOG_INFO("AMD_debug_output found");
		glDebugMessageCallbackAMD(debugCallbackAMD, stderr);
		// we want to handle failed shader compilation ourselves
		glDebugMessageEnableAMD(DEBUG_CATEGORY_SHADER_COMPILER_AMD, DEBUG_SEVERITY_HIGH_AMD, 0, NULL, false);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	} else if (glext_ARB_debug_output) {
		LOG_INFO("ARB_debug_output found");

		glDebugMessageCallback((GLDEBUGPROC)debugCallbackARB, stderr);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_ERROR, GL_DONT_CARE, 0, NULL, GL_TRUE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR, GL_DONT_CARE, 0, NULL, GL_TRUE);

		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PORTABILITY, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PERFORMANCE, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_MARKER, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PUSH_GROUP, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_POP_GROUP, GL_DONT_CARE, 0, NULL, GL_FALSE);
		glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_OTHER, GL_DONT_CARE, 0, NULL, GL_FALSE);
		// We handle GLSL errors ourselves.
		glDebugMessageControl(GL_DEBUG_SOURCE_SHADER_COMPILER, GL_DEBUG_TYPE_ERROR, GL_DONT_CARE, 0, NULL, GL_FALSE);

		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
	} else {
		LOG_INFO("No debug output detected.");
	}
}