#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/quaternion.hpp>
#include "light.h"

class Camera {
	public:
	Camera();
	Camera(glm::vec3 position, glm::vec3 direction);
	//Camera(glm::vec3 position, glm::quat orientation, float fov, float nearplane, float farplane);
	Camera(glm::vec3& position, glm::quat& orientation, float fov_radians, float nearplane, float farplane);
	Camera(glm::vec3& position, glm::vec3& eulerangles, float fov_radians, float nearplane, float farplane);
	Camera(const Camera& other);
	~Camera();

	glm::vec3 getPosition();
	glm::vec3 getDirection();

	void alignWithLight(Light& light);

	// Assumes the up vector is (0.0, 1.0, 0.0)
	void lookAt(glm::vec3 eye, glm::vec3 target);
	glm::mat4x4 getView();
	glm::mat4x4 getViewProject();
	glm::mat4x4 getInverseView();
	glm::mat4x4 getProjection();
	glm::mat4x4 getInverseProjection();
	void setPerspective(float fov, float width, float height, float nearplane = 0.1f, float farplane = 100.0f);
	void setPerspectiveRadians(float fov_radians, float width, float height, float nearplane = 0.1f, float farplane = 100.0f);
	float getAspectRatio();

	// KAPSELOINTI :D
	glm::vec3 pos;
	glm::vec3 dir;

	float getNearPlane();
	float getFarPlane();

	private:
	glm::mat4x4 view;
	glm::mat4x4 inverseView;
	glm::mat4x4 projection;
	glm::mat4x4 inverseProjection;
	float screenwidth;
	float screenheight;
	float nearplane;
	float farplane;
};