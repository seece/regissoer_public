#pragma once

#include <cstdio>

#define EKS_MEASURE(expr) do {int __eks_temp_start = GetTickCount(); \
expr; \
printf("%s took %d ms\n", #expr, GetTickCount() - __eks_temp_start); \
} while (0);