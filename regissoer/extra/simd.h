#ifndef SIMD_H
#define SIMD_H

#include <cstdio>
//#include "stdafx.h"
#include <xmmintrin.h>	// Need this for SSE compiler intrinsics

void sumSSE(float * buffer, int length, float addition);
void multSSE(float * buffer, int length, float factor);

#endif