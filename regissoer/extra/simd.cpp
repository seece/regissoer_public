#include <xmmintrin.h>	
#include <cstdio>

void sumSSE(float * buffer, int length, float addition) 
{
	int SSELength = length/4;

	__m128 x;
	__m128 xDelta = _mm_set1_ps(addition);
	__m128 * bufferSSE = (__m128 *)buffer;

	for (int i=0;i<SSELength;i++) {
		x = _mm_set_ps(buffer[4*i+3], buffer[4*i+2], buffer[4*i+1], buffer[i]);
		bufferSSE[i] = _mm_add_ps(x, xDelta);
	}

	for (int i=SSELength*4;i<length;i++) {
		buffer[i] += addition;
	}

	return;
}

// buffer should be aligned properly
// microsoft has their own aligned malloc function
void multSSE(float * buffer, int length, float factor) 
{
	int SSELength = length/4;

	__m128 x;
	__m128 xMult = _mm_set1_ps(factor);
	__m128 * bufferSSE = (__m128 *)buffer;

	for (int i=0;i<SSELength;i++) {
		x = _mm_set_ps(buffer[4*i+3], buffer[4*i+2], buffer[4*i+1], buffer[i]);
		
		bufferSSE[i] = _mm_mul_ps(x, xMult);
	}

	// kerro my�s ylij��m�
	for (int i=SSELength*4;i<length;i++) {
		buffer[i] *= factor;
	}

	return;
}
