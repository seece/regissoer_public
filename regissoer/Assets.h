#pragma once
#include "AssetMap.h"

namespace assets {
extern MaterialMap materials;
extern TextureMap textures;
extern ShaderMap shaders;
extern MeshMap meshes;
};