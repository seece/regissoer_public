#pragma once
/*
 * An OpenGL shader program. Contains both vertex and fragment shaders.
 */

#include <string>
#include <vector>
#include "GL/gleks.h"
#include "system/Logger.h"
#include "ShaderCache.h"

class ShaderProgram {
	public:
		// Compiles a shader program from a single source with preprocessor separated sections.
		explicit ShaderProgram(const GLchar* shaderSource);

		// Loads a shader program from a binary blob with the first found binary format.
		ShaderProgram(const char* binaryProgram, int length);
		// Compiles a shader program from separate vertex and fragment shader source strings.
		//ShaderProgram(const GLchar* vertexsource, const GLchar* fragmentsource); // not needed so taken commented out

		~ShaderProgram();
		void use();
		GLint getUniformLocation(const GLchar* name); // TODO make these names different from ShaderProgram functions
		GLint getUniformBlockIndex(const GLchar* name);
		bool isValid(); // Returns false if the shader compilation failed.
		GLuint getId();
		// Writes the bytes to out_binary. Returns the amount of bytes written.
		int getProgramBinary(GLenum* out_format, std::vector<unsigned char>& out_binary);

	private:
		void createAndLink(GLuint vertexShader, GLuint fragmentShader);
		void createFromBinary(const char* binaryProgram, int length);
		bool compileShaders(const char* vertexString, const char* fragmentString);
		bool checkProgramStatus();

		static bool compileShaderSource(GLuint shaderId, const char* prefix);

		static const std::string versionString;
		static const std::string vertexDefine;
		static const std::string fragmentDefine;

		GLuint
			programId;

		bool vertexvalid;
		bool fragmentvalid;
		bool programvalid;
};	

