#pragma once
#include <set>
#include "Renderable.h"
#include "pipeline.h"
#include "Instance.h"
#include "Camera.h"

// std::set guarantees a weak strict order and by default it uses the < comparison
// operator. This means all instances are grouped by material.
typedef std::set<Renderable*> Scenegraph_t;

class Scene {
	public:
	Scene();
	~Scene();

	// Adds an instance to the internal storage and to scene graph
	//bool addToScene(Instance instance);

	// Functions used by the scene importer

	// Will take ownership of the given camera pointer
	void giveCamera(Camera* cam);
	// Will take ownership of the given light (will delete instantly)
	void giveLight(Light* light);
	// Takes ownership of the given pointer 
	bool Scene::addPointerToScene(Instance* instance);
	bool Scene::addPointerToScene(std::string name, Instance* instance);

	// Gets an object by name. Returns nullptr if not found.
	Instance* getInstance(const std::string& name);

	// Removes an instance from the internal storage and from scene graph
	//bool removeFromScene(Instance instance);

	// Adds a pointer to an instance to the internal scenegraph
	bool addToGraph(Renderable* instance);
	// Removes an instance from the scenegraph 
	bool removeFromGraph(Renderable* instance);

	// Issues draw calls for the scenegraph
	void render(Pipeline& pipeline, ParameterMap& params);

	// Adds to graph all objects contained in the internal list.
	void updateGraph();

	void useCamera(float index);

	Camera camera;
	LightList_t lights;
	Pipeline pipeline;

	std::vector<std::unique_ptr<Camera>> cameras;
	private:
	//std::vector<Instance> instances;
	std::unordered_map<std::string, Instance*> instanceNames;
	std::vector<std::unique_ptr<Instance>> instances;
	Scenegraph_t scenegraph;
};

typedef std::unordered_map<std::string, Scene*> SceneMap_t;