/**
 * The interactive player class used in debugging.
 * This shouldn't be compiled in the binary in Relase mode. 
 * The USE_SYNC_EDITOR hack is there since GNU Rocket is compiled 
 * only with player code in Release mode.
 */
#pragma once

#ifdef _DEBUG
#define USE_SYNC_EDITOR
#endif

#include "demoplayer.h"
#include "sync/sync.h"

class DebugPlayer : public DemoPlayer {
	public:
	DebugPlayer(Window& win);
	~DebugPlayer();
	void connectSyncEditor();
	void togglePlaying();
	//void play();  // already defined in demobase
	void pause(); 
	double getSongPos();
	float getTime(); // Song play time in seconds
	double seekTo(double secs); // returns the point where seek ended
	double seekToRow(double row); // returns the row where seek ended

	virtual void draw();

	void updateUI();
	void updateSyncTime(double row);
	//void updateSyncParams(double row, ParameterMap& parameters);

	protected:
	// rocket callbacks
	static void callback_pause(void *d, int flag);
	static void callback_set_row(void *d, int row);
	static int callback_is_playing(void *d);

	// We don't make the GNU Rocket sync editor connection in Release mode
	#ifdef USE_SYNC_EDITOR
	sync_cb cb; // sync callback container
	#endif

	private:
};