#include "mesh.h"

Mesh::Mesh(
	eks::gl::VertexFormat format, 
	const eks::gl::Vertex* vert_data, 
	GLsizei vert_count, 
	const GLuint* index_data, 
	GLsizei index_count, 
	GLenum index_type) :
	format(format), vertexCount(vert_count), 
	indexCount(index_count), indexType(index_type),
	vertexData(nullptr), indexData(nullptr)
{
	// we need a copy of the vertex data in case we want to recalculate normals etc.
	vertexData = new eks::gl::Vertex[vertexCount];
	memcpy(vertexData, vert_data, sizeof(eks::gl::Vertex)*vertexCount);

	if (indexCount > 0) {
		// FIXME: only GLuint index type supported
		indexData = new GLuint[indexCount];
		memcpy(indexData, index_data, sizeof(GLuint)*indexCount);
	}

	vbo = new VertexBuffer(format, (const GLvoid*)vertexData, 
		sizeof(eks::gl::Vertex)*vertexCount,	// we assume the vertices lie in a tightly-packed array
		(const GLvoid*)indexData, 
		sizeof(index_type)*indexCount);
}

Mesh::~Mesh() 
{
	// TODO use smart pointers instead
	delete vbo;
	delete vertexData;
	delete indexData;
};

void Mesh::draw(GLenum draw_mode) const
{
	if (format.indexed)
		vbo->draw(draw_mode, indexCount, indexType);
	else
		vbo->draw(draw_mode, vertexCount, indexType);
};

/*
void Mesh::renderFunction()
{
	draw(GL_TRIANGLES);
}
*/