#include "system/logger.h"
#include "Material.h"
#include "Scene.h"

Scene::Scene()
{
	//lights.resize(Pipeline::MAX_LIGHTS);
}

Scene::~Scene()
{

}

/*
bool Scene::addToScene(Instance instance)
{
	instances.push_back(instance);	
	addToGraph(&instances.back());
	return true;
}
*/

bool Scene::addPointerToScene(Instance* instance)
{
	std::unique_ptr<Instance> ptr = std::unique_ptr<Instance>(instance);
	instances.push_back(std::move(ptr));
	//return addToScene(*instance);
	return true;
}

bool Scene::addPointerToScene(std::string name, Instance* instance)
{

	std::unique_ptr<Instance> ptr = std::unique_ptr<Instance>(instance);
	instances.push_back(std::move(ptr));

	if (name.length() != 0 && name != "") {
		instanceNames[name] = instance;
	}

	return true;
}

Instance* Scene::getInstance(const std::string& name)
{
	if (instanceNames.find(name) == instanceNames.end()) {
		return nullptr;
	}

	return instanceNames[name];
}

bool Scene::addToGraph(Renderable* instance)
{
	scenegraph.insert(instance);
	return true;
}

bool Scene::removeFromGraph(Renderable* instance)
{
	if (scenegraph.find(instance) == scenegraph.end()) {
		return false; // not found, report back
	}

	scenegraph.erase(instance);
	return true;
}

void Scene::updateGraph()
{
	scenegraph.clear();

	for (auto& it : instances) {
		scenegraph.insert(it.get());
	}
}

void Scene::render(Pipeline& pipeline, ParameterMap& params) 
{
	if(scenegraph.size() == 0) { return; }
	Pipeline& p = pipeline;
	p.resetTextures();

	Material* last_material = nullptr;

	int amt = 0;
	for (auto& inst : scenegraph) {
		if (!inst->isVisible()) {
			continue;
		}

		glm::mat4x4& transform = inst->getRenderTransformation();

		Material* mat = inst->getRenderMaterial();

		if (mat != last_material) {
			mat->shader->program->use();
			mat->shader->applyParameters(params);
			mat->applyUniforms(p);
			last_material = mat;

			p.updateLightUniforms(mat->shader, lights);
			p.updateCameraUniforms(mat->shader, camera);
		}

		p.modelmatrix = transform;
		p.recalculateMatrices(camera);
		p.updateMatrixUniforms(mat->shader, camera);

		inst->render();

		amt++;
	}
}

void Scene::giveCamera(Camera* cam)
{
	cameras.push_back(std::unique_ptr<Camera>(cam));

	if (cameras.size() == 1) {
		camera = Camera(*cameras[0]);
	}
}

void Scene::giveLight(Light* light)
{
	lights.push_back(*light);
	delete light;
}

void Scene::useCamera(float index)
{
	int realindex = static_cast<int>(index);

	if (index < 0 || index >= cameras.size()) {
		LOG_WARN("Invalid camera index %f -> %d", index, realindex);
		return;
	}

	camera = *cameras[realindex];
}
