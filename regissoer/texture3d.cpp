#include <SOIL.h>
#include "GL/gleks.h"
#include "GLDebug.h"
#include "system/filesystem.h"
#include "system/logger.h"
#include "texture3d.h"

Texture3D::Texture3D(std::string path, int size, bool flip_y, GLint internalformat) :
	TextureBase(fsys::extractBasename(path))
{
	LOG_INFO("Loading 3D texture '%s'", path.c_str());
	fsys::File imageFile(path);

	int soil_format;
	GLenum format = GL_RGBA;

	pickFormat(internalformat, &format, &soil_format);
	
	int width, height;
	unsigned char* image = SOIL_load_image_from_memory(
		(const unsigned char*) imageFile.getData(),
		imageFile.getSize(),
		&width,
		&height,
		0,
		soil_format	
		);

	if (!image) {
		LOG_ERROR("SOIL: Couldn't load image %s", path.c_str());
	}

	texture_width = width;
	texture_height = height;

	LOG_DEBUG("SOIL: %d,0x%x, %s = %s %dx%d", soil_format, format, basename.c_str(), path.c_str(), width, height);

	int channels = soil_format == SOIL_LOAD_RGBA ? 4 : 3;

	int crop_width = size*size;
	int crop_height = size;
	uint8_t* cropped_image = crop_image(image, width, height, channels, 0, 0, crop_width, crop_height);

	if (flip_y) {
		//TextureBase::flip_image(image, width, height, channels);
		//TextureBase::flip_image(cropped_image, crop_width, crop_height, channels);
	}

	glGenTextures(1, &texID);
	eks::gl::assertGlError("Couldn't fetch new texture name");
	glBindTexture(GL_TEXTURE_3D, texID);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	eks::gl::assertGlError("Couldn't set texture parameters");

	glTexImage3D(GL_TEXTURE_3D, 0, internalformat, size, size, size, 0, format, GL_UNSIGNED_BYTE, cropped_image);
	//glTexImage2D(GL_TEXTURE_2D, 0, internalformat, crop_width, crop_height, 0, format, GL_UNSIGNED_BYTE, cropped_image);
	//glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, image);

	delete[] cropped_image;
	SOIL_free_image_data(image);

	eks::gl::assertGlError("Couldn't make texture");
}

Texture3D::~Texture3D()
{
}

void Texture3D::activate(GLenum unit)
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_3D, texID);
}