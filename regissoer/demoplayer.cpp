#include <SDL.h>
#include "system/system.h"
#include "system/logger.h"
#include "window.h"
#include "demoplayer.h"
#include "demo.h"

DemoPlayer* DemoPlayer::instance = nullptr;


DemoPlayer::DemoPlayer(Window& win) :
	window(win),
	currentDemo(nullptr),
	playing(false)
{
	win.registerPlayer(this);
	rocket = sync_create_device("data/sync/trak");
	instance = this;
};

DemoPlayer::~DemoPlayer()
{
	sync_destroy_device(rocket);

}

void DemoPlayer::update()
{
	if (window.keyDown(SDL_SCANCODE_ESCAPE)) {
		window.exitLoop();
	} 

	updateUI();

}

void DemoPlayer::draw()
{
	float t = getTime();

	if (!currentDemo) {
		return;
	}

	currentDemo->update(window, t);
	currentDemo->render(t, parameters);
}

 void DemoPlayer::loadDemo(DemoBase* demo)
{
	if (!demo)
		LOG_WARN("Loading a NULL demo to DemoPlayer");

	LOG_INFO("DemoPlayer: loaded demo 0x%p", demo);

	currentDemo = demo;
}

float DemoPlayer::getTime()
{
	return static_cast<float>(eks::system::getTime());
}

bool DemoPlayer::demoIsLoaded()
{
	return currentDemo != nullptr;
}

bool DemoPlayer::isPlaying()
{
	return playing;
}

void DemoPlayer::addParameter(std::string param)
{
	if (parameters.find(param) != parameters.end()) {
		// the key exists already, no need to add
		return;
	}

	parameters[param] = 0.0f;
}

void DemoPlayer::removeParameter(std::string param)
{
	if (parameters.find(param) == parameters.end()) {
		// parameter doesn't exist
		LOG_WARN("Trying to remove non-existent parameter %s", param.c_str());
		return;
	}

	parameters.erase(param);
}

void DemoPlayer::clearParameters()
{
	parameters.clear();
}

void DemoPlayer::play() 
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return;

	playing = true;
	currentDemo->getSong().play();
}

bool DemoPlayer::checkDemoValid(DemoBase* demo, const char* function) {
	if (demo == nullptr)  {
		LOG_DEBUG("currentDemo is null in %s", function);
		return false;
	}

	return true;
}


ParameterMap& DemoPlayer::getParameterMap()
{
	return parameters;
}

void DemoPlayer::updateSyncParams(double row, ParameterMap& params)
{
	if (!checkDemoValid(currentDemo, __FUNCTION__))
		return;

	for (auto& param : params) {
		const sync_track* trak = sync_get_track(rocket, param.first.c_str());
		parameters[param.first] = sync_get_val(trak, row);
	}

	params["p_time"] = getTime();
	params["p_beat"] = static_cast<float>(currentDemo->getSong().getBeat());
}
