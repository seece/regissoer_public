#include <algorithm>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include "Instance.h"
#include "AssetMap.h"
#include "Assets.h"
#include "system/logger.h"

Instance::Instance() : 
	mesh(nullptr),
	material(nullptr)
{
	initDefaults();
	//cacheMaterialName(material);
}

Instance::Instance(const Instance& other) :
	mesh(other.mesh),
	material(other.material),
	position(other.position),
	scale(other.scale),
	orientation(other.orientation),
	transformation(other.transformation),
	visible(true)
{
	updateTransformation();	
	cacheMaterialName(material);
}

Instance::Instance(Mesh* mesh_pointer, Material* material_pointer) : 
	mesh(mesh_pointer), material(material_pointer)
{
	initDefaults();
	updateTransformation();	
	cacheMaterialName(material);
}

Instance::Instance(Mesh* mesh_pointer, Material* material_pointer, glm::vec3 position, glm::vec3 scale, glm::quat orientation) :
	mesh(mesh_pointer),
	material(material_pointer),
	position(position),
	scale(scale),
	orientation(orientation),
	visible(true)
{
	//LOG_DEBUG("Instance: %p", this);
	updateTransformation();	
	cacheMaterialName(material);
}

Instance::Instance(Mesh* mesh_pointer, Material* material_pointer, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_euler) :
	mesh(mesh_pointer),
	material(material_pointer),
	position(position),
	scale(scale),
	visible(true)
{
	glm::mat4x4 rotation = glm::yawPitchRoll(rotation_euler.z, (rotation_euler.x - glm::half_pi<float>()), rotation_euler.y);
	orientation = glm::quat(rotation);
	updateTransformation();	
	cacheMaterialName(material);
	LOG_DEBUG("inst. material:\t%s", materialname.c_str());
}

void Instance::initDefaults() 
{
	scale = glm::vec3(1.0f);
	position = glm::vec3(0.0f);
	transformation = glm::mat4(1.0f);
	visible = true;
}

Instance::~Instance()
{
}

void Instance::setPosition(const glm::vec3& newpos)
{
	position = newpos;
	updateTransformation();
}

void Instance::setScale(const glm::vec3& newscale)
{
	scale = newscale;
	updateTransformation();
}

void Instance::setOrientation(const glm::quat& neworientation)
{
	orientation = neworientation;
	updateTransformation();
}

glm::vec3 Instance::getPosition() const
{
	return position;
}

glm::vec3 Instance::getScale() const
{
	return scale;
}

glm::quat Instance::getOrientation() const
{
	return orientation;
}

glm::mat4x4& Instance::getTransformation() 
{
	return transformation;
}

Material* Instance::getMaterial()
{
#ifdef _DEBUG
	return assets::materials.get(materialname);
#else
	return material;
#endif
}

Mesh* Instance::getMesh()
{
	return mesh;
}

void Instance::setMesh(Mesh* mesh_pointer)
{
	mesh = mesh_pointer;	
}

void Instance::setMaterial(Material* material_pointer)
{
	material = material_pointer;
}

void Instance::updateTransformation()
{
	glm::mat4 rotation = glm::toMat4(orientation);
	//glm::mat4 rotation = glm::mat4x4(1.0f); // FIXME: remove debugging code
	glm::mat4 translation = glm::translate(glm::mat4x4(1.0f), position); // TODO replace the identity matrix here with rotation?
	glm::mat4 scale = glm::scale(glm::mat4x4(1.0f), this->scale);
	//transformation = translation * scale * rotation;
	transformation = translation * rotation * scale;
	//transformation = glm::scale(transformation, this->scale);
}

void Instance::renderFunc()
{
	// We just hope mesh ain't nullptr :)
	mesh->draw();
}

Material* Instance::getRenderMaterialFunc()
{
	return getMaterial();
}

glm::mat4x4& Instance::getRenderTransformationFunc()
{
	return getTransformation();
}

void Instance::swap (Instance& other) throw ()
{
	using std::swap;
	swap(this->material, other.material);
	swap(this->mesh, other.mesh);
	swap(this->orientation, other.orientation);
	swap(this->position, other.position);
	swap(this->scale, other.scale);
	swap(this->transformation, other.transformation);
}

Instance& Instance::operator=(Instance other)
{
	other.swap(*this);	
	return *this;
}

bool Instance::isVisibleFunc()
{
	return visible;
}

void Instance::cacheMaterialName(Material* mat)
{
	materialname = mat->name;
}
