#pragma once
/* A 3D-texture cube. */

#include "TextureBase.h"

class Texture3D : public TextureBase {
	public:
	Texture3D(std::string path, int size, bool flip_y = true, GLint internalformat = GL_SRGB8_ALPHA8);	
	virtual ~Texture3D();	
	virtual void activate(GLenum unit); 

	protected:
};
