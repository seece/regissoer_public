#pragma once
/* A simple cube Mesh derived from the basic Mesh class. */

#include "../GL/gleks.h"
#include "../Mesh.h"

class CubeMesh : public Mesh {
	public:
	CubeMesh(); 
	~CubeMesh();

	private:
};
