
#include "tetraMesh.h"

const eks::gl::Vertex TETRA_VERTICES[12] =
{
	RGB_VERT( 0.0f,  0.65f,  0.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT( 0.0f, -0.35f, -1.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT(-0.87f,-0.35f,  0.5f, 1.0f, 1.0f, 0.0f),
	RGB_VERT( 0.0f, -0.35f, -1.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT( 0.0f,  0.65f,  0.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT( 0.87f,-0.35f,  0.5f, 1.0f, 0.0f, 1.0f),
	RGB_VERT( 0.87f,-0.35f,  0.5f, 1.0f, 0.0f, 0.0f),
	RGB_VERT( 0.0f,  0.65f,  0.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT(-0.87f,-0.35f,  0.5f, 1.0f, 1.0f, 0.0f),
	RGB_VERT( 0.0f, -0.35f, -1.0f, 1.0f, 0.0f, 0.0f),
	RGB_VERT( 0.87f,-0.35f,  0.5f, 1.0f, 0.0f, 0.0f),
	RGB_VERT(-0.87f,-0.35f,  0.5f, 1.0f, 0.0f, 1.0f)
};

TetraMesh::TetraMesh() : 
		Mesh(eks::gl::FORMAT_COLOR, TETRA_VERTICES, 12)
{
}

TetraMesh::~TetraMesh()
{

}