#include "quadMesh.h"

const eks::gl::Vertex QUAD_VERTICES[8] =
{
	//  x,      y,    z,    w,   red, green, blue, alpha,   nx,   ny,   nz,     u, v
	{{-1.0f, -1.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0, 0}},
	{{-1.0f,  1.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {0, 1}},
	{ {1.0f, -1.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {1, 0}},
	{ {1.0f,  1.0f, 0.0f, 1.0f}, {1.0f, 1.0f, 1.0f, 1.0f}, {0.0f, 0.0f, 1.0f}, {1, 1}}
};

const GLuint QUAD_INDICES[6] =
{
	0, 2, 1, 
	1, 2, 3
};

QuadMesh::QuadMesh() : 
	Mesh(eks::gl::FORMAT_INDEX_NORMAL_UV, QUAD_VERTICES, 8, QUAD_INDICES, 6)
{
}

QuadMesh::~QuadMesh()
{

}