#pragma once

#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/mat4x4.hpp>

class ISpaceNode {
	public:
	virtual void setPosition(const glm::vec3& newpos) = 0;
	virtual void setScale(const glm::vec3& newscale) = 0;
	virtual void setOrientation(const glm::quat& neworientation) = 0;
	virtual glm::vec3 getPosition() const = 0;
	virtual glm::vec3 getScale() const = 0;
	virtual glm::quat getOrientation() const = 0;
	virtual glm::mat4x4& getTransformation() = 0; 
};