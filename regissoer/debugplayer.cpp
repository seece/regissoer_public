#define WIN32_LEAN_AND_MEAN
#define VC_EXTRA_LEAN

#include <cmath>
#include <SDL.h>
#include "demo.h"
#include "debugplayer.h"
#include "window.h"
#include "sync/sync.h"
#include "tool/Tool.h"
#include <Windows.h>

void DebugPlayer::callback_pause(void *data, int flag)
{
	if (data == nullptr) {
		LOG_DEBUG("%s called with data == nullptr", __FUNCTION__);
		return;
	}

	DebugPlayer* player = reinterpret_cast<DebugPlayer*>(data);

	if (flag) {
		player->pause();
	} else {
		player->play();
	}
}

void DebugPlayer::callback_set_row(void *data, int row)
{
	if (data == nullptr) {
		LOG_DEBUG("%s called with data == nullptr", __FUNCTION__);
		return;
	}

	DebugPlayer* player = reinterpret_cast<DebugPlayer*>(data);
	player->seekToRow(static_cast<double>(row));
}

int DebugPlayer::callback_is_playing(void *data)
{
	if (data == nullptr) {
		LOG_DEBUG("%s called with data == nullptr", __FUNCTION__);
		return 0;
	}

	DebugPlayer* player = reinterpret_cast<DebugPlayer*>(data);
	return player->isPlaying();
}

DebugPlayer::DebugPlayer(Window& win) :
	DemoPlayer(win)
{
	#ifndef _DEBUG
		LOG_ERROR("Do NOT use DebugPlayer in Release mode!");
	#endif

	connectSyncEditor();
}

DebugPlayer::~DebugPlayer()
{
}

void DebugPlayer::connectSyncEditor()
{
	#ifdef USE_SYNC_EDITOR

	cb.pause = DebugPlayer::callback_pause;
	cb.set_row = DebugPlayer::callback_set_row;
	cb.is_playing = DebugPlayer::callback_is_playing;

	sync_set_callbacks(rocket, &cb, reinterpret_cast<void *>(this));

	unsigned short port = SYNC_DEFAULT_PORT;

	if (sync_connect(rocket, "localhost", port)) {
		LOG_WARN("Unable to initialize GNU ROCKET connection~");
	} else {
		LOG_INFO("Connected to sync editor in port %u", port);
	}

	#endif
}

void DebugPlayer::updateUI()
{
	if (!demoIsLoaded()) {
		return;
	}

}

void DebugPlayer::updateSyncTime(double row)
{
	int row_int = static_cast<int>(floor(row));

	#ifdef USE_SYNC_EDITOR
	sync_update(rocket, row_int);
	#endif
}

void DebugPlayer::togglePlaying()
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return;

	if (isPlaying()) {
		pause();
	} else {
		play();
	}
}


void DebugPlayer::pause() 
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return;

	playing = false;
	currentDemo->getSong().pause();
}

double DebugPlayer::getSongPos()
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return 0.0;

	return currentDemo->getSong().getTime();
}

double DebugPlayer::seekTo(double secs)
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return 0.0;

	currentDemo->getSong().seek(secs);

	return getSongPos();
}

float DebugPlayer::getTime()
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return 0.0;

	return static_cast<float>(getSongPos());
}

double DebugPlayer::seekToRow(double row)
{
	if (!checkDemoValid(currentDemo, __FUNCTION__)) 
		return 0.0;

	currentDemo->getSong().setRow(row);
	return currentDemo->getSong().getRow();
}

void DebugPlayer::draw()
{
	float t = getTime();

	if (!currentDemo) {
		return;
	}

	currentDemo->update(window, t);
	currentDemo->render(t, parameters);

	Tool::instance->drawDebugUi();
}