#pragma once

/*		
	A shader preprocessor.
	Currently only processes #include directives.
*/

#include <string>
#include <vector>

class ShaderPreprocessor {
	public:
	ShaderPreprocessor();
	~ShaderPreprocessor();
	void process(std::string path, std::string& data);

	protected:
	void processAndAdd(const std::string& path, std::string& target);
};

