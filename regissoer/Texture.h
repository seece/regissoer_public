#pragma once
/* A two dimensional OpenGL texture container. */

#include <string>
#include "texturebase.h"
#include "GL/gleks.h"
#include "system/Filesystem.h"

class Texture : public TextureBase {
	public:
	/* Loads the given file as an OpenGL-texture using the filesystem abstraction 
	 found from the fsys-namespace. Texture's y-axis is flipped by default. */
	Texture(std::string path, bool flip_y = true, bool mipmapped = true, GLint internalformat = GL_SRGB8_ALPHA8);	
	virtual ~Texture();

	// Binds the texture to the GL_TEXTURE_2D slot
	//virtual void bind(GLenum slot = GL_TEXTURE_2D);
	
	// Binds this texture to the given unit 
	virtual void activate(GLenum unit); 

	protected:
};
