#include <cstring>
#include <string>
#include "GL/gleks.h"
#include "GLDebug.h"
#include "system/Logger.h"
#include "ShaderProgram.h"

using eks::gl::assertGlError;
using eks::logger;

const std::string ShaderProgram::versionString = "#version 330\n";
const std::string ShaderProgram::vertexDefine = "#define VERTEX\n";
const std::string ShaderProgram::fragmentDefine = "#define FRAGMENT\n";

// returns true on success and false on failure
bool ShaderProgram::compileShaderSource(GLuint shaderId, const char* prefix) 
{
	glCompileShader(shaderId);

	GLint status = 0;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);
	assertGlError("Couldn't read shader compilation state");

		GLsizei size = 0;
		char infolog[8192];
		glGetShaderInfoLog(shaderId, 8192, &size, infolog);

		if (size > 0)
			LOG_INFO("GLSL: %s: %s", prefix, infolog);

	if (status == GL_FALSE) {
		return false;
	}

	return true;
}

ShaderProgram::ShaderProgram(const GLchar* shaderSource) :
	vertexvalid(false),
	fragmentvalid(false)
{
	std::string vertexShaderString = versionString + vertexDefine + shaderSource;
	std::string fragmentShaderString = versionString + fragmentDefine + shaderSource;

	compileShaders(vertexShaderString.c_str(), fragmentShaderString.c_str());
	programvalid = checkProgramStatus();
}

/*
ShaderProgram::ShaderProgram(const GLchar* vertexsource, const GLchar* fragmentsource) :
	vertexvalid(false),
	fragmentvalid(false)
{
	std::string vertexShaderString = versionString + vertexDefine + vertexsource;
	std::string fragmentShaderString = versionString + fragmentDefine + fragmentsource;

	bool success = compileShaders(vertexShaderString.c_str(), fragmentShaderString.c_str());
	programvalid = checkProgramStatus();
}
*/

ShaderProgram::ShaderProgram(const char* binaryProgram, int length)
{
	createFromBinary(binaryProgram, length);
}

void ShaderProgram::createFromBinary(const char* binaryProgram, int length)
{
	programId = glCreateProgram();
	glProgramBinary(programId, eks::gl::getProgramBinaryFormat(), binaryProgram, length);

	bool status = checkProgramStatus();
	programvalid = checkProgramStatus();
}

bool ShaderProgram::compileShaders(const char* vertexString, const char* fragmentString)
{
	GLuint vertexShaderId, fragmentShaderId;
	vertexvalid = true;
	fragmentvalid = true;

	const GLchar* vertexStrings[] = {vertexString};
	const GLchar* fragmentStrings[] = {fragmentString};

	vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShaderId, 1, vertexStrings, 0);
	assertGlError("Could not read vertex shader source");

	if (!compileShaderSource(vertexShaderId, "VS")) {
		this->vertexvalid = false;
		glDeleteShader(vertexShaderId);
		return false;
	}

	fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShaderId, 1, fragmentStrings, 0);
	assertGlError("Could not read fragment shader source");

	if (!compileShaderSource(fragmentShaderId, "FS"))  {
		this->fragmentvalid = false;
		glDeleteShader(fragmentShaderId);
		return false;
	}

	createAndLink(vertexShaderId, fragmentShaderId);
	// We don't need the shader objects anymore since the program is linked.
	glDeleteShader(vertexShaderId);
	glDeleteShader(fragmentShaderId);

	assertGlError("Could not create the shaders");

	return true;
}

bool ShaderProgram::checkProgramStatus()
{
	GLint status = 0;

	glGetProgramiv(programId, GL_LINK_STATUS, &status);

	if (!status) {
		GLsizei size = 0;
		char infolog[8192];
		glGetShaderInfoLog(programId, 8192, &size, infolog);

		if (size > 0)
			LOG_INFO("GLSL PROG: %s", infolog);
	}

	return status == 1;
}

bool ShaderProgram::isValid()
{
	return programvalid;
}

void ShaderProgram::createAndLink(GLuint vertexShader, GLuint fragmentShader)
{
	programId = glCreateProgram();
	glAttachShader(programId, vertexShader);
	glAttachShader(programId, fragmentShader);
	glProgramParameteri(programId, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
	glLinkProgram(programId);
}

ShaderProgram::~ShaderProgram()
{
	if (!isValid()) 
		return;

	glDeleteProgram(programId);
	assertGlError("Couldn't delete shader program");
}

void ShaderProgram::use()
{
	glUseProgram(programId);
	assertGlError("Couldn't use shader program");
}

GLint ShaderProgram::getUniformLocation(const GLchar* name)
{
	GLint location = glGetUniformLocation(programId, name);

	return location;
}

GLint ShaderProgram::getUniformBlockIndex(const GLchar* name)
{
	GLint index = glGetUniformBlockIndex(programId, name);

	if (index == GL_INVALID_INDEX) {
		//logger::debug("Invalid index. Program id %d in %s", programId,  __FUNCTION__);
		return GL_INVALID_INDEX;
	}

	return index;
}


GLuint ShaderProgram::getId()
{
	return programId;
}

int ShaderProgram::getProgramBinary(GLenum* out_format, std::vector<unsigned char>& out_binary)
{
	GLint binaryLength, outputLength;
	glGetProgramiv(programId, GL_PROGRAM_BINARY_LENGTH, &binaryLength);

	out_binary.resize(binaryLength);
	glGetProgramBinary(programId, binaryLength, &outputLength, out_format, static_cast<GLvoid*>(&out_binary[0]));

	return binaryLength;
}
