#include <cstdlib>
#include "Input.h"
#include "system/Logger.h"

Input::Input() : oldKeyboardState(nullptr), keyboardState(nullptr) 
{
	if (!SDL_WasInit(0)) {
		LOG_ERROR("SDL was not initialized before using Input class.");
	}

	initKeyboard();
	LOG_INFO("Input module initialized.");
};

Input::~Input() 
{
	if (oldKeyboardState) {
		delete[] oldKeyboardState;
	}
};

void Input::initKeyboard()
{
	keyboardState = SDL_GetKeyboardState(&keyCount);

	if (keyCount == 0)  {
		LOG_ERROR("Unable to fetch SDL key count");
		return;
	}

	oldKeyboardState = new uint8_t[keyCount];
};

bool Input::checkKeyValid(int key)
{
	if (!keyboardState) {
		LOG_ERROR("No keyboard state");
		return false;
	}

	if (key < 0 || key >= keyCount)  {
		LOG_ERROR("Trying to check invalid key code %d", key);
		return false;
	}

	return true;
}


bool Input::keyDown(int key)
{
	if (!checkKeyValid(key))
		return false;

	if (!oldKeyboardState) {
		LOG_ERROR("No old keyboard state");
		return false;
	}

	return keyboardState[key] == 1;
}

bool Input::keyHit(int key)
{
	if (!checkKeyValid(key))
		return false;

	return (oldKeyboardState[key] != keyboardState[key]) && !oldKeyboardState[key];
}

void Input::saveKeyboardState()
{
	if (!keyboardState)
		return;

	memcpy(oldKeyboardState, keyboardState, sizeof(Uint8)*keyCount);
}

void Input::processEvents()
{
	saveKeyboardState();
	SDL_PumpEvents();
	keyboardState = SDL_GetKeyboardState(&keyCount);
	oldMouse = mouse;
	mouse.state = SDL_GetMouseState(&mouse.pos.x, &mouse.pos.y);

	SDL_Event evt;

	textInputChars.clear();
	mouseWheel = 0;

	while (SDL_PollEvent(&evt)) {
		switch (evt.type) {
		case SDL_MOUSEWHEEL:
			mouseWheel =  evt.wheel.y;
			break;
		case SDL_TEXTINPUT:
			for (int i = 0; evt.text.text[i] != '\0'; i++) {
				uint32_t keycode = evt.text.text[i]; 

				if (keycode >= 32 && keycode <= 255)
				{
					// filter only printable ASCII characters
					// the text is actually utf-8 encoded so this might be a recipe for a disaster.
					// but it seems to work OK :)
					textInputChars.push_back((char)keycode);
				}
			}
			break;
		default:
			break;
		}
	}
}

Input::MousePosition Input::mousePos()
{
	return mouse.pos;
}

bool Input::mouseDown(int button)
{
	return (mouse.state & SDL_BUTTON(button)) != 0;
}

bool Input::mouseHit(int button)
{
	bool hit_last_time = ((oldMouse.state && SDL_BUTTON(button)) != 0);

	if (mouseDown(button) && !hit_last_time) {
		return true;
	}

	return false;
}

const std::vector<char>& Input::getTextInputChars() const
{
	return textInputChars;
}

int Input::getMouseWheelSpeed() const
{
	return mouseWheel;
}
