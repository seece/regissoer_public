/**
 * Contains a shader program and its metadata (file object, filename).
 * 
 * Also provides utility functions such as reloading the file from disk and
 * applying shader parameter uniforms.
 */
#pragma once

#include <string>
#include <memory>
#include <unordered_map>
#include "system/filesystem.h"
#include "shaderprogram.h"
#include "preprocessor.h"
#include "parametermap.h"

class Shader {
	public:

	struct UniformSlot {
		// the uniform name is saved as the uniformSlots map key
		GLenum datatype;
		int index;
		int arraySize;
		/*GLuint location;*/ // should these be stored in a separate map or not?
	};

	explicit Shader(const std::string& path);
	~Shader();
	ShaderProgram* program;
	std::string getFilepath();
	std::string getBasename();
	void reload(bool forceReload = false);
	void applyParameters(const ParameterMap& params); // tries to apply each parameter as a separate uniform
	GLint getUniformLocation(const std::string& name);
	GLint getUniformBlockIndex(const std::string& name);

	// Sets a single float uniform. 
	void floatUniform(const std::string& name, float value);

	static bool checkIfNameIsParameter(std::string name); // returns true if the uniform name begins with p_
	static bool checkIfNameIsParameter(const char* name); // returns true if the uniform name begins with p_

	static ShaderCache shaderCache; // This is initialized in main();
	// A shader preprocessor. Set to nullptr when not used. 
	static ShaderPreprocessor* processor; 

	std::unordered_map<std::string, UniformSlot> uniformSlots;

	private:
	ShaderProgram* createProgram(fsys::File& shaderfile);
	ShaderProgram* loadCachedProgramBinary(fsys::File& shaderfile, const std::string& expandedSource);
	void updateUniformSlots(); // Populates the uniformSlots map and registers are p_ uniforms to current demo player.

	std::string filepath;
	fsys::File file;
	std::unordered_map<std::string, GLuint> uniformLocations; 
	std::unordered_map<std::string, GLuint> blockLocations;

};