#pragma once
/* Includes OpenGL 3.3 headers and the GLload library for initialization. */

#include <glload/gl_1_0.h>
#include <glload/gl_3_3.h>
#include <glload/gl_load.h>

namespace eks {
namespace gl {
	// Returns the first program binary format supported by this OpenGL implementation. NULL if none found.
	GLenum getProgramBinaryFormat();
}
}