#include <cassert>
#include "gleks.h"

namespace eks {
namespace gl {
	GLenum getProgramBinaryFormat()
	{
		static GLenum formats[512];

		if (formats[0])
			return formats[0];

		GLint num;
		glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, &num);

		if (num == 0)
			return NULL;

		assert(num <= 512);
		glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, (GLint*)&formats[0]);

		return formats[0];
	}
}
}