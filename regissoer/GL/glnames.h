#pragma once
#include "gleks.h"

namespace eks {
namespace gl {
enum DatatypeCategory {
	TYPE_SCALAR = 0,
	TYPE_VECTOR,
	TYPE_MATRIX,
	TYPE_SAMPLER,
	TYPE_OTHER
};
const char* getDatatypeName(GLenum type);
DatatypeCategory getDatatypeCategory(GLenum type);
}
}
