#include "eks_math.h"

const double eks::math::PI = 3.14159265358979323846;

double eks::math::degToRad(double degrees)
{
	return (degrees/360.0)*2.0*PI;
}

double eks::math::radToDeg(double rads)
{
	return (rads/(2.0*PI))*360.0;
}