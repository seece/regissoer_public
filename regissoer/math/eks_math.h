#pragma once

namespace eks {
namespace math {
	extern const double PI;
	double degToRad(double degrees);
	double radToDeg(double rads);
}
}