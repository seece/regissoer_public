#pragma once
/* Engine vertex specification structures and values. */
#include "GL/gleks.h"

/// Create a Vertex struct with only pos.xyz and uv fields.
#define UV_VERT(x,y,z,u,v) {{ x, y, z, 1 }, { 1, 1, 1, 1 }, { 0, 0, 0 }, { u, v }}
/// Create a Vertex struct with only pos.xyz and rgb fields.
#define RGB_VERT(x,y,z,r,g,b) {{ x, y, z, 1 }, { r, g, b, 1 }, { 0, 0, 0 }, { 0, 0 }}


namespace eks {
namespace gl {
struct Vertex
{
	float pos[4];
	float color[4];
	float normal[3];
	float uv[2];
};

// Layout indices used in VBO constructor
enum LayoutIndex {
	LAYOUT_INDEX_VERTEX = 0,
	LAYOUT_INDEX_COLOR = 1,
	LAYOUT_INDEX_NORMAL = 2,
	LAYOUT_INDEX_UV = 3,
	LAYOUT_AMOUNT
};

// A vertex format describes which vertex attributes are enabled and
// also the specific format of each attribute.
struct VertexFormat {
	bool locations_active[LAYOUT_AMOUNT];	// see LayoutIndex enum
	bool indexed;

	struct {
		GLint size;
		GLenum type;
		GLboolean normalized;
		GLsizei stride;
		GLvoid* start_ofs;	
	} format[LAYOUT_AMOUNT];
};

// TODO replace these with a generator function or something
extern const VertexFormat FORMAT_INDEX_COLOR;
extern const VertexFormat FORMAT_INDEX_COLOR_UV;
extern const VertexFormat FORMAT_INDEX_NORMAL;
extern const VertexFormat FORMAT_INDEX_UV;
extern const VertexFormat FORMAT_INDEX_NORMAL_UV;
extern const VertexFormat FORMAT_INDEX_COLOR_UV;
extern const VertexFormat FORMAT_NORMAL;
extern const VertexFormat FORMAT_COLOR;
extern const VertexFormat FORMAT_NORMAL_UV; 	
}
}