
#include <glm/mat4x4.hpp>
#include "system/logger.h"
#include "matrixstack.h"

using eks::logger;

MatrixStack::MatrixStack()
{

}

MatrixStack::~MatrixStack()
{

}

// Clones the current matrix and places it to the top of the stack.
void MatrixStack::pushCurrent()
{
	glm::mat4x4 current = matrices.top();
	this->push(current);
}

void MatrixStack::push(const glm::mat4x4 &mat)
{
	matrices.push(mat);
}

glm::mat4x4 MatrixStack::pop()
{
	if (matrices.empty()) {
		LOG_ERROR("Trying to pop from an empty matrix stack!");
		return glm::mat4x4();
	}	

	glm::mat4x4 top = matrices.top();
	matrices.pop();
	return top;
}

bool MatrixStack::isEmpty() {
	return matrices.empty();
}

unsigned int MatrixStack::getSize() {
	return matrices.size();
}

// Returns a pointer to the matrix currently on top the stack
glm::mat4x4* MatrixStack::top()
{
	if (matrices.empty()) {
		LOG_ERROR("Trying to read top of an empty matrix stack!");
		return NULL;
	}	

	return &matrices.top();
}

void MatrixStack::loadIdentity()
{
	glm::mat4x4 identity = glm::mat4x4(1.0f);
	this->push(identity);
}

void MatrixStack::clear() 
{
	while (!matrices.empty()) {
		matrices.pop();
	}
}