/**
 * An abstract class that provides a demo with time and parameters. 
 */
#pragma once

#include <unordered_map>
#include <string>
#include "sync/sync.h"
#include "parametermap.h"
//#include "window.h"
class Window;
//#include "demo.h"
class DemoBase;
class Song;

class DemoPlayer {
	public:
	DemoPlayer(Window& win);
	virtual ~DemoPlayer();
	virtual void draw();
	virtual void update();

	virtual void loadDemo(DemoBase* demo);
	virtual void updateSyncTime(double row) = 0;
	// Automatically updates p_time variable
	virtual void updateSyncParams(double row, ParameterMap& parameters);
	void addParameter(std::string); // reserves a parameter slot so that it shows up at rocket
	void removeParameter(std::string);
	void clearParameters();
	ParameterMap& getParameterMap();

	virtual void play(); 

	static DemoPlayer* instance; // Latest created demoplayer instance, assigned in constructor.

	protected:
	static bool checkDemoValid(DemoBase* demo, const char* function);
	virtual void updateUI() = 0;
	bool demoIsLoaded();
	bool isPlaying();
	virtual float getTime();

	ParameterMap parameters;
	Window& window;
	DemoBase* currentDemo;

	sync_device *rocket;
	bool playing;
};
