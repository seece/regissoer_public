#pragma once
/*
 * OpenGL debugging utilities.
 */

#include <string>
#include "GL/gleks.h"

namespace eks {
namespace gl {
void print_uniform_block_info(GLuint prog, GLint block_index, std::string const &indent = std::string());
void assertGlError(const char* error_message, bool halt=true);
bool checkContextExists();
void setupDebugOutput();
}
}

