#include <cstdlib>
#include <string>
#include <cstring>
#include <map>
#include <algorithm>
#include <sstream>
#include <vector>
#include <tuple>
#include <array>
#include <cassert>
#include "objloader.h"
#include "system/filesystem.h"
#include "system/logger.h"
#include "Vertex.h"

using eks::logger;

namespace {
	std::string currentFileGlobal;
	int currentLineGlobal;
};

void dumpObjLoaderState()
{
	LOG_DEBUG("%s, line %d", currentFileGlobal.c_str(), currentLineGlobal);
}

struct ObjFace {
	unsigned int vert_index[3];
	unsigned int texcoord_index[3];
	unsigned int normal_index[3];
};

struct ObjVert {
	float x;
	float y;
	float z;

	ObjVert(float x, float y, float z) : x(x), y(y), z(z) {};
	ObjVert() {}; 
};

// If a field has no index, writes 0 since Wavefront Obj format begins
// indexing from 1 instead of 0.
//void parseVertexIndices(const std::string& str, std::vector<unsigned int> &indices)
//void parseVertexIndices(const std::string& str, unsigned int out_indices[8], int index_start_offset, int max_index_count)
void parseVertexIndices(const char* cstr, int len, unsigned int out_indices[8], int index_start_offset, int max_index_count)
{
	const int POOL_SIZE = 256;
	int index_pos = index_start_offset;

	char pool[POOL_SIZE];
	int pos = 0;
	//int len = str.length()+1; // NUL is also part of C-string length
	bool hit_slash = true;

	for (int i=0;i<len;i++) {
		char c = cstr[i];

		if ((c == '/' || c == '\0') && hit_slash) {
			// empty index, e.g. 2//, push just 0
			out_indices[index_pos++] = 0;
			
			pos = 0;
			hit_slash = true;
			continue;
		} else if ((c == '/' || c == '\0') && !hit_slash) {
			// scan ends, push the read value to vector
			pool[pos] = '\0';
			out_indices[index_pos++] = atoi(pool);
			pos = 0;
			hit_slash = true;
			continue;
		}

		assert(index_pos < max_index_count);
		assert(pos < POOL_SIZE);

		pool[pos++] = c;
		hit_slash = false;
	}
}

void parseFaceIndices(const std::string& str, std::vector<ObjFace> &faces)
{
	const char* cstr = str.c_str();

	char* next_token;
	strtok_s(const_cast<char*>(cstr), " ", &next_token); // Why does strtok_s take in a char* instead of const char*?

	const int MAX_FACE_INDICES = 3*3;
	unsigned int index_array[MAX_FACE_INDICES];

	int face_counter = 0;
	char* token;

	while (token = strtok_s(NULL, " ", &next_token)) {
		parseVertexIndices(token, strlen(token)+1, index_array, (face_counter)*3, MAX_FACE_INDICES);
		face_counter++;
	}

	ObjFace face = { 
		{index_array[0], index_array[3 + 0], index_array[2*3 + 0]},
		{index_array[1], index_array[3 + 1], index_array[2*3 + 1]},
		{index_array[2], index_array[3 + 2], index_array[2*3 + 2]},
	};

	faces.push_back(face);
}

namespace {
	inline void createGLVec(ObjVert& vert, ObjVert& normal, ObjVert& uv, eks::gl::Vertex* out_vec) 
	{
		out_vec->pos[0] = vert.x;
		out_vec->pos[1] = vert.y;
		out_vec->pos[2] = vert.z;
		out_vec->pos[3] = 1.0f;  // TODO is this ever necessary?
		out_vec->normal[0] = normal.x;
		out_vec->normal[1] = normal.y;
		out_vec->normal[2] = normal.z;
		out_vec->uv[0] = uv.x;
		out_vec->uv[1] = uv.y;
	}
}

Mesh* eks::util::loadWavefrontObj(const std::string& path, bool verbose)
{
	currentFileGlobal = path;
	currentLineGlobal = 0;
	fsys::File file(path);
	char* text = file.c_str();

	std::stringstream stream(text);
	std::string to;

	LOG_DEBUG("Loading %s", path.c_str());

	if (text == NULL) {
		LOG_ERROR("Couldn't read obj text from %s", path.c_str());
		return NULL;
	}

	std::vector<ObjVert> vertices;
	std::vector<ObjVert> normals;
	std::vector<ObjVert> uvCoords;
	std::vector<ObjFace> faces;

	// The first entry (index 0) will be uninitialized. 
	// Some faces do not contain all fields and the parser will output index #0 for those, 
	// so they end up using these values.
	vertices.push_back(ObjVert(0.0f, 0.0f, 0.0f));
	normals.push_back(ObjVert(0.0f, 0.0f, 0.0f));
	uvCoords.push_back(ObjVert(0.0f, 0.0f, 0.0f));

	while(std::getline(stream, to, '\n')) {
		currentLineGlobal++;
		// skip empty lines
		if (to.length() == 0) 
			continue;
		char first = to[0];

		if (first == '#')
			continue;

		ObjVert vert;

		switch (first) {
		case 'v':

			if (to[1] == 'n')  {
				sscanf(to.c_str(),"vn %f %f %f", &vert.x, &vert.y, &vert.z);
				normals.push_back(vert);

				if (verbose)
					LOG_DEBUG("normal: %f %f %f\n", vert.x, vert.y, vert.z);
			} else if (to[1] == 't')  {
				sscanf(to.c_str(),"vt %f %f", &vert.x, &vert.y);
				vert.z = 0;
				uvCoords.push_back(vert);

				if (verbose)
					LOG_DEBUG("uv coord: %f %f\n", vert.x, vert.y);
			} else {
				sscanf(to.c_str(),"v %f %f %f", &vert.x, &vert.y, &vert.z);
				vertices.push_back(vert);

				if (verbose)
					LOG_DEBUG("vertex: %f %f %f\n", vert.x, vert.y, vert.z);
			}
			break;
		case 'f':
			parseFaceIndices(to, faces);
			break;
		default:
			break;
		}
	}

	// Maps <vertex, normal, uv> indices to vertex index
	std::map<std::tuple<int, int, int>, int> indexmap;

	// Actual OpenGL index buffer
	std::vector<uint32_t> index_buffer;
	index_buffer.reserve(faces.size()*3);

	std::vector<eks::gl::Vertex> vertex_buffer;
	vertex_buffer.reserve(faces.size()*2);
	
	eks::gl::Vertex vert; // Used as a temporary inside the loop

	for (std::vector<ObjFace>::size_type i=0;i!=faces.size();i++) {
		ObjFace& face = faces[i];
		int vertindices[3];

		// Loop through the three face indices to fill in the missing vertex indices
		for (int v=0;v<3;v++) {
			int f = face.vert_index[v];
			int normal = face.normal_index[v];
			int uv = face.texcoord_index[v];
			auto key = std::tuple<int,int,int>(f, normal, uv);

			// Find out of which three vertices this face consists of.
			if (indexmap.find(key) == indexmap.end()) {
				// Not found, create the vertex entry and add it to the map
				createGLVec(vertices[f], normals[normal], uvCoords[uv], &vert);
				vertex_buffer.push_back(vert); // TODO reserve size from the map and write directly to it? do the values get initialized?

				int newindex = vertex_buffer.size() - 1;
				indexmap[key] = newindex;
				vertindices[v] = newindex;
			} else {
				vertindices[v] = indexmap[key];
			}
		}

		// Save the indices to index buffer
		for (int v=0;v<3;v++) {
			index_buffer.push_back(vertindices[v]);
		}
	}

	Mesh* mesh = new Mesh(eks::gl::FORMAT_INDEX_NORMAL_UV, &vertex_buffer[0], vertex_buffer.size(), &index_buffer[0], index_buffer.size());

	return mesh;
}