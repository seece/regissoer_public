#include "GL/gleks.h"
#include "GLDebug.h"
#include "system/Logger.h"
#include "VertexBuffer.h"

using namespace eks;

VertexBuffer::VertexBuffer(gl::VertexFormat format, const GLvoid* vertex_data, GLsizeiptr size, 
	const GLvoid* index_data, GLsizeiptr index_size) :
	activeFormat(format),
	vaoId(0),
	indexId(0)
{
	// for help, see http://stackoverflow.com/a/12103465

	glGenVertexArrays(1, &vaoId);
	gl::assertGlError("Couldn't generate VAO name");
	glBindVertexArray(vaoId);
	gl::assertGlError("Couldn't bind VAO"); 

	if (format.locations_active[gl::LAYOUT_INDEX_VERTEX])
		glEnableVertexAttribArray(0);

	if (format.locations_active[gl::LAYOUT_INDEX_COLOR])
		glEnableVertexAttribArray(1);

	if (format.locations_active[gl::LAYOUT_INDEX_NORMAL])
		glEnableVertexAttribArray(2);

	if (format.locations_active[gl::LAYOUT_INDEX_UV])
		glEnableVertexAttribArray(3);

	gl::assertGlError("Could not enable vertex attributes");

	// fetch object names (just uint indices)
	if (format.indexed)
		glGenBuffers(1, &indexId);	

	glGenBuffers(1, &vboId);
	glBindBuffer(GL_ARRAY_BUFFER, vboId);	// attach the created VertexBuffer to the VAO

	glBufferData(GL_ARRAY_BUFFER, size, vertex_data, GL_STATIC_DRAW);
	gl::assertGlError("Could not upload buffer data");

	for (int i=0;i<gl::LAYOUT_AMOUNT;++i) {
		if (!format.locations_active[i]) {
			continue;
		}

		glVertexAttribPointer(i, 
			format.format[i].size,
			format.format[i].type,
			format.format[i].normalized,
			format.format[i].stride,
			format.format[i].start_ofs);
	}

	gl::assertGlError("Couldn't set VAO attributes");

	// GL_ELEMENT_ARRAY_BUFFER is part of VAO's state, even though 
	// GL_ARRAY_BUFFER isn't.
	if (format.indexed) {
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_size, index_data, GL_STATIC_DRAW);
		gl::assertGlError("Couldn't bind the index buffer to VAO");
	}

	glBindVertexArray(0);
}

VertexBuffer::~VertexBuffer()
{
	if (activeFormat.indexed)
		glDeleteBuffers(1, &indexId);

	glDeleteBuffers(1, &vboId);
	glDeleteVertexArrays(1, &vaoId);
	gl::assertGlError("Couldn't delete buffer objects");
}

void VertexBuffer::draw(GLenum mode, GLsizei count, GLenum index_type) const
{
	glBindVertexArray(vaoId);
	gl::assertGlError("Couldn't bind VAO for drawing");

	if (activeFormat.indexed) {
		glDrawElements(mode, count, index_type, (GLvoid*)0);
		gl::assertGlError("Couldn't draw indexed VertexBuffer!");
	} else {
		glDrawArrays(mode, 0, count);
		gl::assertGlError("Couldn't draw VertexBuffer!");
	}

	glBindVertexArray(0);
}
