#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/trigonometric.hpp>
#include "Camera.h"
#include "system/logger.h"

// Point towards the negative Z-axis by default.
Camera::Camera() : pos(glm::vec3(0.0f, 0.0f, 0.0f)), dir(glm::vec3(0.0f, 0.0f, -1.0f))
{
	view = glm::lookAt(pos, pos + dir, glm::vec3(0.0f, 1.0f, 0.0f));
}

Camera::Camera(glm::vec3& position, glm::quat& orientation, float fov_radians, float nearplane, float farplane)
{
	setPerspectiveRadians(fov_radians, 1280.0f, 720.0f, nearplane, farplane); // TODO do not hardcore aspect ratio in camera constructor
	LOG_WARN("This camera constructor probably not implemented (yet???!)");
}

Camera::Camera(glm::vec3& position, glm::vec3& eulerangles, float fov_radians, float nearplane, float farplane) 
	: pos(position)
{
	LOG_DEBUG("EULER CAMERA conSTRUcTO!");
	setPerspectiveRadians(fov_radians, 1280.0f, 720.0f, nearplane, farplane); // TODO do not hardcore aspect ratio in camera constructor
	glm::mat4x4 rotation = glm::inverse(glm::yawPitchRoll(eulerangles.z, (eulerangles.x - glm::half_pi<float>()), -eulerangles.y));
	glm::mat4x4 translation = glm::translate(glm::mat4x4(1.0f), -position);

	view = rotation * translation;

	inverseView = glm::inverse(view);
	pos = position;
	dir = glm::vec3(glm::inverse(rotation) * glm::vec4(0.0f, 0.0f, -1.0f, 1.0f)); // inverse the inverted matrix huh
}

Camera::Camera(glm::vec3 position, glm::vec3 direction) :
	pos(position), dir(direction)
{
	view = glm::lookAt(pos, pos + dir, glm::vec3(0.0f, 1.0f, 0.0f));
}

Camera::Camera(const Camera& other)
	:
	screenwidth(other.screenwidth),
	screenheight(other.screenheight),
	nearplane(other.nearplane),
	farplane(other.farplane),
	view(other.view),
	inverseView(other.inverseView),
	projection(other.projection),
	inverseProjection(other.inverseProjection),
	pos(other.pos),
	dir(other.dir)
{
}

Camera::~Camera()
{
}

glm::mat4x4 Camera::getView()
{
	return view;
}

glm::mat4x4 Camera::getProjection()
{
	return projection;
}


glm::mat4x4 Camera::getViewProject()
{
	return projection*view;
}

glm::mat4x4 Camera::getInverseProjection()
{
	return inverseProjection;
}

glm::mat4x4 Camera::getInverseView()
{
	return inverseView;
}

void Camera::lookAt(glm::vec3 eye, glm::vec3 target)
{
	view = glm::lookAt(eye, target, glm::vec3(0.0f, 1.0f, 0.0f));
	inverseView = glm::inverse(view);
	pos = eye;
	dir = glm::normalize(target - eye);
}

glm::vec3 Camera::getPosition()
{
	return pos;
}

glm::vec3 Camera::getDirection()
{
	return dir;
}

void Camera::setPerspective(float fov, float width, float height, float nearplane, float farplane)
{
	projection = glm::perspectiveFov(glm::radians(fov), width, height, nearplane, farplane);
	inverseProjection = glm::inverse(projection);
	this->nearplane = nearplane;
	this->farplane = farplane;
	this->screenwidth = width;
	this->screenheight = height;
}

void Camera::setPerspectiveRadians(float fov_radians, float width, float height, float nearplane, float farplane)
{
	setPerspective(fov_radians/(2.0f*glm::pi<float>())*360.0f, width, height, nearplane, farplane); 
}

float Camera::getAspectRatio()
{
	return screenheight/screenwidth;
}

void Camera::alignWithLight(Light& light)
{
	lookAt(light.pos, light.pos + light.dir);
	// TODO use setPerspective to read FOV
}

float Camera::getNearPlane()
{
	return nearplane;
}

float Camera::getFarPlane()
{
	return farplane;
}