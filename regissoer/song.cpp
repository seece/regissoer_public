#include <bass.h>
#include "system/logger.h"
#include "song.h"

using eks::logger;

Song::Song(std::string path, double bpm, SongType songtype, int rows_per_beat) : 
	file(path), bpm(bpm), rpb(rows_per_beat), stream(0), songtype(songtype)
{
	LOG_INFO("Loading song %s", file.getFullPath().c_str());	

	if (songtype == SONG_STREAM)
		stream = BASS_StreamCreateFile(TRUE, file.getData(), 0, file.getSize(), BASS_STREAM_PRESCAN);
	else
		stream = BASS_MusicLoad(TRUE, file.getData(), 0, file.getSize(), BASS_STREAM_PRESCAN, 1);


	if (!stream)
		LOG_ERROR("Couldn't create stream");

	BASS_INFO info;
	BASS_GetInfo(&info);
	latency = info.latency / 1000.0;
}

Song::~Song()
{
	if (!stream ) {
		return;
	}

	if (songtype == SONG_STREAM) {
		if (!BASS_StreamFree(stream))
			LOG_ERROR("Couldn't free stream");
	}

	if (songtype == SONG_TRACKER) {
		if (!BASS_MusicFree(stream))
			LOG_ERROR("Couldn't free stream");
	}
}

void Song::play()
{
	if (!BASS_ChannelPlay(stream, FALSE)) 
		LOG_WARN("Couldn't play channel in stream %d", stream);
}

void Song::pause()
{
	BASS_ChannelPause(stream);
}

void Song::stop()
{
	BASS_ChannelStop(stream);
}

void Song::toggle()
{

}

double Song::seek(double seconds)
{
	unsigned long long pos = BASS_ChannelSeconds2Bytes(stream, seconds);
	BASS_ChannelSetPosition(stream, pos, BASS_POS_BYTE);
	return getTime();
}

double Song::getTime()
{
	unsigned long long pos = BASS_ChannelGetPosition(stream, BASS_POS_BYTE);
	return BASS_ChannelBytes2Seconds(stream, pos) - latency;
}

double Song::getLength()
{
	unsigned long long pos = BASS_ChannelGetLength(stream, BASS_POS_BYTE);
	return BASS_ChannelBytes2Seconds(stream, pos);
}

double Song::getBPM()
{
	return bpm;
}

double Song::getBeat()
{
	return getTime() * (bpm / 60.0);
}

double Song::getRow()
{
	return getBeat() * static_cast<double>(rpb);
}

void Song::setRow(double row)
{
	double row_rate = (static_cast<double>(bpm) / 60.0) * rpb;
	double time = row / row_rate;
	seek(time);
}