#pragma once
/* 
	Pipeline contains current view transformation information (Model, View and Projection matrices).

*/

#include <string>
#include <memory>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include "Vertex.h"
//#include "math/eks_vec3.h"
//#include "math/eks_mat4.h"
#include "matrixstack.h"
#include "shader.h"
#include "Renderable.h"
#include "mesh.h"
#include "light.h"
#include "texture.h"
#include "parametermap.h"
#include "fbo.h"
#include "Camera.h"

class Pipeline {
	public:
	Pipeline();
	~Pipeline();

	void setShader(Shader* newshader); // sets the new shader pointer and updates uniforms

	/*
	void push(); // clones the current matrix
	void pop();  // pops off the top model matrix stack entry
	void clear(); // clears the model matrix stack
	void loadIdentity();
	glm::mat4x4* top(); // returns the matrix on top of the stack
	*/
	void resetTextures();
	//void lookAt(glm::vec3, glm::vec3 target, glm::vec3 up);

	//void setRenderUniforms(ShaderProgram* target, Camera& camera); 
	void setScreenUniforms(Shader* target);

	// Re-calculates matrix uniforms and uploads the changes as uniforms
	void updateUniforms(Camera& camera, LightList_t& lights);
	// Re-calculates matrix uniforms
	void recalculateMatrices(Camera& camera);

	// These are helper functions to set parts of the pipeline by hand
	void updateLightUniforms(Shader* target, LightList_t& lights);
	void updateMatrixUniforms(Shader* target, Camera& camera);
	void updateCameraUniforms(Shader* target, Camera& camera);

	void draw(const Mesh& mesh, GLenum mode = GL_TRIANGLES); // updates uniforms and draws the mesh
	void draw(Renderable& object);
	//void updateMatrixUniforms(Camera& camera); 

	void setTexture(Texture& texture, const std::string& name);

	const static int MAX_LIGHTS = 8;
	/* indices for uniform buffers */ 
	const static int UNIFORM_INDEX_MATRICES = 0;
	const static int UNIFORM_INDEX_LIGHTS = 1;
	const static int UNIFORM_INDEX_SCREEN = 2; // screen dimension information

	glm::mat4x4 modelmatrix;
	//glm::mat4x4 view;
	glm::mat4x4 viewProject;
	glm::mat3x3 normalMatrix;
	glm::mat4x4 modelView;

	protected:
	void createUniformBuffers();
	void setupTransformBuffers();
	void setupLightBuffers();

	//MatrixStack stack;
	Shader* shader;
	bool checkShaderExists();
	int next_texture_slot; // the slot where to bind next texture. setTexture uses this
	//Camera camera;

	GLuint matricesUBO;
	GLuint lightsUBO;
	GLuint matricesBlockIndex;
	GLuint lightsBlockIndex;
	static const GLsizeiptr UNIFORM_SIZE_MATRICES = sizeof(float) * 6 * 16;
	// 3 x vec3 + bool(int that fits inside padding) 
	static const GLsizeiptr LIGHT_STRIDE = (sizeof(float) * 4) * 3;
	static const GLsizeiptr UNIFORM_SIZE_LIGHTS = MAX_LIGHTS * LIGHT_STRIDE; 

	char lightTempBuffer[UNIFORM_SIZE_LIGHTS];
	char matrixTempBuffer[UNIFORM_SIZE_MATRICES];
};
