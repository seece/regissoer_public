#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/trigonometric.hpp>
#include "pipeline.h"
#include "window.h"
#include "GLDebug.h"
#include "system/logger.h"

using eks::logger;

Pipeline::Pipeline() : shader(nullptr), next_texture_slot(0)
{
	createUniformBuffers();
	
	resetTextures(); 
	modelmatrix = glm::mat4x4(1.0f);
}

Pipeline::~Pipeline() 
{
	glDeleteBuffers(1, &matricesUBO);
	glDeleteBuffers(1, &lightsUBO);
}

void Pipeline::setShader(Shader* newshader)
{
	shader = newshader;
	setupTransformBuffers();
	setupLightBuffers();
	setScreenUniforms(newshader);
}

void Pipeline::resetTextures()
{
	next_texture_slot = 8;
}

void Pipeline::draw(const Mesh& mesh, GLenum mode)
{
	mesh.draw(mode);
}

void Pipeline::draw(Renderable& object)
{
	object.render();
}

void Pipeline::updateUniforms(Camera& camera, LightList_t& lights)
{
	if (shader == nullptr) {
		LOG_ERROR("%s called with nullptr shader", __FUNCTION__);
		return;
	}

	recalculateMatrices(camera);

	updateMatrixUniforms(shader, camera);
	updateLightUniforms(shader, lights);
	updateCameraUniforms(shader, camera);
}

void Pipeline::recalculateMatrices(Camera& camera)
{
	viewProject = (camera.getProjection() * camera.getView());
	modelView = camera.getView() * modelmatrix;
	//normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelView)));
	normalMatrix = glm::transpose(glm::inverse(glm::mat3x3(modelmatrix)));	// TODO do all lighting in viewspace, not worldspace
}

void Pipeline::createUniformBuffers()
{
	matricesBlockIndex = GL_INVALID_INDEX;
	lightsBlockIndex = GL_INVALID_INDEX;

	glGenBuffers(1, &matricesUBO);
	glGenBuffers(1, &lightsUBO);

	glBindBuffer(GL_UNIFORM_BUFFER,	matricesUBO);	
	glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_MATRICES, NULL, GL_STREAM_DRAW);

	glBindBuffer(GL_UNIFORM_BUFFER,	lightsUBO);	
	glBufferData(GL_UNIFORM_BUFFER, UNIFORM_SIZE_LIGHTS, NULL, GL_STREAM_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void Pipeline::setupLightBuffers()
{
	lightsBlockIndex = shader->getUniformBlockIndex("LightArray");

	if (lightsBlockIndex == GL_INVALID_INDEX) {
		//LOG_DEBUG("Invalid block index for LightArray, skipping");
		return;
	}

	glUniformBlockBinding(shader->program->getId(), lightsBlockIndex, UNIFORM_INDEX_LIGHTS);
	glBindBufferRange(GL_UNIFORM_BUFFER, UNIFORM_INDEX_LIGHTS, lightsUBO, 0, UNIFORM_SIZE_LIGHTS);

}

void Pipeline::setupTransformBuffers() 
{
	matricesBlockIndex = shader->getUniformBlockIndex("TransformMatrices");

	if (matricesBlockIndex == GL_INVALID_INDEX) {
		//LOG_DEBUG("Invalid block index for TransformMatrices, skipping");
		return;
	}

	// TODO actually this should be called only after shader creation
	glUniformBlockBinding(shader->program->getId(), matricesBlockIndex, UNIFORM_INDEX_MATRICES);
	glBindBufferRange(GL_UNIFORM_BUFFER, UNIFORM_INDEX_MATRICES, matricesUBO, 0, UNIFORM_SIZE_MATRICES);
}

void Pipeline::setScreenUniforms(Shader* target) 
{
	// TODO make a uniform buffer out of these and bind to each shader program 
	glUniform1f(target->getUniformLocation("screen.size.x"), Window::currentSettings.width);
	glUniform1f(target->getUniformLocation("screen.size.y"), Window::currentSettings.height);
}

void Pipeline::updateMatrixUniforms(Shader* target, Camera& camera)
{
	//glUniformMatrix4fv(target->getUniformLocation("viewProjectMatrix"), 1, GL_FALSE, glm::value_ptr(viewProject));
	glUniformMatrix3fv(target->getUniformLocation("normalMatrix"), 1, GL_FALSE, glm::value_ptr(normalMatrix));
	glUniformMatrix4fv(target->getUniformLocation("modelMatrix"), 1, GL_FALSE, glm::value_ptr(modelmatrix)); 

	glBindBuffer(GL_UNIFORM_BUFFER,	matricesUBO);

	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 0], glm::value_ptr(camera.getView()), sizeof(glm::mat4x4));
	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 1], glm::value_ptr(camera.getProjection()), sizeof(glm::mat4x4));
	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 2], glm::value_ptr(camera.getInverseView()), sizeof(glm::mat4x4));
	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 3], glm::value_ptr(camera.getInverseProjection()), sizeof(glm::mat4x4));
	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 4], glm::value_ptr(viewProject), sizeof(glm::mat4x4));
	memcpy(&matrixTempBuffer[sizeof(float) * 16 * 5], glm::value_ptr(normalMatrix), sizeof(glm::mat3x3));

	glBufferSubData(GL_UNIFORM_BUFFER, 0, Pipeline::UNIFORM_SIZE_MATRICES, matrixTempBuffer);
}

void Pipeline::updateCameraUniforms(Shader* target, Camera& camera)
{
	glUniform3fv(target->getUniformLocation("scenecam.pos"), 1, glm::value_ptr(camera.getPosition()));
	glUniform1f(target->getUniformLocation("scenecam.aspect"), camera.getAspectRatio());
	glUniform1f(target->getUniformLocation("scenecam.near"), camera.getNearPlane());
	glUniform1f(target->getUniformLocation("scenecam.far"), camera.getFarPlane());
}

void Pipeline::updateLightUniforms(Shader* target, LightList_t& lights)
{
	glBindBuffer(GL_UNIFORM_BUFFER,	lightsUBO);

	const int MAX_SHADOW_CASTERS = 1;

	// First disable all lights
	for (int i=0;i<Pipeline::MAX_LIGHTS;i++) {
		GLsizeiptr offset = LIGHT_STRIDE * i;
		unsigned int enabled = 0;
		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 2 + sizeof(float) * 3], &enabled, sizeof(int));
	}

	int shadow_casting_index = 0; 
	int regular_index = MAX_SHADOW_CASTERS;	// The first light spot is reserved for the shadow caster

	for (int i=0;i<lights.size();i++) {
		Light& cur = lights[i];
		GLsizeiptr offset; 
		unsigned int enabled = cur.enabled;

		if (!enabled)
			continue;

		int index = -1;

		if (cur.castsShadows) {
			index = 0;
		} else {
			index = regular_index;
			regular_index++;
		}

		offset = LIGHT_STRIDE * index;

		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 0],					glm::value_ptr(cur.pos),		sizeof(float) * 3);
		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 1], 					glm::value_ptr(cur.intensity),	sizeof(float) * 3);
		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 2], 					glm::value_ptr(cur.dir),		sizeof(float) * 3);
		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 2 + sizeof(float) * 3],&enabled,						sizeof(int)); // fits inside padding (vec4.w)
		memcpy(&lightTempBuffer[offset + sizeof(float) * 4 * 3],					&cur.falloff,					sizeof(float));
	}

	glBufferSubData(GL_UNIFORM_BUFFER, 0, Pipeline::UNIFORM_SIZE_LIGHTS, &lightTempBuffer);
}

void Pipeline::setTexture(Texture& texture, const std::string& name)
{
	texture.activate(GL_TEXTURE0 + next_texture_slot);
	GLuint location = shader->getUniformLocation(name.c_str());

	if (location != -1) {
		glUniform1i(location, next_texture_slot);
	} else {
		//LOG_DEBUG("Texture uniform %s of %s not found, skipping", name.c_str(), shader->getBasename().c_str());
		return;
	}

	next_texture_slot++;
}
