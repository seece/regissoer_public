#include "Tool.h"
#include "../GL/gleks.h"
#include "../GL/glnames.h"
#include "imgui/imgui.h"
#include "../util/stb_image.h"
#include "../system/system.h"

#include <algorithm>

namespace {
static const char* ImImpl_GetClipboardTextFn()
{
	return SDL_GetClipboardText();
}

static void ImImpl_SetClipboardTextFn(const char* text, const char* text_end)
{
	if (!text_end)
		text_end = text + strlen(text);
	if (*text_end == 0)
	{
		// Already got a zero-terminator at 'text_end', we don't need to add one
		SDL_SetClipboardText(text);
	}
	else
	{
		// Add a zero-terminator because glfw function doesn't take a size
		char* buf = (char*)malloc(text_end - text + 1);
		memcpy(buf, text, text_end-text);
		buf[text_end-text] = '\0';
		SDL_SetClipboardText(buf);
		free(buf);
	}
}
}

static GLuint fontTexture;
static int shader_handle, vert_handle, frag_handle;
static int texture_location, ortho_location;
static int position_location, uv_location, colour_location;

//streaming vbo
static unsigned int vbohandle, vaohandle, cursor, size;
template<typename T>
unsigned int stream(GLenum target, unsigned int vbo, unsigned int *vbo_cursor, unsigned int *vbo_size, T *start, int elementCount)
{
	unsigned int bytes = sizeof(T) *elementCount;
	unsigned int aligned = bytes + bytes % 64; //align memory
	unsigned int actualSize = 0;
	glGetBufferParameteriv(target, GL_BUFFER_SIZE, (GLint*)&actualSize);
	glBindBuffer(target, vbo);
	//If there's not enough space left, orphan the buffer object, create a new one and start writing
	if (vbo_cursor[0] + aligned > vbo_size[0])
	{
		assert(aligned < vbo_size[0]);
		glBufferData(target, vbo_size[0], NULL, GL_DYNAMIC_DRAW);
		vbo_cursor[0] = 0;
	}
	void* mapped = glMapBufferRange(target, vbo_cursor[0], aligned, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
	memcpy(mapped, start, bytes);
	vbo_cursor[0] += aligned;
	glUnmapBuffer(target);
	return vbo_cursor[0] - aligned; //return the offset we use for glVertexAttribPointer call
}

void make_ortho(float *result, float const & left, float const & right, float const & bottom, float const & top, float const & zNear, float const & zFar)
{
	result[0] = static_cast<float>(2) / (right - left);
	result[5] = static_cast<float>(2) / (top - bottom);
	result[10] = -float(2) / (zFar - zNear);
	result[12] = -(right + left) / (right - left);
	result[13] = -(top + bottom) / (top - bottom);
	result[14] = -(zFar + zNear) / (zFar - zNear);
}

// from https://github.com/thelinked/imgui/blob/master/examples/opengl3_example/main.cpp

// This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
// If text or lines are blurry when integrating ImGui in your engine:
// - try adjusting ImGui::GetIO().PixelCenterOffset to 0.0f or 0.5f
// - in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
static void ImImpl_RenderDrawLists(ImDrawList** const cmd_lists, int cmd_lists_count)
{
	if (cmd_lists_count == 0)
		return;
	// Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_SCISSOR_TEST);
	// Setup texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, fontTexture);
	// Setup orthographic projection matrix
	const float width = ImGui::GetIO().DisplaySize.x;
	const float height = ImGui::GetIO().DisplaySize.y;
	float ortho[16] = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 }; //identity matrix
	make_ortho(ortho, 0.0f, width, height, 0.0f, -1.0f, +1.0f);

	glUseProgram(shader_handle);
	glUniform1i(texture_location, 0);
	glUniformMatrix4fv(ortho_location, 1, GL_FALSE, ortho);
	glBindVertexArray(vaohandle);
	glEnableVertexAttribArray(position_location);
	glEnableVertexAttribArray(uv_location);
	glEnableVertexAttribArray(colour_location);

	for (int n = 0; n < cmd_lists_count; n++)
	{
		const ImDrawList* cmd_list = cmd_lists[n];
		const ImDrawVert* vtx_buffer = reinterpret_cast<const ImDrawVert*>(cmd_list->vtx_buffer.begin());
		int vtx_size = static_cast<int>(cmd_list->vtx_buffer.size());
		unsigned offset = stream(GL_ARRAY_BUFFER, vbohandle, &cursor, &size, vtx_buffer, vtx_size);
		glVertexAttribPointer(position_location, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (void*)(offset));
		glVertexAttribPointer(uv_location, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (void*)(offset + 8));
		glVertexAttribPointer(colour_location, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (void*)(offset + 16));
		int vtx_offset = 0;
		const ImDrawCmd* pcmd_end = cmd_list->commands.end();
		for (const ImDrawCmd* pcmd = cmd_list->commands.begin(); pcmd != pcmd_end; pcmd++)
		{
			glScissor((int)pcmd->clip_rect.x, (int)(height - pcmd->clip_rect.w), (int)(pcmd->clip_rect.z - pcmd->clip_rect.x), (int)(pcmd->clip_rect.w - pcmd->clip_rect.y));
			glDrawArrays(GL_TRIANGLES, vtx_offset, pcmd->vtx_count);
			vtx_offset += pcmd->vtx_count;
		}
	}

	glDisableVertexAttribArray(position_location);
	glDisableVertexAttribArray(uv_location);
	glDisableVertexAttribArray(colour_location);
	glUseProgram(0);
	glDisable(GL_SCISSOR_TEST);
}

void initShader(int *pid, int *vert, int *frag, const char *vs, const char *fs)
{
	pid[0] = glCreateProgram();
	vert[0] = glCreateShader(GL_VERTEX_SHADER);
	frag[0] = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(vert[0], 1, &vs, 0);
	glShaderSource(frag[0], 1, &fs, 0);
	glCompileShader(vert[0]);
	glCompileShader(frag[0]);
	glAttachShader(pid[0], vert[0]);
	glAttachShader(pid[0], frag[0]);
	glLinkProgram(pid[0]);
}

Tool* Tool::instance = nullptr;


Tool::Tool(Window& w, DemoBase& d, DemoPlayer& p)
	: window(w), demo(d), player(p)
{
	instance = this;
	initImGuiGL();
	initImGui();
}

Tool::~Tool()
{
	cleanupImGuiGL();
}


void Tool::initImGuiGL()
{
	static const char vertex_shader[] = \
		"#version 330\n"
		"uniform mat4 ortho;\n"
		"in vec2 Position;\n"
		"in vec2 UV;\n"
		"in vec4 Colour;\n"
		"out vec2 Frag_UV;\n"
		"out vec4 Frag_Colour;\n"
		"void main()\n"
		"{\n"
		" Frag_UV = UV;\n"
		" Frag_Colour = Colour;\n"
		"\n"
		" gl_Position = ortho*vec4(Position.xy,0,1);\n"
		"}\n";

	static const char fragment_shader[] = \
		"#version 330\n"
		"uniform sampler2D Texture;\n"
		"in vec2 Frag_UV;\n"
		"in vec4 Frag_Colour;\n"
		"out vec4 FragColor;\n"
		"void main()\n"
		"{\n"
		" FragColor = Frag_Colour * texture(Texture, Frag_UV.st);\n"
		"}\n";

	initShader(&shader_handle, &vert_handle, &frag_handle, vertex_shader, fragment_shader);
	texture_location = glGetUniformLocation(shader_handle, "Texture");
	ortho_location = glGetUniformLocation(shader_handle, "ortho");
	position_location = glGetAttribLocation(shader_handle, "Position");
	uv_location = glGetAttribLocation(shader_handle, "UV");
	colour_location = glGetAttribLocation(shader_handle, "Colour");

	size = static_cast<int>(pow(2.0, 20.0)); // 1 MiB streaming buffer
	
	glGenVertexArrays(1, &vaohandle);

	glGenBuffers(1, &vbohandle);
	glBindBuffer(GL_ARRAY_BUFFER, vbohandle);
	glBufferData(GL_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
}

void Tool::cleanupImGuiGL()
{
	glDeleteBuffers(1, &vbohandle);
	glDeleteVertexArrays(1, &vbohandle);
}

void Tool::initImGui()
{
	//glfwGetWindowSize(window, &w, &h);
	int w = window.getWidth();
	int h = window.getHeight();

	ImGuiIO& io = ImGui::GetIO();

	io.IniFilename = "imgui_config.ini";

	io.DisplaySize = ImVec2((float)w, (float)h); // Display size, in pixels. For clamping windows positions.
	io.DeltaTime = 1.0f/60.0f; // Time elapsed since last frame, in seconds (in this sample app we'll override this every frame because our timestep is variable)
	io.PixelCenterOffset = 0.0f; // Align OpenGL texels
	io.KeyMap[ImGuiKey_Tab] = SDL_SCANCODE_TAB; // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array.
	io.KeyMap[ImGuiKey_LeftArrow] = SDL_SCANCODE_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = SDL_SCANCODE_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = SDL_SCANCODE_UP;
	io.KeyMap[ImGuiKey_DownArrow] = SDL_SCANCODE_DOWN;
	io.KeyMap[ImGuiKey_Home] = SDL_SCANCODE_HOME;
	io.KeyMap[ImGuiKey_End] = SDL_SCANCODE_END;
	io.KeyMap[ImGuiKey_Delete] = SDL_SCANCODE_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = SDL_SCANCODE_BACKSPACE;
	io.KeyMap[ImGuiKey_Enter] = SDL_SCANCODE_RETURN;
	io.KeyMap[ImGuiKey_Escape] = SDL_SCANCODE_ESCAPE;
	io.KeyMap[ImGuiKey_A] = SDL_SCANCODE_A;
	io.KeyMap[ImGuiKey_C] = SDL_SCANCODE_C;
	io.KeyMap[ImGuiKey_V] = SDL_SCANCODE_V;
	io.KeyMap[ImGuiKey_X] = SDL_SCANCODE_X;
	io.KeyMap[ImGuiKey_Y] = SDL_SCANCODE_Y;
	io.KeyMap[ImGuiKey_Z] = SDL_SCANCODE_Z;
	io.RenderDrawListsFn = ImImpl_RenderDrawLists;
	io.SetClipboardTextFn = ImImpl_SetClipboardTextFn;
	io.GetClipboardTextFn = ImImpl_GetClipboardTextFn;
	// Load font texture
	glGenTextures(1, &fontTexture);
	glBindTexture(GL_TEXTURE_2D, fontTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	const void* png_data;
	unsigned int png_size;
	ImGui::GetDefaultFontData(NULL, NULL, &png_data, &png_size);
	int tex_x, tex_y, tex_comp;
	void* tex_data = stbi_load_from_memory((const unsigned char*)png_data, (int)png_size, &tex_x, &tex_y, &tex_comp, 0);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_x, tex_y, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_data);
	stbi_image_free(tex_data);
}

void Tool::updateImGui()
{
	ImGuiIO& io = ImGui::GetIO();
	// Setup timestep
	static double time = 0.0f;
	const double current_time = eks::system::getTime();
	io.DeltaTime = (float)(current_time - time);
	time = current_time;

	// Setup inputs
	Input::MousePosition mpos = window.mousePos();
	io.MousePos = ImVec2((float)mpos.x, (float)mpos.y); // Mouse position, in pixels (set to -1,-1 if no mouse / on another screen, etc.)
	io.MouseDown[0] = window.mouseDown(1);
	io.MouseDown[1] = window.mouseDown(3);
	int scrollUp = (int)window.mouseHit(4);
	int scrollDown = (int)window.mouseHit(5);
	io.MouseWheel = window.getInput()->getMouseWheelSpeed();
	io.KeyShift = window.keyDown(SDL_SCANCODE_LSHIFT) || window.keyDown(SDL_SCANCODE_RSHIFT);
	SDL_Keymod modstate = SDL_GetModState();

	io.KeyCtrl = (modstate & KMOD_CTRL) != 0;
	io.KeyShift = (modstate & KMOD_SHIFT) != 0;

	for (int i = SDL_SCANCODE_A; i < 300; i++) {
		io.KeysDown[i] = window.keyDown(i);
	}

	for (auto c : window.getInput()->getTextInputChars()) {
		io.AddInputCharacter(c);
	}

	// Start the frame
	ImGui::NewFrame();
}

void Tool::drawDebugUi()
{
	updateImGui();

	ImGuiIO& io = ImGui::GetIO();

	static bool show_test_window = true;
	static bool show_asset_inspector = true;
	static float f;
	ImGui::Text("the debuggaus ui!");
	//ImGui::SliderFloat("float", &f, 0.0f, 1.0f);
	show_test_window ^= ImGui::Button("Test Window");
	show_asset_inspector ^= ImGui::Button("Asset inspector");

	if (ImGui::Button("Purge shader cache")) {
		Shader::shaderCache.purge();
		LOG_INFO("Shader cache cleared.");
	}

	// Show the ImGui test window
	// Most of user example code is in ImGui::ShowTestWindow()
	if (show_test_window)
	{
		ImGui::ShowTestWindow(&show_test_window);
	}

	// Show another simple window
	if (show_asset_inspector)
	{
		ImGui::Begin("Asset Inspector", &show_asset_inspector, ImVec2(400,300));
		ImGui::Text("%d shaders", assets::shaders.entries.size());

		std::vector<const char*> shadernames;

		for (auto& entry : assets::shaders.entries) {
			shadernames.push_back(entry.first.c_str());
		}

        static int selected_shader = -1;
        ImGui::Combo("shader", &selected_shader, &shadernames[0], shadernames.size());

		if (selected_shader >= 0) {
			std::string name(shadernames[selected_shader]);
			Shader& shader = assets::shaders[name];

			std::vector<std::string> sortedUniformNames;

			for (auto& uniform : shader.uniformSlots) {
				sortedUniformNames.push_back(uniform.first);
			}

			/*
			std::sort(sortedUniformNames.begin(), sortedUniformNames.end(), 
				[shader](std::string a, std::string b){
				if (shader.u)
			});*/

			std::sort(sortedUniformNames.begin(), sortedUniformNames.end());

			std::string label = std::to_string(shader.uniformSlots.size()) + " uniforms";
			if (ImGui::CollapsingHeader(label.c_str())) {
				static ImGuiTextFilter filter;
				filter.Draw();

				ImGui::Columns(3, "data", true);
				ImGui::Text("Type"); ImGui::NextColumn();
				ImGui::Text("Name"); ImGui::NextColumn();
				ImGui::Text("Index"); ImGui::NextColumn(); 
				ImGui::Separator();

				for (auto& uniformName : sortedUniformNames) {
					auto& uniform = shader.uniformSlots[uniformName];
					const char* dataType = getDatatypeName(uniform.datatype);
					eks::gl::DatatypeCategory category = getDatatypeCategory(uniform.datatype);

					if (!filter.PassFilter(uniformName.c_str()))
						continue;

					ImVec4 color(1.0, 1.0, 1.0, 1.0);

					switch (category) {
					case TYPE_VECTOR:
						color = ImVec4(0.5, 0.5, 1.0, 1.0);
						break;
					case TYPE_MATRIX:
						color = ImVec4(0.5, 1.0, 0.5, 1.0);
						break;
					case TYPE_SAMPLER:
						color = ImVec4(1.0, 0.5, 0.5, 1.0);
						break;
					}

					ImGui::TextColored(color, dataType); ImGui::NextColumn();

					if (Shader::checkIfNameIsParameter(uniformName)) {
						ImGui::TextColored(ImVec4(1.0, 1.0, 0.0, 1.0), uniformName.c_str());

						if (ImGui::IsHovered()) {
							auto& params = DemoPlayer::instance->getParameterMap();
							if (params.find(uniformName) != params.end()) 
								ImGui::SetTooltip("%f", params[uniformName]);
							else 
								ImGui::SetTooltip("No value set.");
						}
					} else {
						ImGui::Text(uniformName.c_str());
					}

					ImGui::NextColumn();
					ImGui::Text(std::to_string(uniform.index).c_str()); ImGui::NextColumn();
				}
				ImGui::Columns(1);
			}
		}

		ImGui::End();
	}

	ImGui::Render();
}