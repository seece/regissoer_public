/*
	Demo editor tool code that renders debug data and allows user to manipulate
	the demo. Also has code to handle hot keys and filesystem polling for 
	data updates (maybe?).
*/
#pragma once
#include "../Assets.h"
#include "../demo.h"
#include "../demoplayer.h"
#include "../song.h"
#include "../window.h"

class Tool {
public:
	Tool(Window& w, DemoBase& d, DemoPlayer& p);
	~Tool();

	void drawDebugUi();

	static Tool* instance;
private:
	Window& window;
	DemoBase& demo;
	DemoPlayer& player;
	void initImGuiGL();
	void cleanupImGuiGL();
	void initImGui();
	void updateImGui();
};
