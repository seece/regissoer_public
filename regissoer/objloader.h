#pragma 
#include <string>
#include "mesh.h"

namespace eks {
namespace util {
Mesh* loadWavefrontObj(const std::string& path, bool verbose=false);
};
};