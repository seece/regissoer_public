#include <string>
#include "Assets.h"

namespace assets {
MaterialMap materials;
TextureMap textures;
ShaderMap shaders;
MeshMap meshes;
};