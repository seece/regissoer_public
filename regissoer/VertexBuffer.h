#pragma once
/*
	A wrapper for OpenGL Vertex Buffer Objects and its attributes. 
	Stores VAO, VBO and possibly index buffer OpenGL names.

	A VBO doesn't hold much state about the object, use Mesh class to store 
	vertex and index data.
*/

#include "GL/gleks.h"
#include "GLDebug.h"
#include "Vertex.h"

class VertexBuffer {
	public:

	VertexBuffer(eks::gl::VertexFormat format, const GLvoid* vertex_data, GLsizeiptr size, 
		const GLvoid* index_data = 0, GLsizeiptr index_size = 0);
	~VertexBuffer();
	// index_type is ignored on non-indexed meshes
	void draw(GLenum mode, GLsizei count, GLenum index_type) const;

	private:
	eks::gl::VertexFormat activeFormat;

	GLuint vaoId;
	GLuint vboId;
	GLuint indexId;
};