#pragma once
#include <stack>
#include <glm/mat4x4.hpp>

class MatrixStack {
	public:
	MatrixStack();
	~MatrixStack();

	void push(const glm::mat4x4 &mat); // pushes the given matrix on top of the stack
	void pushCurrent(); // clones the current matrix and places it on top of the stack
	glm::mat4x4 pop(); // pops a matrix from stack
	glm::mat4x4* top(); // returns the matrix on top of the stack
	bool isEmpty(); 
	unsigned int getSize(); // how many elements are there currently stored in the stack
	void loadIdentity(); // pushes an identity matrix to the stack
	void clear(); // removes all matrices from the stack

	private:
	std::stack <glm::mat4x4> matrices;
};