#pragma once

/**
 * The non-interactive player class used in standalone release builds.
 */
#pragma once
#include "demoplayer.h"
#include "song.h"

class ReleasePlayer : public DemoPlayer {
	public:
	explicit ReleasePlayer(Window& win);
	~ReleasePlayer();

	void updateSyncTime(double row);

	protected:
	float getTime();
	void updateUI();

	private:
};