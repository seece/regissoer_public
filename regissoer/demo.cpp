#include "demo.h"
#include "song.h"

DemoBase::DemoBase(Window& window, Song& song) :
	window(window), song(song)
{

};

Song& DemoBase::getSong() 
{
	return song;
}

SceneMap_t& DemoBase::getSceneMap()
{
	return scenes; 
}