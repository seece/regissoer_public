#pragma once

#include <cstdint>
#include <string>
#include "GL/gleks.h"

class TextureBase {
	public:
	TextureBase(std::string path);
	virtual ~TextureBase();

	// Returns texture object's OpenGL name
	GLuint getID();

	// Binds the texture to the given slot
	virtual void bind(GLenum slot);
	
	// Binds this texture to the given unit 
	virtual void activate(GLenum unit) = 0; 
	std::string getBasename(); 

	int getWidth();
	int getHeight();

	protected:
	void pickFormat(GLint internalformat, GLenum* format, int* soil_format);
	void flip_image(unsigned char* img, int width, int height, int channels);
	uint8_t* crop_image(uint8_t* img, int img_w, int img_h, int channels, int crop_x, int crop_y, int crop_w, int crop_h);
	GLuint texID;
	//void load(const unsigned char* imagedata);
	std::string basename;
	int texture_width;
	int texture_height;
};
