#pragma once
#include <string>
#include <unordered_map>
#include <vector>

typedef std::unordered_map<std::string, float> ParameterMap;
