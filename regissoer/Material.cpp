#include "Material.h"
#include "pipeline.h"

Material::Material(std::string name, Shader* shader, Texture* texture, Texture* emitmap, float exponent, float multiplier, 
				   glm::vec3 specularColor) :
	name(name),
	shader(shader), 
	texture(texture), 
	emitmap(emitmap), 
	exponent(exponent), 
	multiplier(multiplier), 
	specularColor(specularColor),
	fullbright(false)
{
}

Material::Material() : 
	shader(nullptr),
	texture(nullptr),
	emitmap(nullptr),
	exponent(1.0f),
	fullbright(false)
{
}

Material::~Material()
{
}


void Material::applyUniforms(Pipeline& p)
{
	p.setShader(shader);
	p.setTexture(*texture, "tex");
	p.setTexture(*emitmap, "emitMap");
	shader->floatUniform("exponent", exponent);
	shader->floatUniform("specularMultiplier", multiplier);
}