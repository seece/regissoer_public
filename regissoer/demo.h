#pragma once

#include <string>
#include <unordered_map>
#include "window.h"
#include "shaderprogram.h"
#include "fbo.h"
#include "shader.h"
#include "matrixstack.h"
#include "mesh.h"
#include "mesh/tetraMesh.h"
#include "mesh/quadMesh.h"
#include "song.h"
#include "parametermap.h"
#include "Scene.h"

class DemoBase {
	public:
	DemoBase(Window& window, Song& song);
	virtual ~DemoBase() {};

	virtual void update(Window& window, float time) = 0;
	virtual void render(float time, ParameterMap params) = 0;
	virtual void reloadAssets() = 0;

	virtual Song& getSong();
	SceneMap_t& getSceneMap();

	protected:

	Window& window;
	Song& song;
	SceneMap_t scenes;
};