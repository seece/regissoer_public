#pragma once

#include <vector>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>
#include "SpaceNode.h"

struct Light : public ISpaceNode {
	Light();
	Light(glm::vec3 pos, glm::vec3 intensity, glm::vec3 dir, float falloff, bool enabled);
	Light::Light(glm::vec3 pos, glm::vec3 dir, glm::vec3 color, float energy, float falloff, float spot_size, bool shadow, bool enabled);
	~Light();

	// Sets pos and dir to match the given camera position and orientation target.
	void lookAt(glm::vec3 camera, glm::vec3 target);

	// Sets the view matrix that transforms a point to light view space coordinates
	//void saveViewMatrix(eks::math::Mat4& output) const; 

	glm::vec3 pos;
	glm::vec3 intensity; 
	glm::vec3 dir;
	float falloff; // currently unused
	bool enabled;
	bool castsShadows;

	glm::mat4x4 transform;

	virtual void setPosition(const glm::vec3& newpos);
	virtual void setScale(const glm::vec3& newscale);
	virtual void setOrientation(const glm::quat& neworientation);
	virtual glm::vec3 getPosition() const;
	virtual glm::vec3 getScale() const;
	virtual glm::quat getOrientation() const;
	virtual glm::mat4& getTransformation(); 
};

typedef std::vector<Light> LightList_t;
