#include <SOIL.h>
#include "texturebase.h"

#include "GL/gleks.h"
#include "GLDebug.h"
#include "TextureBase.h"
#include "system/filesystem.h"
#include "system/Logger.h"

using eks::logger;


TextureBase::TextureBase(std::string path) : basename(fsys::extractBasename(path))
{
}

TextureBase::~TextureBase()
{
}

GLuint TextureBase::getID()
{
	return texID;
}

void TextureBase::bind(GLenum slot) 
{
	glBindTexture(slot, getID());
}


std::string TextureBase::getBasename() {
	return basename;
}

// Ripped from SOIL
void TextureBase::flip_image(unsigned char* img, int width, int height, int channels)
{
	int i, j;
	for( j = 0; j*2 < height; ++j )
	{
		int index1 = j * width * channels;
		int index2 = (height - 1 - j) * width * channels;
		for( i = width * channels; i > 0; --i )
		{
			unsigned char temp = img[index1];
			img[index1] = img[index2];
			img[index2] = temp;
			++index1;
			++index2;
		}
	} 
}

// Returns a cropped version of a image. Should be deleted by caller.
uint8_t* TextureBase::crop_image(uint8_t* img, int img_w, int img_h, int channels, int crop_x, int crop_y, int crop_w, int crop_h) 
{
	int cropsize = crop_w*crop_h*channels;
	uint8_t* cropped = new uint8_t[cropsize];

	for (int y=0;y<crop_h;++y) {
	for (int x=0;x<crop_w;++x) {
		int index = ((crop_y + y) * img_w) * channels + (crop_x + x) * channels;
		int index2 = (y * crop_w + x) * channels;

		for (int c=0;c<channels;c++) {
			cropped[index2 + c] = img[index + c];

			if (c != 3) {
				//cropped[index2 + c] = (x ^ y) % 256;
			}
		}
	}
	}

	return cropped;
}

void TextureBase::pickFormat(GLint internalformat, GLenum* format, int* soil_format)
{

	switch (internalformat) {
		case GL_RGB:
			*soil_format = SOIL_LOAD_RGB;
			*format = GL_RGB;
			break;
		case GL_SRGB:
			*soil_format = SOIL_LOAD_RGB;
			*format = GL_RGB;
			break;
		case GL_RGBA:
			*soil_format = SOIL_LOAD_RGBA;
			*format = GL_RGBA;
			break;
		case GL_SRGB8_ALPHA8:
			*soil_format = SOIL_LOAD_RGBA;
			*format = GL_RGBA;
			break;
		default:
			*soil_format = SOIL_LOAD_RGB;
			*format = GL_RGB;
			LOG_DEBUG("Unknown texture internalformat, falling back to SOIL_LOAD_RGB");
			break;
	}
}

int TextureBase::getWidth()
{
	return texture_width;
}

int TextureBase::getHeight()
{
	return texture_height;
}