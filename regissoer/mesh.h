#pragma once
/**
  * Mesh, a container for vertex + index data and a VertexBuffer.
  */

#include <memory>
#include "GL/gleks.h"
#include "VertexBuffer.h"
#include "Renderable.h"

class Mesh {
	public:	
		/**
		 * Copies the vertex & index data to internal buffers and creates
		 * a VBO with the data.
		 *
		 * format: vertex format to use
		 * vert_data: pointer to vertex data of the mesh
		 * vert_count: number of vertices
		 * index_data: pointer to _unsigned integer_ indices
		 * index_count: number of indices
		 * index_type: type of indices MUST BE GL_UNSIGNED_INT
		 */
		Mesh(eks::gl::VertexFormat format, const eks::gl::Vertex* vert_data, GLsizei vert_count, 
			const GLuint* index_data = nullptr, GLsizei index_count = 0, GLenum index_type = GL_UNSIGNED_INT);
		virtual ~Mesh();
		virtual void draw(GLenum draw_mode = GL_TRIANGLES) const;

	protected:
		VertexBuffer* vbo;
		GLenum mode; 
		eks::gl::Vertex* vertexData;
		eks::gl::VertexFormat format;

		GLsizei vertexCount; 
		GLuint* indexData;
		GLsizei indexCount; 
		GLenum indexType; 

	private:
		// Disallow copying and assignment.
		Mesh(const Mesh&);
		Mesh& operator=(const Mesh&);

		//virtual void renderFunction();
};