#pragma once
/*
	An OpenGL framebuffer object wrapper. Creates the given texture attachments in 
	constructor.

	FBO constructor example:

	FBO::Buffer buffers[]  = {
		{GL_RGB16F, GL_RGB, GL_UNSIGNED_INT, width, height}, // albedo
		{GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, width, height}, // normals, 8-bit unsigned normalized integer format
		{GL_RGB16F, GL_RGB, GL_UNSIGNED_INT, width, height}, // position
		{GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, width, height}
	};

	framebuffer = new FBO(width, height, buffers, 4);

	Depth attachment must come last in the given buffer list.
*/
#include "GL/gleks.h"

class FBO {
	public:
	struct Buffer {
		GLenum intFormat;
		GLenum extFormat;
		GLenum extFormatType; /** must match intFormat signedness */
		int width;
		int height;
	};

	// Switch back to window-system provided framebuffer.
	static void bindDefault(); 

	/*	buffers is an array of Buffer structures of size count.
		An optional depth buffer definition must be last in the given list.
		The generated textures use GL_LINEAR filtering and CLAMP_TO_EDGE wrapping. */
	FBO(int width, int height, const Buffer* buffers, int count);
	~FBO();

	void bind(GLenum target = GL_FRAMEBUFFER);
	GLuint getTextureID(int index);
	void clear(GLbitfield mask = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	void clearDepth();

	// Sets the OpenGL viewport to match FBO dimensions.
	void setViewport();
	GLuint getName() const;

	private:
	// disable copy constructor
	FBO(const FBO&); 
	// disable assignment operator
	FBO& operator=(const FBO&);
	void setDrawBuffers(const Buffer buffers[], int count);

	int textureCount;
	int width; // used for viewport calculation
	int height;
	GLuint fboName;
	GLuint* textures;
	GLenum* drawBuffers; // for render targets
};