#include <cstdio>
#include <string>
#include <Windows.h>
#include <Shlwapi.h>
#include <cstdint>
#include "math/PMurHash.h"
#include "system/filesystem.h"
#include "ShaderCache.h"
#include "ShaderProgram.h"

const std::string ShaderCache::cacheDir = "shadercache/"; // Must end in slash. Data path will be prepended with data-path.
std::string ShaderCache::fullCacheDir;

ShaderCache::ShaderCache()
{

}

ShaderCache::~ShaderCache()
{

}

void ShaderCache::initialize()
{
	LOG_INFO("Initializing shader cache.");
	fullCacheDir = fsys::getAssetSourceDir() + cacheDir;

	if (!PathFileExists(fullCacheDir.c_str())) {
		CreateDirectory(fullCacheDir.c_str(), NULL);
	}

	updateCache();
}

void ShaderCache::updateCache()
{
	filenames.clear();

	std::vector<std::string> D;
	fsys::listDirectoryFiles(cacheDir, D);

	for (auto noloo :D){
		filenames.insert(noloo);
	}
}

void ShaderCache::purge()
{
	std::vector<std::string> D;
	fsys::listDirectoryFiles(cacheDir, D);

	for (auto huspois :D){
		std::string path = fullCacheDir + huspois;
		DeleteFile(path.c_str());
	}
}

ShaderProgram* ShaderCache::load(const std::string& filename, const std::string& source)
{
	std::string cachedName; 

	if (!getCachedName(filename, source, &cachedName)) {
		return nullptr;
	}

	fsys::File binary(cacheDir + cachedName);
	return new ShaderProgram(binary.getData(), eks::gl::getProgramBinaryFormat());
}

bool ShaderCache::getCachedName(const std::string& filename, const std::string& source, std::string* out_cachePath)
{
	std::string hash;
	calculateHash(source, &hash);
	std::string cachePath = makeFilename(filename, hash);

	*out_cachePath = cachePath;

	return filenames.find(cachePath) != filenames.end();
}

void ShaderCache::saveShader(const std::vector<unsigned char>& data, const std::string& filename, const std::string& source)
{
	std::string hash, filepath, fullpath;
	calculateHash(source, &hash);
	filepath = makeFilename(filename, hash);
	fullpath = fullCacheDir + filepath;

	FILE* fp = fopen(fullpath.c_str(), "wb");

	if (!fp) {
		LOG_ERROR("Couldn't save cached shader binary as '%s'!", fullpath.c_str());
		return;
	}

	fwrite(&data[0], 1, data.size(), fp);
	fclose(fp);

	filenames.insert(filename); 
}

void ShaderCache::calculateHash(const std::string& input, std::string* output)
{
	uint32_t hash = PMurHash32(1, &input[0], input.size());
	char hashname[128];
	sprintf_s(hashname, 128, "%x", hash);
	*output = std::string(hashname);
}

std::string ShaderCache::makeFilename(const std::string& filename, const std::string& hash)
{
	return filename + "_" + hash;
}