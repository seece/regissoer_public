#include "demoplayer.h"
#include "releaseplayer.h"
#include "window.h"
#include "system/system.h"
#include "demo.h"

ReleasePlayer::ReleasePlayer(Window& win) :
	DemoPlayer(win)
{

}

ReleasePlayer::~ReleasePlayer()
{

}

void ReleasePlayer::updateUI()
{
	if (currentDemo->getSong().getTime() >= currentDemo->getSong().getLength() - 0.01f) {
		window.exitLoop();
	}
}

void ReleasePlayer::updateSyncTime(double row)
{
	// we update the editor row here in debug mode, but
	// in release player there isn't really much to do
}

float ReleasePlayer::getTime()
{
	return static_cast<float>(eks::system::getTime());
}