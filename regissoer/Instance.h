#pragma once
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>
#include "mesh.h"
#include "Material.h"
#include "Renderable.h"
#include "SpaceNode.h"
#include "AssetMap.h"


class Instance : public Renderable, ISpaceNode {
	public:
	Instance();
	Instance(const Instance& other);
	Instance(Mesh* mesh_pointer, Material* material_pointer);
	Instance(Mesh* mesh_pointer, Material* material_pointer, glm::vec3 position, glm::vec3 scale, glm::quat orientation);
	Instance(Mesh* mesh_pointer, Material* material_pointer, glm::vec3 position, glm::vec3 scale, glm::vec3 rotation_euler);
	virtual ~Instance();

	// Implement ISpaceNode interface
	virtual void setPosition(const glm::vec3& newpos);
	virtual void setScale(const glm::vec3& newscale);
	virtual void setOrientation(const glm::quat& neworientation);
	virtual glm::vec3 getPosition() const;
	virtual glm::vec3 getScale() const;
	virtual glm::quat getOrientation() const;
	virtual glm::mat4x4& getTransformation();

	Material* getMaterial();
	Mesh* getMesh();
	void setMesh(Mesh* mesh_pointer);
	void setMaterial(Material* material_pointer);

	void swap (Instance& other) throw ();
	Instance& operator=(Instance other);

	private:
	void initDefaults();
	void updateTransformation();

	// for Renderable
	virtual void renderFunc(); 
	virtual Material* getRenderMaterialFunc();
	virtual glm::mat4x4& getRenderTransformationFunc();
	bool isVisibleFunc();
	void cacheMaterialName(Material* mat);

	Mesh* mesh;
	Material* material;
	std::string materialname;

	glm::vec3 position;
	glm::vec3 scale;
	glm::quat orientation;
	glm::mat4 transformation;
	bool visible;
};