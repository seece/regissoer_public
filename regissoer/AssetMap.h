/*
	An asset container template class and implementations for each asset type.

	Basically just an over-engineered string -> asset map.
*/
#pragma once

#include <memory>
#include <unordered_map>
#include <typeinfo> // for typeid
#include "system/logger.h"
#include "Texture.h"
#include "Material.h"
#include "mesh.h"

template <class T>
class AssetMap {
public:
	//AssetMap(std::string dataDirectory) : dataDirectory(dataDirectory) {};
	AssetMap() {};
	virtual ~AssetMap() {};

	// Transfers ownership of an unique_ptr to this map.
	void handOver(std::string name, std::unique_ptr<T> entry)
	{
		entries[name] = std::move(entry);
	}

	// Takes the ownership of the given pointer.
	void handOver(std::string name, T* entry)
	{
		entries[name] = std::unique_ptr(entry);
	}

	T* get(const std::string& name)
	{
		if (entries.find(name) == entries.end()) {
			LOG_ERROR("Couldn't find asset %s of type %s", name.c_str(), typeid(T).name());
		}

		return entries[name].get();
	}

	bool exists(std::string name)
	{
		return entries.find(name) != entries.end();
	}

	T& operator[](const std::string& name)
	{
		return *get(name);
	}

	int loadAssets()
	{
		std::vector<std::string> files;
		fsys::listDirectoryFiles(dataDirectory, files);

		int loaded = 0;

		LOG_DEBUG("Loading assets from %s", dataDirectory.c_str());

		for (auto& file: files) {
			if (load(file))
				loaded++;
		}

		return loaded;
	}

	// Implement this in derived classes.
	virtual bool load(const std::string& file) = 0;

	std::unordered_map<std::string, std::unique_ptr<T>> entries;
	static const std::string dataDirectory; // This is specialized in AssetMap.cpp for each container class.
};

class TextureMap : public AssetMap<Texture> {
public:
	int loadTextureAssets();
protected:
	virtual bool load(const std::string& file);
};

class MaterialMap : public AssetMap<Material> {
public:
	// Takes the ownership of the given pointer, and reads the name straight from the material struct.
	void handOver(Material* entry);
protected:
	virtual bool load(const std::string& file);
};

class ShaderMap: public AssetMap<Shader> {
public:
	// Loads a shader from the /shaders data directory.
	// Filename should be e.g. "shiny.glsl"
	void reloadShaders(bool force_reload = false);
protected:
	virtual bool load(const std::string& file);
};

class MeshMap: public AssetMap<Mesh> {
public:
protected:
	virtual bool load(const std::string& file);
};