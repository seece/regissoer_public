#pragma once
/* A logger class used for global application logging.
 * Includes multiple inputs for different log levels. 
 */
#include <cstdarg>
#include <memory>

class LogSink {
	public:
	LogSink(const char* filename);
	virtual ~LogSink();
	void info(const char* format, ...);
	void debug(const char* format, ...); // messages printed only when compiled in Debug-mode.
	void warn(const char* format, ...); 
	void error(const char* format, ...);

	protected:
	enum ConsoleColor { DARKBLUE = 1, DARKGREEN, DARKTEAL, DARKRED, DARKPINK, DARKYELLOW, GRAY, DARKGRAY, BLUE, GREEN, TEAL, RED, PINK, YELLOW, WHITE };
	void setColor(ConsoleColor color);
	ConsoleColor lastColor;

	void write(const char* format, va_list argptr, FILE* output = stderr, bool messagebox = false);
	void openConsoleWindow();
	void* hCon; // HANDLE == void*
};

namespace eks {
	// Global logger object that will be instantiated by the main program.
	// Allocated on the heap since this allows us to control its lifetime
	// in a more fine-grained manner.
	extern std::unique_ptr<LogSink> logger;
}

#define LOG_INFO(...) eks::logger->info(__VA_ARGS__)
#define LOG_DEBUG(...) eks::logger->debug(__VA_ARGS__)
#define LOG_WARN(...) eks::logger->warn(__VA_ARGS__)
#define LOG_ERROR(...) eks::logger->error(__VA_ARGS__)