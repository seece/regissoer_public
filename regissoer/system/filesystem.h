#pragma once

#include <string>
#include <vector>

namespace fsys {
// Currently only SOURCE_DIR is used.
enum FileSourceType {SOURCE_DIR = 0, SOURCE_ZIP = 1};

extern fsys::FileSourceType assetSourceType;
extern std::string assetSourceName;

void setAssetSource(FileSourceType type, const std::string& sourceName);
const std::string& getAssetSourceDir();

// Lists files from asset directory */
void listDirectoryFiles(const std::string& path, std::vector<std::string>& entries);
// Lists files from directory without using the datapath. */
void listRootDirectoryFiles(const std::string& path, std::vector<std::string>& entries);
// Lists directories from asset directory */
void listDirectoryDirs(const std::string& path, std::vector<std::string>& entries);

// Returns the basename of a file path, e.g. "game" in "files/game.exe"
std::string extractBasename(const std::string& path);

// Prepends the current data directory to the given path
const std::string buildDataPath(const std::string& path);

class File {
public:
	File(const std::string& path);
	//File(const char * path);
	~File();
	const void print();

	// Reloads the file from disk 
	long reload();

	// Returns the file data as a NUL-terminated C-string 
	char* c_str();

	// Returns the basename of the file, e.g. "article" in "data/article.txt" 
	const std::string getBasename();

	// Checks if the current modified date is newer than the one saved on last reload.
	bool beenModified();

	// Last modification date as a 64-bit unsigned integer.
	unsigned long long getLastModified();
	std::string getFilePath();
	std::string getFullPath();
	char* getData();
	long getSize();

private:
	void load(const char * path);
	char* stringdata;	/** contains the data as a C-string, default: NULL */
	void updateLastModified();

	unsigned long long lastModified;
	char* data;
	long size;
	std::string filepath;
	std::string fullpath;
};

};
