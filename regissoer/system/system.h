#pragma once

namespace eks {
namespace system {
	void initializeSystem();
	void audioInit();
	void cleanupSystem();
	unsigned long long getMillisecs();
	double getTime();	
	bool debugModeEnabled();
	void setMainWindowHandle(void* hwnd);
	void* getMainWindowHandle();
	int getAudioLatency(); // Returns the BASS audio latency in milliseconds
}
}