#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <cstdint>
#include <Windows.h>
#include "../util.h"
#include "filesystem.h"
#include "tinydir.h"
#include "logger.h"

using eks::logger;

namespace fsys {
FileSourceType fsys::assetSourceType = fsys::SOURCE_DIR;
std::string fsys::assetSourceName = "";

void setAssetSource(FileSourceType type, const std::string& sourceName)
{
	assetSourceType = type;
	assetSourceName = sourceName;
}

const std::string& getAssetSourceDir()
{
	return assetSourceName;
}

const std::string fsys::buildDataPath(const std::string& path)
{
	return fsys::assetSourceName + path;
}

std::string fsys::extractBasename(const std::string& path)
{
	char drive[_MAX_DRIVE]; 
	char dir[_MAX_DIR];
	char basename[_MAX_FNAME];
	char extension[_MAX_EXT];
	_splitpath(path.c_str(), drive, dir, basename, extension);

	return std::string(basename);
}

File::File(const std::string& path) :
	filepath(path),
	stringdata(nullptr),
	size(0),
	lastModified(0),
	fullpath(buildDataPath(filepath))
{

	if (fsys::assetSourceType != fsys::SOURCE_DIR) {
		LOG_ERROR("Invalid file source type!");
	}

	load(fullpath.c_str());
}

namespace {
enum ListingType {
	LIST_ALL,
	LIST_FILES,	
	LIST_DIRS
};

void listDirectoryContents(const std::string& path, ListingType type, std::vector<std::string>& entries) {
	tinydir_dir dir;
	int result = tinydir_open(&dir, path.c_str());

	if (result == -1) 
		LOG_ERROR("Couldn't open directory %s for listing", path.c_str());

	while (dir.has_next) {
		tinydir_file file;
		tinydir_readfile(&dir, &file);

		if ((!file.is_dir) && ((type == LIST_FILES) || (type == LIST_ALL))) 
			entries.push_back(std::string(file.name));

		if (file.is_dir && ((type == LIST_DIRS) || (type == LIST_ALL))) 
			entries.push_back(std::string(file.name));

		tinydir_next(&dir);
	}

	tinydir_close(&dir);
}
}

void listDirectoryFiles(const std::string& path, std::vector<std::string>& entries)
{
	listDirectoryContents(buildDataPath(path), LIST_FILES, entries);
}

void listRootDirectoryFiles(const std::string& path, std::vector<std::string>& entries)
{
	listDirectoryContents(path, LIST_FILES, entries);
}

void listDirectoryDirs(const std::string& path, std::vector<std::string>& entries)
{
	listDirectoryContents(buildDataPath(path), LIST_DIRS, entries);
}



void File::load(const char * path) 
{
	size_t result;
	data = nullptr;
	size = 0;

	FILE * fp;
	fp = fopen(path, "rb");

	if (fp == nullptr) {
		LOG_ERROR("Couldn't open file %s", path);
		return;
	}

	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);
	rewind(fp);

	data = new char[size];
	if (data == nullptr) {
		LOG_ERROR("Memory error when loading file %s", path);
		delete[] data;
		data = nullptr;
		fclose(fp);
		return;
	}

	result = fread(data,1,size,fp);

	if (result != size) {
		LOG_INFO("Warning: only read %u / %u bytes from %s", result, size, path);
	}

	fclose(fp);

	updateLastModified();
}

File::~File() 
{
	if (stringdata != nullptr) {
		delete[] stringdata;
	}

	if (data == nullptr || size == 0) {
		return;
	}

	delete this->data;
}

char* File::getData()
{
	return data;
}

long File::getSize()
{
	return size;
}

const void File::print() 
{
	if (this->data == nullptr || this->size == 0) {
		return;
	}

	for (int i=0;i<size;i++) {
		std::cout << data[i];
	}
}

char* File::c_str()
{
	if (stringdata == nullptr) {
		stringdata = new char[this->size + 1];
		memcpy(stringdata, data, size);
		stringdata[size] = '\0';
	}

	return stringdata;
} 

const std::string File::getBasename()
{
	char drive[_MAX_DRIVE]; 
	char dir[_MAX_DIR];
	char basename[_MAX_FNAME];
	char extension[_MAX_EXT];
	_splitpath(filepath.c_str(), drive, dir, basename, extension);

	return std::string(basename);
}

long File::reload()
{
	if (data != nullptr) {
		delete[] data;	
		data = nullptr;
	}

	if (stringdata != nullptr) {
		delete[] stringdata;
		stringdata = nullptr;
	}

	load(fullpath.c_str());

	return size;
}

void File::updateLastModified()
{
	HANDLE handle  = CreateFile(fullpath.c_str(),
		GENERIC_READ,
		FILE_SHARE_READ,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (handle == INVALID_HANDLE_VALUE) {
		LOG_ERROR("Couldn't open file %s for read in updateLastModified", filepath.c_str());			
		return;
	}

	FILETIME modTime;
	GetFileTime(handle, NULL, NULL, &modTime);
	CloseHandle(handle);

	// the 64 bit integer is returned in two parts, so we need to join them
	// TODO clean this up
	uint64_t last = 0;
	last |= ((static_cast<uint64_t>(modTime.dwHighDateTime)) << 32ULL) & 0xFFFFFFFF00000000ULL;
	last |= modTime.dwLowDateTime & 0x00000000FFFFFFFFULL;

	lastModified = last;
}

unsigned long long File::getLastModified()
{
	updateLastModified();
	return lastModified;
}

bool File::beenModified()
{
	unsigned long long last = lastModified;	
	updateLastModified();

	if (lastModified > last) {
		return true;
	}

	return false;
}

std::string File::getFilePath()
{
	return filepath;
}

std::string File::getFullPath()
{
	return fullpath;
}
}
