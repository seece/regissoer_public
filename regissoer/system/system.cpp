#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN

#include <Windows.h>
#include <bass.h>
#include "system.h"
#include "logger.h"

namespace {
	LARGE_INTEGER programStartTime;
	LARGE_INTEGER frequency;
	HWND mainWindow = NULL;
	int bassAudioLatency = 0;
}

void eks::system::initializeSystem()
{
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&programStartTime);

	srand(static_cast<unsigned int>(programStartTime.QuadPart));

	audioInit();
}

void eks::system::cleanupSystem()
{
	BASS_Free();
}

double eks::system::getTime()
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	return (now.QuadPart - programStartTime.QuadPart) / static_cast<double>(frequency.QuadPart);
}

unsigned long long eks::system::getMillisecs()
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	return (now.QuadPart - programStartTime.QuadPart)*1000 / frequency.QuadPart;
}

void eks::system::audioInit()
{
	// TODO use proper window handle here instead of NULL?
	if (!BASS_Init(-1, 44100, BASS_DEVICE_LATENCY, 0, NULL)) {
		LOG_ERROR("Couldn't initialize BASS. Error: %i", BASS_ErrorGetCode());
	}

	int version = BASS_GetVersion();

	if (HIWORD(version)!=BASSVERSION) {
		LOG_ERROR("Wrong version of BASS.DLL loaded (0x%x != 0x%x)", HIWORD(version), BASSVERSION);
	} else {
		LOG_INFO("Using BASS v. 0x%x", version);
	}

	BASS_INFO info;
	BASS_GetInfo(&info);

	LOG_INFO("Audio latency: %u ms", info.latency);
	bassAudioLatency = (int)info.latency;
}

bool eks::system::debugModeEnabled()
{
	#ifdef _DEBUG
		return true;
	#else
		return false;
	#endif
}

void eks::system::setMainWindowHandle(void* hwnd)
{
	mainWindow = static_cast<HWND>(hwnd);
}

void* eks::system::getMainWindowHandle()
{
	return mainWindow;
}

int eks::system::getAudioLatency()
{
	return bassAudioLatency;
}