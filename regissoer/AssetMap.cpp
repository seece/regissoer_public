#include <memory>
#include "system/filesystem.h"
#include "system/logger.h"
#include "AssetMap.h"
#include "mesh.h"
#include "objloader.h"
#include "extra/eks_debug.h"

const std::string AssetMap<Texture>::dataDirectory = "textures/";
const std::string AssetMap<Material>::dataDirectory = "materials/";
const std::string AssetMap<Shader>::dataDirectory = "shaders/";
const std::string AssetMap<Mesh>::dataDirectory = "meshes/";


// TextureMap


int TextureMap::loadTextureAssets()
{
	std::string textureDir = std::string("textures/");
	std::vector<std::string> files;
	fsys::listDirectoryFiles(textureDir, files);

	int loaded = 0;

	LOG_DEBUG("Loading texture assets from %s", textureDir.c_str());

	for (auto& file: files) {
		Texture* textureptr = new Texture(textureDir + file);
		handOver(textureptr->getBasename(), std::unique_ptr<Texture>(textureptr));
		loaded++;
	}

	return loaded;
}

bool TextureMap::load(const std::string& file)
{
	Texture* textureptr = new Texture(dataDirectory + file);
	handOver(textureptr->getBasename(), std::unique_ptr<Texture>(textureptr));

	return true;
}


// MaterialMap


bool MaterialMap::load(const std::string& file)
{
	// TODO implement material system
	LOG_ERROR("New material system not implemented yet.");
	return false;
}

// Takes the ownership of the given pointer.
void MaterialMap::handOver(Material* entry)
{
	entries[entry->name] = std::unique_ptr<Material>(entry);
}


// ShaderMap


bool ShaderMap::load(const std::string& file)
{
	std::string filename = dataDirectory + file;

	Shader* shader = nullptr;
	LOG_INFO("Loading shader: %s", filename.c_str());

	// In debug mode we continuosly try to reload the shader until it compiles.
	#ifdef _DEBUG
	while (true) {
		if (shader)
			delete shader;

		shader = new Shader(filename);

		if (shader->program->isValid()) {
			break;
		} else {
			// Warning waits for keypress.
			LOG_WARN("Invalid shader %s", filename.c_str());
		}
	}
	#else
		shader = new Shader(filename);
	#endif

	std::string basename = shader->getBasename();
	entries[basename] = std::unique_ptr<Shader>(shader);

	return true;
}

void ShaderMap::reloadShaders(bool force_reload /* = false */)
{
	for (auto it = entries.begin(); it != entries.end(); ++it) {
		// LOG_INFO("Reloading shader %s", it->first.c_str());
		it->second->reload(force_reload);
	}
}

// MeshMap

bool MeshMap::load(const std::string& file)
{
	Mesh* ptr;
	EKS_MEASURE(
		ptr = eks::util::loadWavefrontObj(dataDirectory + file);
	);

	if (!ptr)
		return false;

	handOver(fsys::extractBasename(file), std::unique_ptr<Mesh>(ptr));

	return true;
}