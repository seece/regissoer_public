#define WIN32_LEAN_AND_MEAN
#define WIN32_EXTRA_LEAN

#include <string>
#include <memory>
#include <Windows.h>
#include "window.h"
#include "Input.h"
#include "GL/gleks.h"
#include <SDL.h>
#include <SDL_syswm.h>
//#include "Vertex.h"
#include "GLDebug.h"
#include "system/logger.h"
#include "system/system.h"

eks::launcher::LauncherFunc_t eks::launcher::launcherPointer = nullptr;

Window::LaunchSettings eks::launcher::inquireLaunchSettings(std::string title)
{
	if (eks::launcher::launcherPointer == nullptr) {
		Window::Settings defaults;
		defaults.fullscreen = false;
		defaults.width = 1280;
		defaults.height = 720;
		defaults.vsync = true;
		defaults.title = title;
		Window::LaunchSettings launch = {defaults, true};
		return launch;
	}

	Window::LaunchSettings set = eks::launcher::launcherPointer();
	set.win.title = title;

	return set;
}


void checkSDLError(int line = -1)
{
#ifndef NDEBUG
        const char *error = SDL_GetError();
        if (*error != '\0')
        {
                printf("SDL Error: %s\n", error);
                if (line != -1)
                        printf(" + line: %i\n", line);
                SDL_ClearError();
        }
#endif
}

Window::Window(Window::Settings& windowSettings) :
	appRunning(true),
	settings(windowSettings),
	mainWindow(nullptr),
	player(nullptr)
{
	init();
	printGraphicsInfo();
}

Window::Window(Window::LaunchSettings& launchSettings) :
	appRunning(true),
	settings(launchSettings.win),
	mainWindow(nullptr),
	player(nullptr)
{
	if (!launchSettings.run)
		exit(0); // phew

	init();
}

void Window::init()
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
		LOG_ERROR("Can't initialize SDL!");
		return;
	}

	input = std::unique_ptr<Input>(new Input());

	initGL(settings);

	if (settings.fullscreen)
		SDL_ShowCursor(0);

	SDL_SysWMinfo info;
	if (SDL_GetWindowWMInfo(mainWindow, &info)) {
		eks::system::setMainWindowHandle(info.info.win.window);
	}
	//logger::systemSpecs = getGPUInfoString();
	currentSettings = settings;
}

Window::~Window()
{
	SDL_GL_DeleteContext(mainContext);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
}

void Window::initGL(Window::Settings config)
{
	// Create a OpenGL 4.2 core profile context
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	#ifdef _DEBUG
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	#endif

	Uint32 windowflags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

	if (config.fullscreen) {
		windowflags |= SDL_WINDOW_FULLSCREEN;
	}

	LOG_INFO("Creating SDL window");
	mainWindow = SDL_CreateWindow(config.title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		config.width, config.height, windowflags);

	if (!mainWindow)	
		LOG_ERROR("Unable to create a window");

	checkSDLError(__LINE__);

	LOG_INFO("Creating GL context");
	mainContext = SDL_GL_CreateContext(mainWindow);
	checkSDLError(__LINE__);

	// VSYNC
	SDL_GL_SetSwapInterval(config.vsync ? 1 : 0);
	LOG_INFO("VSYNC: %s", config.vsync ? "on" : "off");
		
	int loadTest = ogl_LoadFunctions();
	if(loadTest == ogl_LOAD_FAILED) {
		LOG_ERROR("Couldn't init OpenGL functions!");
	}

	openGLInfo = std::string(reinterpret_cast<const char*>(glGetString(GL_VERSION)));
	GLSLInfo = std::string(reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION)));
	openGLVendorInfo = std::string(reinterpret_cast<const char*>(glGetString(GL_VENDOR)));

	LOG_INFO("OpenGL %s, GLSL %s\n%s\n", glGetString(GL_VERSION),
		   glGetString(GL_SHADING_LANGUAGE_VERSION),
		   glGetString(GL_VENDOR));

	#ifdef _DEBUG
	#ifndef _DEBUG_NO_GL
	eks::gl::setupDebugOutput();
	#endif
	#endif

	if (glext_ARB_get_program_binary != 1) {
		LOG_ERROR("ARB_get_program_binary not supported.");
	}

	/*
	if (glext_EXT_direct_state_access != 1) {
		LOG_ERROR("EXT_direct_state_access not supported.");
	}
	*/
}

// Calls the player's draw method if possible, and flips the buffers.
void Window::draw()
{
	if (!player) return;

	player->update(); // in theory this could be called at different times than draw
	player->draw();
	SDL_GL_SwapWindow(mainWindow);
}

void Window::cleanup()
{

}


void Window::registerPlayer(DemoPlayer* player)
{
	if (!player) 
		LOG_ERROR("Trying to register a NULL player pointer!");

	this->player = player;
}

bool Window::keyDown(int key)
{
	return input->keyDown(key);
}

bool Window::keyHit(int key)
{
	return input->keyHit(key);
}

bool Window::processEvents()
{
	input->processEvents();
	return appRunning;
}

Input::MousePosition Window::mousePos()
{
	return input->mousePos();
}

bool Window::mouseDown(int button)
{
	return input->mouseDown(button);
}

bool Window::mouseHit(int button)
{
	return input->mouseHit(button);
}

void Window::exitLoop()
{
	appRunning = false;
}

void Window::messageBox(std::string& message)
{
	MessageBox(GetActiveWindow(), message.c_str(), "Fail", MB_OK | MB_ICONWARNING);	
}

std::string Window::getGPUInfoString()
{
	return std::string("OpenGL: ") 
		+ openGLInfo 
		+ std::string("\r\n GLSL: ") 
		+ GLSLInfo
		+ std::string("\r\n Vendor: ")
		+ openGLVendorInfo 
		+ std::string("\r\n");
}

void Window::printGraphicsInfo()
{
	GLint texture_units;
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &texture_units);

	LOG_DEBUG("Max texture units: %d", texture_units);
}

int Window::getWidth()
{
	return settings.width;
}

int Window::getHeight()
{
	return settings.height;
}

const Input* Window::getInput() const
{
	return input.get();
}