#include <sstream>
#include <fstream>
#include <cctype>
#include "preprocessor.h"
#include "system/logger.h"
#include "system/filesystem.h"

#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Shlwapi.h> // PathRemoveFileSpec  

using eks::logger;
using std::string;

ShaderPreprocessor::ShaderPreprocessor()
{
}

ShaderPreprocessor::~ShaderPreprocessor()
{
}

namespace {
int findFirstWord(const std::string& needle, const std::string& haystack)
{
	int start = -1;
	int needle_pos = 0;

	char whitespace[] = {' ', '\t'};
	for (int i = 0 ; i < haystack.size() ; ++i) {
		char c = haystack[i];

		// Skip leading whitespace
		if (needle_pos == 0 && isspace(c)) {
			continue;
		}

		if (c == needle[needle_pos]) {
			// Mark first matching char
			if (needle_pos == 0)
				start = i;

			needle_pos++;

			if (needle_pos == needle.size()) {
				return start;
			}
		} else {
			break;
		}
	}

	return -1;
}

// Reads the contents a quoted string. Leading whitespace is skipped.
string readSimpleString(int startIndex, const string& input)
{
	int start = -1;
	int len = 0;

	for (int i = startIndex ; i < input.size() ; ++i) {
		char c = input[i];

		if (start == -1) {
			// skip leading whitespace
			if (isspace(c))
				continue;

			// try to find start
			if (c == '"') {
				start = i+1;
			}
		} else {
			// try to find end
			if (c == '"') {
				break;
			}

			len++;
		}
	}

	if (start == -1)
		return "";

	return input.substr(start, len);
}

// path/file.txt --> path/
bool removeFileFromPath(string& str) 
{
	int firstslash = -1;

	for (int i = str.size()-1; i >= 0 ; --i) {
		char c = str[i];

		if (c == '/' || c == '\\') {
			firstslash = i;
			str.resize(i + 1);

			return true;
		}
	}

	return false;
}
}

void ShaderPreprocessor::processAndAdd(const string& path, string& target)
{
	string filepath = fsys::buildDataPath(path);
	string filedir = path;
	if (!removeFileFromPath(filedir)) {
		LOG_ERROR("Couldn't parse remove file name from shader path '%s'", filedir.c_str());
	}

	std::ifstream file(filepath);

	if (!file) {
		LOG_ERROR("Couldn't open shader file '%s' for reading.", path.c_str());
		return;
	}

	string line;
	const string include_command = "#include";
	int linenum = 0;

	while (std::getline(file, line)) {
		linenum++;
		int includestart = findFirstWord(include_command, line);

		if (includestart >= 0) {
			string newpath = readSimpleString(includestart + include_command.size(), line);
			//line = "// " + line;
			if (newpath.length() == 0) {
				LOG_ERROR("Couldn't parse #include in shader '%s' at line %d", path.c_str(), linenum);
				continue;
			}

			processAndAdd(filedir + newpath, target);
		} else {
			// No include, just concatenate the line.
			target += line;
			target += "\r\n"; // the newline is not stored in line
		}
	}
}

void ShaderPreprocessor::process(string path, string& data)
{
	data.clear();
	data.reserve(10000);

	processAndAdd(path, data);
}
