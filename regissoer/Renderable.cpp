#include "Renderable.h"

void Renderable::render()
{
	renderFunc();
}

Material* Renderable::getRenderMaterial()
{
	return getRenderMaterialFunc();
}

glm::mat4x4& Renderable::getRenderTransformation()
{
	return getRenderTransformationFunc();
}

bool Renderable::isVisible()
{
	return isVisibleFunc();
}