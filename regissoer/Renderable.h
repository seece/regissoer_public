#pragma once

#include <glm/mat4x4.hpp>
#include "Material.h"

class Renderable {
	public:
	// The public interface that stays the same
	void render();
	Material* getRenderMaterial();
	glm::mat4x4& getRenderTransformation();
	bool isVisible();

	private:
	// the actual render function implementation that gets overridden
	virtual void renderFunc() = 0;
	virtual Material* getRenderMaterialFunc() = 0;
	virtual glm::mat4x4& getRenderTransformationFunc() = 0;
	virtual bool isVisibleFunc() = 0;
};