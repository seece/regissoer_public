#include <SOIL.h>
#include "GL/gleks.h"
#include "GLDebug.h"
#include "Texture.h"
#include "system/filesystem.h"
#include "system/Logger.h"

using eks::logger;

Texture::Texture(std::string path, bool flip_y, bool mipmapped /* = true */, GLint internalformat) : 
	TextureBase(path)
{
	LOG_INFO("Loading texture '%s'", path.c_str());
	fsys::File imageFile(path);

	int soil_format;
	GLenum format = GL_RGBA;
	pickFormat(internalformat, &format, &soil_format);
	
	int width, height;
	unsigned char* image = SOIL_load_image_from_memory(
		(const unsigned char*) imageFile.getData(),
		imageFile.getSize(),
		&width,
		&height,
		0,
		soil_format	
		);

	if (!image) {
		LOG_ERROR("SOIL: Couldn't load image %s", path.c_str());
	}

	texture_width = width;
	texture_height = height;

	LOG_DEBUG("SOIL: %d,0x%x, %s = %s %dx%d", soil_format, format, basename.c_str(), path.c_str(), width, height);

	if (flip_y) {
		TextureBase::flip_image(image, width, height, soil_format == SOIL_LOAD_RGBA ? 4 : 3);
	}

	glGenTextures(1, &texID);
	eks::gl::assertGlError("Couldn't fetch new texture name");
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	if (mipmapped)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	else
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	eks::gl::assertGlError("Couldn't set texture parameters");

	glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, GL_UNSIGNED_BYTE, image);

	if (mipmapped) {
		glGenerateMipmap(GL_TEXTURE_2D);
		eks::gl::assertGlError("Couldn't generate mipmaps");
	}

	SOIL_free_image_data(image);

	eks::gl::assertGlError("Couldn't make texture");
}

Texture::~Texture()
{
	glDeleteTextures(1, &texID);
}

void Texture::activate(GLenum unit)
{
	glActiveTexture(unit);
	glBindTexture(GL_TEXTURE_2D, texID);
}
