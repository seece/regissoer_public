/** 
 * The window singleton class. Takes care of OpenGL initialization and keyboard/mouse I/O.
 */
#pragma once

#include <memory>
#include <string>
#include <SDL.h>
#include "Input.h"
#include "demoplayer.h"

class Window {
	public:
		struct Settings {
			int width;
			int height;
			bool fullscreen;
			bool vsync;
			std::string title;

			Settings() {};
			Settings(int width, int height, bool fullscreen, bool vsync, std::string title)
				: width(width), height(height), fullscreen(fullscreen), vsync(vsync), title(title)
				{};
		};

		struct LaunchSettings {
			Settings win;
			bool run;
		};

		Window(Settings& windowSettings);
		Window(LaunchSettings& launchSettings);
		~Window();
		int getWidth();
		int getHeight();
		void registerPlayer(DemoPlayer* player);
		bool keyDown(int key);
		bool keyHit(int key);

		Input::MousePosition mousePos();
		bool mouseDown(int button);
		bool mouseHit(int button);
		// Returns true if the app is still running, false if execution has ended.
		bool processEvents();
		void draw();
		void exitLoop();
		void messageBox(std::string& message);

		std::string getGPUInfoString();
		static Settings currentSettings; // read the screensize here
		const Input* getInput() const;
	private:
		void init();
		void initGL(Window::Settings settings);
		void cleanup();
		void printGraphicsInfo();

		Settings settings;
		SDL_Window* mainWindow;
		SDL_GLContext mainContext;
		std::string openGLInfo;
		std::string openGLVendorInfo;
		std::string GLSLInfo;
		bool appRunning;

		DemoPlayer* player;
		std::unique_ptr<Input> input;
};


namespace eks {
namespace launcher {
	typedef Window::LaunchSettings (*LauncherFunc_t)(); // launcher function pointer type
	extern LauncherFunc_t launcherPointer; // nullptr by default
	Window::LaunchSettings inquireLaunchSettings(std::string title);
}
}