SETLOCAL EnableDelayedExpansion
pushd %~dp0
set GET=call get.js.bat
mkdir tools 2> NUL
if not exist tools\wget.exe     %GET% http://cce.kapsi.fi/buildtools/bin/gnu/wget.exe     tools\wget.exe 
if not exist tools\unzip.exe    %GET% http://cce.kapsi.fi/buildtools/bin/gnu/unzip.exe    tools\unzip.exe 
popd

exit /B 0

