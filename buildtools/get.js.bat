@set @junk=1 /*
@echo off
cscript //nologo //E:jscript %0 %0 %*
goto :eof
*/

getFilename = function (path) {
	return path.substr(path.lastIndexOf("/") + 1, path.length);
}

function deleteIfExists(file) {
	fso = new ActiveXObject("Scripting.FileSystemObject");
	if (fso.FileExists(file)) {
		fso.DeleteFile(file);
	}
}

var http = (function (){
	var http = {};
	
	var send_request = function (url) {
		var WinHttpReq = new ActiveXObject("WinHttp.WinHttpRequest.5.1");
		WinHttpReq.Open("GET", url, /*async=*/false);
		WinHttpReq.Send();
		return WinHttpReq;
	}
	
	http.get = function (url) {
		var req = send_request(url);
		return req.ResponseText;
	}
	
	http.get_to_file = function (url, filepath) {
		var req = send_request(url);
		
		if (req.Status != 200) {
			throw "Couldn't GET " + url + "" + " Status: " + req.Status;
		}
		
		deleteIfExists(filepath);
		
		BinStream = new ActiveXObject("ADODB.Stream");
		BinStream.Type = 1;
		BinStream.Open();
		BinStream.Write(req.ResponseBody);
		BinStream.SaveToFile(filepath);
	}
	
	return http;
})();


if (WScript.Arguments.length < 2) {
	WScript.Echo("Usage: " + WScript.Arguments(0) + " URL [TARGET_FILE]");
	WScript.Echo("You can't overwrite files with this script.");
	WScript.Quit(1);
}

try {
	http.get_to_file(
		WScript.Arguments(1), 
		WScript.Arguments.length > 2 ? WScript.Arguments(2) : getFilename(WScript.Arguments(1))
	);
} catch (e) {
	WScript.Echo("ERROR: " + e);
	WScript.Quit(1);
}

WScript.Quit(0);