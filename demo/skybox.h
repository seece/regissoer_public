
#pragma once

#include <parametermap.h>
#include "effect.h"

void render_skybox(EffectInput in, Scene* scene, glm::vec3 light_dir, glm::vec3 light_col = glm::vec3(.6f, .3f, .2f), glm::vec3 dark_col = glm::vec3(.0f));
