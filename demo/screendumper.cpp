#include <cstdint>
#include <cmath>
#include <GL/gleks.h>
#include "screendumper.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <util/stb_image_write.h>

namespace {
// Ripped from SOIL
void flip_image(unsigned char* img, int width, int height, int channels)
{
	int i, j;
	for( j = 0; j*2 < height; ++j )
	{
		int index1 = j * width * channels;
		int index2 = (height - 1 - j) * width * channels;
		for( i = width * channels; i > 0; --i )
		{
			unsigned char temp = img[index1];
			img[index1] = img[index2];
			img[index2] = temp;
			++index1;
			++index2;
		}
	} 
}

void insert_gamut(uint8_t* pixels, int width, int height, int channels)
{
	int size = 32;
	float step = 256.0f/float(size);

	for (int b=0;b<size;++b) {
	for (int r=0;r<size;++r) {
	for (int g=0;g<size;++g) {
		uint8_t rr = (uint8_t)floor(r*step + 0.5f); 
		uint8_t gg = (uint8_t)floor(g*step + 0.5f);
		uint8_t bb = (uint8_t)floor(b*step + 0.5f);

		int index = g*width*channels + (b*size)*channels + r*channels;

		pixels[index++] = rr;
		pixels[index++] = gg;
		pixels[index++] = bb;
	}
	}
	}

}
}


int dumpScreenshot(int width, int height, const std::string& path, bool save_gamut)
{
	int pixelcount = width * height * 3;
	uint8_t* pixels = new uint8_t[pixelcount];

	glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixels);

	flip_image(pixels, width, height, 3);

	if (save_gamut)
		insert_gamut(pixels, width, height, 3);
	//int stbi_write_png(char const *filename, int w, int h, int comp, const void *data, int stride_in_bytes);
	int result = stbi_write_png_desu(path.c_str(), width, height, 3, pixels, width*3);

	delete [] pixels;

	return result;
}
