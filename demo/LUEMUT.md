## Pikanäppäimet
* `F1` printtaa kaikki käytössä olevat Tcl-komennot terminaaliin
* `F5` lataa shaderit uusiksi
* `F6` lataa parametrit ja niiden filtterit uusiksi (`scripts/params.tcl` ja `scripts/param_mods.tcl`)
* `F9` näyttää ruudulla shadowmapin syvyyskartan

Seekkaus ja muu hoidetaan rocketista.

## Parametrit

Ohjelman käyttämät parametrit on listattu tiedostossa `scripts/params.tcl`. Nimilista lähetetään automaattisesti GNU Rocketille. Voit ladata listan lennossa uusiksi `F6`-näppäimellä.

Parametrit näkyvät C++-koodille ´params´-taulun jäseninä.

Kaikki parametrit jotka ovat muotoa `p_muuttuja` lähetetään myös uniformeina kaikille shadereille. Shaderissa pitää tietenkin esitellä käytettävä muuttuja ennen käyttöä, esim. `uniform float p_lame_rgbdistort;`. 


## Shaderit
Kaikki shaderit includettavat `uniforms.glsl` ja `utils.glsl` tiedostot. 

### Uudelleenlataus
Paina `F5`. Vain muuttuneet shadertiedostot ladataan. *HUOM* `#include`:lla ladattujen tiedostojen muutoksia ei huomata. 