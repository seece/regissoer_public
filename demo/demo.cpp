#define GLM_FORCE_RADIANS
#pragma warning(disable:4503) // decorated name length exceeded

#include <cassert>
#include <ctime>
#include <memory>
#include <SDL.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_query.hpp>
#include <glm/trigonometric.hpp>
#include <glm/ext.hpp>
#include <extra/eks_debug.h>
#include <objloader.h>
#include <pipeline.h>
#include <preprocessor.h>
#include <texturebase.h>
#include <system/logger.h>
#include <system/system.h>
#include <Renderable.h>
#include <Assets.h>
#include "demo.h"
#include "effect.h"
#include "screendumper.h"

#include <mesh/cubeMesh.h>
#include <mesh/quadMesh.h>

#include "effect_helpers.h"

using eks::gl::assertGlError;
Window::Settings Window::currentSettings; 

namespace eks {
	typedef void (*RenderFunc_t)(Pipeline&, ParameterMap&);
}

// Uses a different size scale than drawOverlay, this one is in absolute screen coords.
void Demo::drawOverlayTexture(GLuint textureID, float x, float y, float width, float height, std::string shadername)
{
	Shader& overlay = assets::shaders[shadername];
	overlay.program->use();

	glActiveTexture(GL_TEXTURE0 + 7);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glUniform1i(overlay.program->getUniformLocation("tex"), 7);

	glm::mat4x4 transf(1.0f);
	transf = glm::translate(transf, glm::vec3(x, y, 0.0f));
	transf = glm::scale(transf, glm::vec3(width, height, 1.0f));
	
	GLuint modelLoc = overlay.program->getUniformLocation("modelMatrix");
	glUniformMatrix4fv(modelLoc, 1, false, glm::value_ptr(transf));
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	assets::meshes["quad"].draw();
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

void Demo::drawOverlay(TextureBase& tex, float x, float y, float width, float height, std::string shadername)
{
	Shader& overlay = assets::shaders[shadername];
	overlay.program->use();

	tex.activate(GL_TEXTURE0 + 7);
	glUniform1i(overlay.program->getUniformLocation("tex"), 7);

	glm::mat4x4 transf(1.0f);
	transf = glm::translate(transf, glm::vec3(x, y, 0.0f));
	transf = glm::scale(transf, glm::vec3((tex.getWidth() / 1280.0f) * width, (tex.getHeight() / 720.0f) * height, 1.0f));
	
	GLuint modelLoc = overlay.program->getUniformLocation("modelMatrix");
	glUniformMatrix4fv(modelLoc, 1, false, glm::value_ptr(transf));
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);
	assets::meshes["quad"].draw();
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
}

Demo::Demo(Window& window, Song& song) :
	DemoBase(window, song)
{
	init();

	#ifdef _DEBUG
		runTests();
	#endif
}

Demo::~Demo()
{
	delete Shader::processor;
	delete framebuffer;
}

void test_effu(ParameterMap& params, EffectInput in, EffectOutput* out)
{
	std::string scenename = "testscene";
	Scene* scene = in.scenes[scenename];
	Pipeline& p = scene->pipeline;

	Mesh& quad = assets::meshes["quad"];
	Mesh& house = assets::meshes["titans4b"];
	Shader& geometry = assets::shaders["geometry"];
	Shader& shading = assets::shaders["shading"];

	geometry.program->use();
	geometry.applyParameters(params);

	pick_camera_index(params, scene);
	apply_camera_move(params, scene->camera);
	apply_shadow_camera(in, scene, out);
	Camera cam = scene->camera;

	//float ang = params["p_time"] * 2.0f;
	//scene->getInstance("_ukkeli")->setOrientation(glm::rotation(vec3(cos(ang), 1.0f, -sin(ang)), vec3(0.0f, 1.0f, 0.0f)));

	scene->updateGraph();
	scene->render(p, params);
	out->lights = &scene->lights;
	out->camera = &scene->camera;
}

void Demo::render(float time, ParameterMap params)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	std::vector<EffectFunc_t> effects;
	effects.push_back(test_effu);

	int effect_index = static_cast<int>(params["effect"]);	

	if (effect_index < 0 || (unsigned int)effect_index >= effects.size()) {
		//LOG_DEBUG("Clamping invalid effect index %d", effect_index);
		effect_index = effect_index < 0 ? 0 : effects.size()-1;
	}

	EffectFunc_t active_effect = effects[effect_index];

	Light templight;

	EffectInput input(this->framebuffer, nullptr, scenes);
	EffectOutput output;
	EffectOutput shadowPassOutput;

	input.light_index = -1;
	input.framebuffer->bind();
	input.framebuffer->clear();
	active_effect(params, input, &output);

	Camera scene_camera = *output.camera;	// output.camera can be overwritten in the shadow map pass

	//drawOverlay(*colorLUT2D, 0.0f, 0.0f, 1.0f, 1.0f);

	// render the scene for each shadow casting light
	if (output.lights) {
		for (int i = 0 ; i < (int)output.lights->size() ; ++i) {
			Light& light = output.lights->at(i);
			if (!light.enabled)
				continue;

			if (!light.castsShadows)
				continue;

			input.light_index = i;
			input.framebuffer = shadowbuffer;
			shadowbuffer->bind();
			shadowbuffer->clearDepth();
			shadowbuffer->setViewport();

			active_effect(params, input, &shadowPassOutput);
			//shadowPassOutput.shadowView = shadowPassOutput.camera->getView();
			//break; // too bad we've got only one shadow map!
		}
	}

	framebuffer->bind();
	framebuffer->setViewport();

	applyShadingPass(&scene_camera, output.lights, params, shadowbuffer, shadowPassOutput.shadowTransform);

	if (output.transparentEffect) {
		// Render transparent effect draw lambda function
		EffectInput t_input(this->colorbuffer, nullptr, scenes);
		output.transparentEffect(params, 
			t_input, 
			//ShadowMapData(shadowPassOutput.shadowTransform, framebuffer->getTextureID(4)) // 4 happens to be depth buffer index
			ShadowMapData(shadowPassOutput.shadowTransform, shadowbuffer->getTextureID(0)),
			framebuffer->getTextureID(4) // 4 happens to be depth buffer index
		); 
	}

	applyPostProcessPass(params, colorbuffer, framebuffer, bloombuffer);

#ifdef _DEBUG
	if (window.keyDown(SDL_SCANCODE_F2)) {
		drawOverlayTexture(shadowbuffer->getTextureID(0), -0.25f, -0.25f, 0.5f, 0.5f, "depthrender");
	}
	if (window.keyDown(SDL_SCANCODE_F3)) {
		drawOverlayTexture(framebuffer->getTextureID(1), -0.5f, -0.5f, 1.f, 1.f);
	}
#endif

	/*
	if (song.getTime() > song.getLength() - 0.1f) {
		window.exitLoop();
	}
	*/
}

void Demo::applyShadingPass(Camera* sceneCamera, LightList_t* sceneLights, ParameterMap& params, FBO* shadowmap, glm::mat4x4& shadowview)
{
	Shader& shading = assets::shaders["shading"];
	Mesh& quad = assets::meshes["quad"];
	Camera* cam = &dummyCamera;
	LightList_t* lights = &dummyLights;

	if (sceneCamera)
		cam = sceneCamera;

	if (sceneLights)
		lights = sceneLights;

	Pipeline p;	
	shading.program->use();
	p.setShader(&shading);
	shading.applyParameters(params);

	p.updateUniforms(*cam, *lights);

	// TODO wrap these up somehow
	glActiveTexture(GL_TEXTURE0 + 0);
		glBindTexture(GL_TEXTURE_2D, framebuffer->getTextureID(0));
		glUniform1i(shading.getUniformLocation("albedobuffer"), 0);

	glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, framebuffer->getTextureID(1));
		glUniform1i(shading.getUniformLocation("normalbuffer"), 1);

	glActiveTexture(GL_TEXTURE0 + 2);
		glBindTexture(GL_TEXTURE_2D, framebuffer->getTextureID(2));
		glUniform1i(shading.getUniformLocation("positionbuffer"), 2);

	glActiveTexture(GL_TEXTURE0 + 3);
		glBindTexture(GL_TEXTURE_2D, framebuffer->getTextureID(3));
		glUniform1i(shading.getUniformLocation("materialbuffer"), 3);

	glActiveTexture(GL_TEXTURE0 + 4);
		glBindTexture(GL_TEXTURE_2D, framebuffer->getTextureID(4));
		glUniform1i(shading.getUniformLocation("depthbuffer"), 4);

	glActiveTexture(GL_TEXTURE0 + 5);
		glBindTexture(GL_TEXTURE_2D, shadowmap->getTextureID(0));
		glUniform1i(shading.program->getUniformLocation("shadowbuffer"), 5);

	// texture 6 here

	glActiveTexture(GL_TEXTURE0 + 7);
		glBindTexture(GL_TEXTURE_3D, colorTables["colorlut_saturate"]->getID());
		glUniform1i(shading.getUniformLocation("colorLUT"), 7);

	glUniformMatrix4fv(shading.program->getUniformLocation("shadowTransform"), 1, GL_FALSE, glm::value_ptr(shadowview));

#ifdef _DEBUG
	if (window.keyDown(SDL_SCANCODE_SPACE)) {
		glUniform1f(shading.getUniformLocation("debugKey"), 1.0f);
	} else {
		glUniform1f(shading.getUniformLocation("debugKey"), 0.0f);
	}
#endif

	glDisable(GL_DEPTH_TEST);
	colorbuffer->bind();

	quad.draw();
}

void Demo::applyPostProcessPass(ParameterMap& params, FBO* colorfbo, FBO* framefbo, FBO* bloomfbo)
{
	Mesh& quad = assets::meshes["quad"];
	framefbo->bind();	// use the original framebuffer as a temp buffer
	framefbo->clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	const int bloom_iterations = 4;
	FBO* first_bloom_source = colorfbo;
	Shader& bloom = assets::shaders["bloom"];
	bloom.program->use();
	bloom.applyParameters(params);

	glDisable(GL_DEPTH_TEST);

	for (int i=0;i<bloom_iterations;i++) {
		framefbo->bind();	

		bloom.floatUniform("use_vertical_bloom", 0.0f);
		glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, first_bloom_source->getTextureID(0));
			glUniform1i(bloom.getUniformLocation("colorbuffer"), 0);
			
		quad.draw();

		bloomfbo->bind();
		bloom.floatUniform("use_vertical_bloom", 1.0f);
		glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, framefbo->getTextureID(0));
			glUniform1i(bloom.getUniformLocation("colorbuffer"), 0);

		quad.draw();

		first_bloom_source = bloomfbo;
	}

	FBO::bindDefault();
	Shader& post = assets::shaders["post"];
	post.program->use();
	post.applyParameters(params);
	glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorfbo->getTextureID(0));
		glUniform1i(post.getUniformLocation("colorbuffer"), 0);
	glActiveTexture(GL_TEXTURE0 + 1);
		glBindTexture(GL_TEXTURE_2D, bloomfbo->getTextureID(0));
		glUniform1i(post.getUniformLocation("bloombuffer"), 1);

	quad.draw();
	glEnable(GL_DEPTH_TEST);
}

void Demo::init()
{
	glGetError();	// reset error state
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	assertGlError("Could not set OpenGL depth testing options");
 
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	assertGlError("Could not set OpenGL culling options");

	int width = Window::currentSettings.width;
	int height = Window::currentSettings.height;

	FBO::Buffer buffers[]  = {
		{GL_R11F_G11F_B10F, GL_RGBA, GL_UNSIGNED_INT, width, height}, // albedo
		{GL_RGBA8, GL_RGBA, GL_UNSIGNED_BYTE, width, height}, // normals, 8-bit unsigned normalized integer format
		{GL_RGB16F, GL_RGB, GL_UNSIGNED_INT, width, height}, // position
		{GL_RGBA8, GL_RGBA, GL_UNSIGNED_INT, width, height}, // emit, specular size, specular amount 
		{GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, width, height}
	};

	framebuffer = new FBO(width, height, buffers, 5);

	const int shadow_quality = 1024;

	FBO::Buffer shadowbuffer_spec[] = {
		{GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, shadow_quality, shadow_quality}
	};

	shadowbuffer = new FBO(shadow_quality, shadow_quality, shadowbuffer_spec, 1);

	FBO::Buffer colorbuffer_spec[] = {
		{GL_R11F_G11F_B10F, GL_RGB, GL_UNSIGNED_INT, width, height}
	};

	colorbuffer = new FBO(width, height, colorbuffer_spec, 1);

	FBO::Buffer bloombuffer_spec[] = {
		{GL_R11F_G11F_B10F, GL_RGB, GL_UNSIGNED_INT, width, height}
	};

	bloombuffer = new FBO(width, height, bloombuffer_spec, 1);

	assets::meshes.handOver("tetra", std::unique_ptr<Mesh>(new TetraMesh()));
	assets::meshes.handOver("quad", std::unique_ptr<Mesh>(new QuadMesh()));

	test_material = new Material("testmaterial",
		assets::shaders.get("panic"), 
		&assets::textures["checker"], 
		&assets::textures["black"], 
		250.0f, 
		1.0f,
		glm::vec3(1.0f));

	assets::materials.handOver(test_material);

	Instance model = Instance(assets::meshes.get("titans4b"), test_material);
	Instance model2 = Instance(assets::meshes.get("titans4b"), test_material);
	instances.push_back(model);
	instances.push_back(model2);

	colorLUT2D = new Texture("colorlut.png", true, false, GL_RGBA);

	colorTables["colorlut"]				= std::unique_ptr<Texture3D>(new Texture3D("colorlut.png", 32, true, GL_RGBA));
	colorTables["colorlut_saturate"]	= std::unique_ptr<Texture3D>(new Texture3D("colorlut_saturate.png", 32, true, GL_RGBA));

	initDummyHelpers();
}

void Demo::update(Window& window, float t)
{
#ifdef _DEBUG
	if (window.keyHit(SDL_SCANCODE_F12)) {
		time_t rawtime;
		struct tm* timeinfo;
		char buffer[80];

		time(&rawtime);
		timeinfo = localtime(&rawtime);

		strftime(buffer,80,"_%d_%m_%Y_-_%I_%M_%S",timeinfo);

		std::string path = "temp/demoshot" + std::string(buffer) + std::string(".png");
		LOG_INFO("Saving screenshot to %s", path.c_str());
		FBO::bindDefault();
		dumpScreenshot(window.getWidth(), window.getHeight(), path);

	}
#endif
}

void Demo::reloadAssets()
{
	assets::shaders.reloadShaders();
}

void Demo::runTests()
{
	glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::quat rot = glm::angleAxis(glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // 90 asteen pyöritys VASTAPÄIVÄÄN

	glm::vec3 uus = rot*right;
	printf("right: %s,\t uus: %s\n",glm::to_string(right).c_str(), glm::to_string(uus).c_str());
}


void Demo::initDummyHelpers()
{
	using glm::vec3;
	// A non-visible dummy light
	Light dummylight(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0, 0.0f), vec3(1.0f, 1.0f, 1.0f), 1.0f, 75.0f, 1.0f, false, false);
	dummyLights.push_back(dummylight);

	dummyCamera.lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, -1.0f));

	Camera* tempcam = new Camera();
	tempcam->lookAt(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, -1.0f));
	tempcam->setPerspective(90.0f, 1280.0f, 720.0f, 0.1f, 100.0f);

	Scene* testscene = new Scene();
	testscene->giveLight(new Light(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0, 0.0f), vec3(1.0f, 1.0f, 1.0f), 1.0f, 75.0f, 1.0f, false, false));
	testscene->giveCamera(tempcam);

	Instance* inst = new Instance(assets::meshes.get("_karmes"), test_material, vec3(0.0f, 0.0f, -10.0f), vec3(1.0f), glm::quat());
	testscene->addPointerToScene(inst);

	scenes["testscene"] = testscene;
}
