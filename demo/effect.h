#pragma once

#include <functional>
#include <glm/mat4x4.hpp>
#include <parametermap.h>
#include <light.h>
#include <shader.h>
#include <FBO.h>
#include <Scene.h>
#include <scene.h>

struct EffectInput {
	EffectInput(
		FBO* framebuffer, 
		FBO* overlay, 
		SceneMap_t& scenes
	) :
		framebuffer(framebuffer), 
		overlay(overlay), 
		scenes(scenes)
	{};

	FBO* framebuffer; 
	FBO* overlay; 		// a possible layered effect render, might be empty
	int light_index;	// updated for each light rendering pass. -1 if none
	
	SceneMap_t& scenes;
};

struct ShadowMapData {
	ShadowMapData(
		glm::mat4x4& transform,
		GLuint textureID) :
	transform(transform),
		textureID(textureID) {};

	glm::mat4x4& transform;
	GLuint textureID;
};

// This is a forward-rendered function that will be called after the deferred shading pass.
typedef std::function<void(ParameterMap& params, EffectInput in, ShadowMapData shadowmap, GLuint scenedepth)> TransparentEffectFunc_t;

struct EffectOutput {
	EffectOutput() : camera(nullptr), lights(nullptr), transparentEffect(nullptr), shadowTransform(1.0f) {};
	//Scene* scene;								// nullptr if not used
	Camera* camera;								// nullptr if not set
	LightList_t* lights;						// nullptr when no lights are used in scene	
	TransparentEffectFunc_t transparentEffect;	// nullptr if not set
	glm::mat4x4 shadowTransform;				// shadowmap transformation for the only shadow casting light
};

typedef void (*EffectFunc_t)(ParameterMap& params, EffectInput in, EffectOutput* output);

