#pragma once

#include <map>
#include <texturebase.h>
#include <texture.h>
#include <demo.h>
#include <parametermap.h>
#include <pipeline.h>
#include <Scene.h>
#include <Instance.h>
#include <texture3d.h>

class Demo: public DemoBase {
	public:
	Demo(Window& window, Song& song);
	~Demo();

	void runTests();
	void update(Window& window, float time);
	void render(float time, ParameterMap params);
	void reloadAssets();

	void drawOverlay(TextureBase& tex, float x, float y, float width, float height, std::string shadername = "overlay");
	void drawOverlayTexture(GLuint textureID, float x, float y, float width, float height, std::string shadername = "overlay");
	protected:
	FBO* framebuffer;
	FBO* shadowbuffer;
	FBO* colorbuffer;
	FBO* bloombuffer;

	LightList_t dummyLights;
	Camera dummyCamera;
	std::vector <Instance> instances;
	Material* test_material;
	//Texture3D* colorLUT;
	Texture* colorLUT2D;

	std::map<std::string, std::unique_ptr<Texture3D>> colorTables;
	void init();
	void initDummyHelpers();
	void applyPostProcessPass(ParameterMap& params, FBO* colorbuffer, FBO* framebuffer, FBO* bloombuffer);
	void applyShadingPass(Camera* cam, LightList_t* lights, ParameterMap& params, FBO* shadowmap, glm::mat4x4& shadowview);
};