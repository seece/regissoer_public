#include "launcher.h"

namespace {
bool runprogram = true, full = false, vsync = false;

int global_screenw = 0, global_screenh = 0;
}

INT_PTR CALLBACK launcherProc(HWND dlg, UINT msg, WPARAM w, LPARAM l)
{
	if(msg==WM_INITDIALOG)
	{
		ComboBox_AddString(GetDlgItem(dlg, IDC_COMBO1), L"1280x720  (we render large pixels)");
		ComboBox_AddString(GetDlgItem(dlg, IDC_COMBO1), L"1920x1080 (\"full\" HD)");
		ComboBox_AddString(GetDlgItem(dlg, IDC_COMBO1), L"3840x2160 (the PEISIK experience)");
		ComboBox_AddString(GetDlgItem(dlg, IDC_COMBO1), L"WxH (custom)");
		ComboBox_SetMinVisible(GetDlgItem(dlg, IDC_COMBO1), 4);
		
		ComboBox_SetCurSel(GetDlgItem(dlg, IDC_COMBO1), 1);
		CheckDlgButton(dlg, IDC_CHECK1, BST_CHECKED);
	}
	if(msg==WM_COMMAND)
	{

		if(HIWORD(w)==BN_CLICKED && LOWORD(w)==IDCANCEL)
		{
			runprogram=false;
			EndDialog(dlg,0);
			return 1;
		}

		if(HIWORD(w)==BN_CLICKED && LOWORD(w)==IDOK) // launch pressed
		{
			int len = 1+ComboBox_GetTextLength(GetDlgItem(dlg, IDC_COMBO1));
			wchar_t * buff = new wchar_t[len];
			ComboBox_GetText(GetDlgItem(dlg, IDC_COMBO1), buff, len);

			swscanf_s(buff, L"%dx%d", &global_screenw, &global_screenh);

			delete [] buff;

			if(BST_CHECKED == IsDlgButtonChecked(dlg,IDC_CHECK1))
				full = true;

			if(BST_CHECKED == IsDlgButtonChecked(dlg,IDC_CHECK2))
				vsync = true;

			EndDialog(dlg, 0);
			return 1;
		}
	}
	return 0;
}

Window::LaunchSettings launch()
{
	InitCommonControls();
	DialogBox(GetModuleHandle(0), MAKEINTRESOURCE(IDD_DIALOG1), 0, (DLGPROC)launcherProc);

	Window::Settings set;
	set.width = global_screenw;
	set.height = global_screenh;
	set.fullscreen = full;
	set.vsync = vsync;

	Window::LaunchSettings lset = {set, runprogram};

	return lset;
}