#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3

#ifdef VERTEX

layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
// normal
layout(location=3) in vec2 in_UV;
out vec4 ex_Color;
out vec2 ex_UV;

void main()
{
	ex_Color = in_Color;
	ex_UV = in_UV;
	gl_Position = in_Position;
}

#endif

#ifdef FRAGMENT

vec3 filmicTonemap(vec3 c) {
	vec3 x = max(vec3(0.0), c - vec3(0.004));
	vec3 correct = (x*(6.2*x+vec3(.5)))/(x*(6.2*x+vec3(1.7))+vec3(0.06));
	return pow(correct, vec3(2.2));	// the formula includes gamma correction TODO how to get rid of it
	// maybe another tonemapping function could be used?
}

in vec4 ex_Color;
in vec2 ex_UV;

layout(location=0) out vec4 out_Color;

uniform sampler2D colorbuffer;
uniform sampler2D bloombuffer;

uniform float p_bloom_amount;
uniform float p_bloom_treshold;
uniform float p_brite_mult;
	
#define USE_FILMIC_TONEMAP
#define USE_FXAA
#define USE_BLOOM

#include "inc/fxaa.glsl"
#line 37

void main()
{
	vec2 resolution = textureSize(colorbuffer, 0);
	vec2 uv = ex_UV;
	
	#ifdef USE_FXAA
	vec4 col = applyFXAA(uv * resolution.xy, colorbuffer, resolution.xy);
	#else
	vec4 col = texture(colorbuffer, ex_UV);
	#endif
	
	#ifdef USE_BLOOM
	col.rgb += texture(bloombuffer, ex_UV).rgb;
	#endif
	
	#ifdef USE_FILMIC_TONEMAP
	col.rgb = filmicTonemap(col.rgb);
	#endif

	out_Color = col; 
} 

#endif