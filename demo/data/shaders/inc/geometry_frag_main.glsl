void main()
{
	float dist = length(ex_Position.xyz - vec3(0.0, 4.0, -10.0));
	vec4 sampld = texture(tex, ex_UV.xy);
	vec3 col = sampld.rgb;

	if (sampld.a <= ALPHA_TEST_THRESHOLD) {
		discard;
	}
	
	out_Color = vec4(col, 1.0);
	
	out_Position = ex_Position.xyz;
	out_Normal = compressv3(ex_Normal.xyz);
	out_Material = vec4(texture(emitMap, ex_UV.xy).r, scaleSpecular(specularExponent), specularMultiplier, 0.0);

}
