#ifdef FRAGMENT
in vec4 ex_Color;
in vec3 ex_Normal;
in vec2 ex_UV;
in vec4 ex_Position;

layout(location = 0) out vec4 out_Color;
layout(location = 1) out vec3 out_Normal;
layout(location = 2) out vec3 out_Position;
layout(location = 3) out vec4 out_Material;

uniform sampler2D tex;
uniform sampler2D emitMap;
uniform float specularExponent;
uniform float specularMultiplier;
#endif