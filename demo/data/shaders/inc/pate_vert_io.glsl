
layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
layout(location=2) in vec3 in_Normal;
layout(location=3) in vec2 in_UV;
out vec4 ex_Color;
out vec3 ex_Normal;
out vec2 ex_UV;
out vec4 ex_Position;	// view space position
