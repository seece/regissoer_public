uniform sampler2D albedobuffer;
uniform sampler2D normalbuffer;
uniform sampler2D positionbuffer;
uniform sampler2D materialbuffer;
uniform sampler2D depthbuffer;
uniform sampler3D colorLUT;