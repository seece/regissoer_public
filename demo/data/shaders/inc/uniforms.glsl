
struct Light {
	vec3 pos;	
	vec3 intensity;
	vec3 dir;
	bool enabled;
	float falloff;
};

struct Screen
{
	vec2 size;
};

struct Camera
{
	vec3 pos;
	float aspect;
	float near;
	float far;
};

uniform Screen screen;
uniform Camera scenecam;

uniform mat4 modelMatrix;
//uniform mat4 viewProjectMatrix; 

uniform float p_time;
uniform float p_beat;

layout(std140) uniform TransformMatrices
{
	mat4 viewMatrix;
	mat4 projectionMatrix;
	mat4 inverseViewMatrix;
	mat4 inverseProjectionMatrix;
	mat4 viewProjectMatrix;
	mat3 normalMatrixNotWorkingWhy;
};

uniform mat3 normalMatrix;

layout(std140) uniform LightArray
{
	Light spot[8];	
} lights;

#define MAX_LIGHTS (8)
// the index of the spotlight that casts shadows
#define SHADOW_SPOT_INDEX (0)
