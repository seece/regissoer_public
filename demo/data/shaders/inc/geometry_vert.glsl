// this file is included by geometry.glsl
void main()
{
	//normal = (viewMatrix*modelMatrix) * normal;
	
	ex_Color = vec4(in_Normal, 1.0);
	//ex_Normal = normalize(normalMatrix * in_Normal.xyz);
	//mat3 normalmat = transpose((inverse(mat3(modelMatrix))));
	mat3 normalmat = normalMatrix;
	ex_Normal = normalize(normalmat * in_Normal.xyz);
	ex_UV = in_UV.xy;

	PROCESSING_FUNCTION();

	//ex_Position = (viewMatrix*modelMatrix)*in_Position;
	//gl_Position = (projectionMatrix*viewMatrix*modelMatrix)*in_Position;
}

