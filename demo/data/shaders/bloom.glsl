#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3

#ifdef VERTEX
uniform sampler2D colorbuffer;

layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
// normal
layout(location=3) in vec2 in_UV;
out vec4 ex_Color;
out vec2 ex_UV;
out vec2 resolution;
out vec2 resolutionInv;

void main()
{
	resolution = textureSize(colorbuffer, 0).xy;
	resolutionInv = vec2(1.0) / textureSize(colorbuffer, 0).xy;

	ex_Color = in_Color;
	ex_UV = in_UV;
	gl_Position = in_Position;
}

#endif

#ifdef FRAGMENT
in vec4 ex_Color;
in vec2 ex_UV;
in vec2 resolution;
in vec2 resolutionInv;


layout(location=0) out vec4 out_Color;

uniform sampler2D colorbuffer;
uniform float use_vertical_bloom;

uniform float p_bloom_add;

//float bloom_kernul[9] = float[9](0.05, 0.09, 0.12, 0.15, 0.16, 0.15, 0.12, 0.09, 0.05);
float bloom_kernul[9] = float[9](0.03, 0.05, 0.10, 0.15, 0.21, 0.15, 0.10, 0.05, 0.03);

void main()
{
	vec2 uv = ex_UV;
	vec4 col = vec4(0.0, 0.0, 0.0, 1.0);
	
	float limit = 0.3;
	
	for (int i=0;i<bloom_kernul.length();i++) {
		vec2 ofs = vec2(float(i) - 4.0, 0.0);
		
		if (use_vertical_bloom == 0.0) {
			ofs *= resolutionInv;
			vec3 c = texture(colorbuffer, ex_UV + ofs).rgb;
			
			//col.rgb += c;
			
			col.rgb +=
				max(c - vec3(limit), vec3(0.0)) * bloom_kernul[i]; 
				
		} else {
			ofs.xy = ofs.yx;
			ofs *= resolutionInv;
			col.rgb += texture(colorbuffer, ex_UV + ofs).rgb* bloom_kernul[i];// * 0.01;
			//col.rgb *= 1.0 + p_bloom_add;
		}
	}
	
	if (use_vertical_bloom == 1.0) {
		//col.rgb += texture(colorbuffer, ex_UV).rgb;
		//col.rgb *= 1.0;
	}

	out_Color = col*p_bloom_add*0.7; 
} 
#endif