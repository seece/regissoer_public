#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3 1

#ifdef VERTEX
#line 1 4
#include "inc/geometry_vert_io.glsl"

#line 10 1
void process() 
{
	ex_Position = (viewMatrix*modelMatrix)*in_Position;
	gl_Position = (viewProjectMatrix*modelMatrix)*in_Position;
}

#define PROCESSING_FUNCTION process
#line 1 5
#include "inc/geometry_vert.glsl"

#endif


#ifdef FRAGMENT
#line 1 6
#include "inc/geometry_frag_io.glsl"
#line 1 7
#include "inc/geometry_frag_main.glsl"
#endif
