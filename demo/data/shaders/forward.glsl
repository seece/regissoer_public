#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3 1

uniform mat4 shadowTransform;

#ifdef VERTEX
#line 1 4
#include "inc/geometry_vert_io.glsl"
#line 10 1

out vec4 ex_WorldPosition;	

void process() 
{
	ex_WorldPosition = modelMatrix*in_Position;
	ex_Position = (viewMatrix*modelMatrix)*in_Position;
	gl_Position = (projectionMatrix*viewMatrix*modelMatrix)*in_Position;
}

#define PROCESSING_FUNCTION process
#line 1 5
#include "inc/geometry_vert.glsl"

#endif


#ifdef FRAGMENT
#line 1 26
in vec4 ex_Color;
in vec3 ex_Normal;
in vec2 ex_UV;
in vec4 ex_Position;
in vec4 ex_WorldPosition;	

layout(location = 0) out vec4 out_Color;
//layout(location = 1) out vec3 out_Normal;
//layout(location = 2) out vec3 out_Position;
//layout(location = 3) out vec4 out_Material;

uniform sampler2D tex;
uniform sampler2D emitMap;

uniform sampler2D shadowbuffer;

#include "inc/shadows.glsl"

void main()
{
	//float dist = length(ex_Position.xyz - vec3(0.0, 4.0, -10.0));
	vec4 sampld = texture(tex, ex_UV.xy);
	vec4 col = sampld.rgba;
	vec2 screen_uv = gl_FragCoord.xy / textureSize(shadowbuffer, 0);
	
	float sceneDepth = texture(shadowbuffer, screen_uv).r;
	
	if (gl_FragCoord.z > sceneDepth) {
		discard;
	}
	
	vec4 lightview = shadowTransform * ex_WorldPosition; // light position in lamp view space
	vec3 lightclip = lightViewToClip(lightview);
	vec3 lamplight = accumulate_light(scenecam, ex_WorldPosition, lightclip, ex_Normal, 100.0, 1.0, 0.0);
	
	col.a *= 0.5;
	col.rgb = vec3(0.1);
	col.rgb += lamplight*1.0;
	
	out_Color = col;
}

#endif