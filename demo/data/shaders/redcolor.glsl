#include "inc/uniforms.glsl"
#include "inc/utils.glsl"

#ifdef VERTEX

layout(location=0) in vec4 in_Position;
layout(location=1) in vec4 in_Color;
// normal
layout(location=3) in vec2 in_UV;
out vec4 ex_Color;
out vec2 ex_UV;

void main()
{
	ex_Color = in_Color;
	ex_UV = in_UV;
	gl_Position = modelMatrix*in_Position;
}

#endif

#ifdef FRAGMENT

in vec4 ex_Color;
in vec2 ex_UV;

layout(location=0) out vec4 out_Color;

uniform float p_overlay_alpha;
uniform sampler2D tex;

void main()
{
	vec4 col = texture(tex, ex_UV);
	col.a *= p_overlay_alpha;

	out_Color = vec4(1.0, 0.0, 0.0, 1.0);
} 

#endif