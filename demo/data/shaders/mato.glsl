#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3 1

#ifdef VERTEX
#line 1 4
#include "inc/geometry_vert_io.glsl"

uniform float p_rotation;
#line 10 1
void process() 
{
	float alpha = p_rotation;
	mat2 rot = mat2(cos(alpha), sin(alpha), -sin(alpha), cos(alpha));
	vec4 pos = in_Position;
	float len = length(pos.xyz);
	//pos.y *= 1.0 + pos.z*1.0;
	//pos.xyz *= 1.0 + sin(len*2.0 + p_beat*1.0)*0.1;
	//pos.xyz *= 1.0/(length(pos.xyz)*0.2);
	pos.zy = rot*pos.zy;

	ex_Position = (viewMatrix*modelMatrix)*pos;
	gl_Position = (viewProjectMatrix*modelMatrix)*pos;
}

#define PROCESSING_FUNCTION process
#line 1 5
#include "inc/geometry_vert.glsl"

#endif


#ifdef FRAGMENT
#line 1 6
#include "inc/geometry_frag_io.glsl"
#line 1 7
#include "inc/geometry_frag_main.glsl"
#endif
