#include "inc/uniforms.glsl"
#include "inc/utils.glsl"
#line 3

#ifdef VERTEX
#line 9
layout(location=0) in vec4 in_Position;
smooth out vec3 direction;

void main()
{
	direction = (inverse(viewMatrix) * vec4(in_Position.xy, -1.0, .0)).xyz;
	gl_Position = vec4(in_Position.xy, .0, 1.0);
}

#endif

#ifdef FRAGMENT

layout(location = 0) out vec4 out_Color;
layout(location = 1) out vec3 out_Normal;
layout(location = 2) out vec3 out_Position;
layout(location = 3) out vec4 out_Material;

uniform vec3 light_direction, light_color, dark_color;
smooth in vec3 direction;

void main()
{
	out_Color = vec4(mix(dark_color, light_color, pow(clamp(dot(normalize(light_direction), normalize(direction))*.5+.5, .0, 1.0), 12.0)), 1.0);
	out_Material = vec4(1.0, .0, .0, .0);

	out_Normal = vec3(.0); // not sure if explicit writes needed but won't probably hurt either
	out_Position = vec3(.0);
} 

#endif
