
proc load_rocket_params {playername} {
	upvar $playername player
	
	player clearParameters

	# ois makee jos n�� p-alkuset luettais suoraan shadereista
	
	set params {
		effect
		camx
		camy
		camz
		cam_in
		p_bloom_add
		cam_num
		p_rotation
		p_brite_mult
	    light_x	
	    light_y	
	    light_z	
        overlay_id
        p_overlay_alpha
        p_dist_mult
        p_particle_alpha
    }

	foreach param $params {
		player addParameter $param
	}
}

