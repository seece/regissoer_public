namespace eval material_import {
namespace export load_material 
namespace export load_all_materials


proc load_material {path shaders textures} {
	set oldpath [pwd]	
	cd [getAssetSourceDir]
	set basename [util::basename $path]
	
	set infile [open $path "r"]
	set data [read $infile]
	#set data [split $file_data "\n"]
	close $infile
	cd $oldpath
	
	set mat [eval "concat $data"]
	set desc [lindex $mat 0]
	set shader [lindex $mat 1]
	set texture [lindex $mat 2]
	set emit [lindex $mat 3]
	set exponent [lindex $mat 4]
	set multiplier [lindex $mat 5]
	set specular_color [lindex $mat 6]
	
	set material [new_Material $basename [shaders get $shader] [textures get $texture] [textures get $emit] $exponent $multiplier [create_vec3 {*}$specular_color]]
	
	logdebug "Material $basename: $shader, exp: $exponent x $multiplier"
	
	return $material
}

# Loads and adds all materials from data/materials/ to the material map.
proc load_all_materials {materials shaders textures} {
	set oldpath [pwd]	
	cd [getAssetSourceDir]
	set filelist [util::getfiles "materials"]
	cd $oldpath
	
	foreach f $filelist {
		set basename [util::basename $f]
		puts "Loading material $basename"
		set mat [load_material $f $shaders $textures]
		materials addMaterial $basename $mat
	}
}

}