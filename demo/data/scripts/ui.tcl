source scripts/keys.tcl

proc update_ui {scenenamelist windowname demoptrname playername songname shadersname texturesname materialsname meshesname scenesname} {
	global const

	upvar $windowname win
	upvar $demoptrname demo
	upvar $playername player
	upvar $songname song
	upvar $shadersname shaders
	upvar $texturesname textures
	upvar $materialsname materials
	upvar $meshesname meshes
	upvar $scenesname scenes

	if {[win keyHit $const(SDL_SCANCODE_F1)]} {
		set commandlist  [lsort [info commands]]
		puts $commandlist
		set outfile [open "commandlist.txt" "w"]
		puts -nonewline $outfile $commandlist
		close $outfile
	}

	if {[win keyHit $const(SDL_SCANCODE_F5)]} {
		shaders reloadShaders [win keyDown $const(SDL_SCANCODE_LSHIFT)]
	}

	if {[win keyHit $const(SDL_SCANCODE_F6)]} {
		puts -nonewline "Reloading parameters..."

		global param_mods

		set mainPath [pwd]
		cd [getAssetSourceDir]
		source scripts/params.tcl
		source scripts/param_mods.tcl
		cd $mainPath

		load_rocket_params player

		puts "OK"
	}

	if {[win keyHit $const(SDL_SCANCODE_F7)]} {
		puts "Reloading scenes..."
		set scenemap [DemoBase_getSceneMap $demo]
		foreach skenename $scenenamelist {
			addSceneMapEntry $scenemap $skenename [scene_import::load_scene "scenes/$skenename.sken" meshes textures materials]
		}
	}
		
	if {[win keyHit $const(SDL_SCANCODE_F8)]} {
		puts "Reloading materials..."
		material_import::load_all_materials materials shaders textures
	}
}
