puts "REGISS�R2 Tcl shell"

#package require TclOO

set SOURCE_DIR 0
set SOURCE_ZIP 1
setAssetSource $SOURCE_DIR "data/" 
initializeSystem

set mainPath [pwd]
cd [getAssetSourceDir]
source scripts/util.tcl
source scripts/params.tcl
source scripts/param_utils.tcl
source scripts/param_mods.tcl
source scripts/ui.tcl
source scripts/material_import.tcl
source scripts/scene_import.tcl
cd $mainPath

if {[debugModeEnabled]} {
	Settings windowsettings [expr {1280}] [expr {720}] false false "Peisik Demo"
	Window win windowsettings
} else {
	Window win [inquireLaunchSettings "bag of sins"]
}

Song song mother_to_mother.it 115 $Song_SONG_TRACKER

ShaderMap shaders
TextureStore textures
MaterialStore materials
MeshStore meshes

# Wrap the asset maps to a class that's used as a Demo object parameter
AssetStore assets materials textures shaders meshes

meshes loadAssets
textures loadTextureAssets

foreach f [util::map util::filename [util::getfiles "data/shaders"]] {
	shaders loadFile $f
}

material_import::load_all_materials materials shaders textures

set demoptr [createDemoObject win song assets]
set scenemap [DemoBase_getSceneMap $demoptr]
set scenenames {rooms pate matelija}

foreach skenename $scenenames {
	addSceneMapEntry $scenemap $skenename [scene_import::load_scene "scenes/$skenename.sken" meshes textures materials]
}

if {[debugModeEnabled]} {
	DebugPlayer player win
} else {
	ReleasePlayer player win
}

puts "Debugmode enabled: [debugModeEnabled]"

load_rocket_params player

# the value of the demoptr is the actual pointer string
player loadDemo $demoptr 
puts "Playing demo $demoptr"

set param_map [player getParameterMap]
set vektor [new_stringvector]

player play

while {[win processEvents]} {
	set row [song getRow]
	getParamKeys $param_map $vektor
	

	if {[debugModeEnabled]}	{
		update_ui $scenenames win demoptr player song shaders textures materials meshes scenes
	}

	player updateSyncTime $row
	player updateSyncParams $row [player getParameterMap]

	# Apply parameter modifiers (defined in param_mods.tcl)
	set param_names [read_params $vektor]
	update_param_modifiers $param_names $param_mods [player getParameterMap]

	win draw
}

puts "Demo ended"
cleanupSystem
exit
