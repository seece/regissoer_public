namespace eval util {

proc map {fun list} {
    set res {}
    foreach element $list {lappend res [$fun $element]}
    set res
 } ;# RS
 
proc filename {pathdir} {
	return [file tail $pathdir]
}

proc filter {list script} {
    set res {}
    foreach e $list {if {[uplevel 1 $script [list $e]]} {lappend res $e}}
    set res
}

proc pause {{message "Press Enter to continue "}} {
    puts -nonewline $message
    flush stdout
    gets stdin
}

proc getfiles {directory} {
	return [filter [glob -nocomplain -directory $directory *] {file isfile}]
}

proc basename {path} {
	return [file rootname [file tail $path]]
}
}