
# converts a stringvector to a list
proc read_params {vector} {
	set out {}

	while {[stringvector_size $vector] > 0} {
		lappend out [stringvector_pop $vector]
	}

	return $out
}

proc update_param_modifiers {names modulation parameter_map} {
	#upvar $parameter_map params
	set params $parameter_map

	foreach name $names {
		if {![dict exists $modulation $name]} {
			continue
		}

		if {![checkParamExists $params $name]} {
			# this value hasn't been set by rocket, so skip
			continue
		}

		set x [getParamValue $params $name]
		set formula [dict get $modulation $name]
		set substituted [subst -nocommands $formula]
		set newval [expr $substituted]

		setParamValue $params $name $newval
	}
}
