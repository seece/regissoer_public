#pragma once
// Local helper function for effects

#pragma warning(disable:4503) // decorated name length exceeded
#define GLM_FORCE_RADIANS

#include "effect.h"
#include <gl/gleks.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/vector_query.hpp>
#include <glm/trigonometric.hpp>
#include <glm/ext.hpp>

#include <string>
#include <algorithm>
#include <array>

#include <parametermap.h>
#include <Assets.h>

using glm::vec3;

namespace {

struct Smoke {
	glm::vec3 pos;
	glm::vec3 viewpos;
	int id;
};

void wrap_transparent_func(TransparentEffectFunc_t func, bool depthwrite, ParameterMap& params, 
						   EffectInput in, 
						   ShadowMapData shadowmap, 
						   GLuint depthtexture) 
{
	glEnable(GL_DEPTH_TEST);

	if (depthwrite)
		glDepthMask(GL_TRUE);
	else 
		glDepthMask(GL_FALSE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	func(params, in, shadowmap, depthtexture);

	glDisable(GL_BLEND);
	glDepthMask(GL_TRUE);
}

void apply_camera_move(ParameterMap& params, Camera& cam)
{
	glm::vec3 pos = cam.getPosition();
	glm::vec3 dir = cam.getDirection();
	pos.x += params["camx"];
	pos.y += params["camy"];
	pos.z += params["camz"];
	pos = pos + dir * params["cam_in"];
	cam.lookAt(pos, pos+dir);
}

// Picks the correct light index to use as camera, if any. Overwrites scene->camera.
void apply_shadow_camera(EffectInput& in, Scene* scene, EffectOutput* out) 
{
	if (in.light_index >= 0) {
		scene->camera.alignWithLight(scene->lights[in.light_index]);
		scene->camera.setPerspective(60.0f, 1024.0f, 1024.0f, 0.1f, 200.0f);
		out->shadowTransform = scene->camera.getViewProject();
	}
}

int pick_camera_index(ParameterMap& params, Scene* scene) {
	int ind = (int)params["cam_num"];

	if (ind < 0) {
		ind = 0;
	}
	
	if (ind > (int)scene->cameras.size()-1) {
		ind = (int)scene->cameras.size()-1;
	}

	scene->useCamera((float)ind);

	return ind;
}

void move_light(ParameterMap& params, Scene* scene, int index) {
	Light& l = scene->lights[index];
	l.setPosition(l.getPosition() + glm::vec3(params["light_x"], params["light_y"], params["light_z"]));
}

void draw_smoke(ParameterMap& params, 
				EffectInput& in, 
				std::string scenename, 
				ShadowMapData shadowmap, 
				Camera& cam, 
				float spread, 
				glm::vec3 emit_pos,
				GLuint depthtexture)
{
		glEnable(GL_DEPTH_TEST);

		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		Scene* scene = in.scenes[scenename];
		Mesh& model = assets::meshes["titans4b"];
		Mesh& quad = assets::meshes["quad"];
		Shader& shader = assets::shaders["smoke"];
		Texture& noise = assets::textures["noise1"];
		shader.program->use();
		Pipeline p;
		MatrixStack stack;
		stack.loadIdentity();

		p.setShader(&shader);
		shader.applyParameters(params);
		p.modelmatrix = *stack.top();
		p.updateUniforms(cam, scene->lights);

		glActiveTexture(GL_TEXTURE0 + 6);
		glBindTexture(GL_TEXTURE_2D, depthtexture);
		glUniform1i(shader.getUniformLocation("depthbuffer"), 6);

		glActiveTexture(GL_TEXTURE0 + 7);
		glBindTexture(GL_TEXTURE_2D, shadowmap.textureID);
		glUniform1i(shader.getUniformLocation("shadowbuffer"), 7);
		//glUniformMatrix4fv(shader.program->getUniformLocation("shadowTransform"), 1, GL_FALSE, glm::value_ptr(glm::transepose(shadowmap.transform));
		glUniformMatrix4fv(shader.program->getUniformLocation("shadowTransform"), 1, GL_FALSE, glm::value_ptr(shadowmap.transform));

		p.setTexture(noise, "noisetex");

		const int particle_count = 15;
		std::array<Smoke, particle_count> particles;
		glm::mat4x4 camview = scene->camera.getView();
		//glm::vec3 emit_pos = scene->getInstance("_empty.emit")->getPosition();

		for (int i = 0; i < (int)particles.size() ; i++) {
			Smoke* s = &particles[i];
			s->id = i;
			s->pos = emit_pos + glm::vec3(cos(i) * 3.0f, sin(i*1.5f) * 2.0f, sin(i*0.81f) * 3.0f);
			s->pos *= 1.0f;
			s->viewpos = glm::vec3(camview * glm::vec4(s->pos, 1.0f));	// transform to view space
		}

		std::sort(particles.begin(), particles.end(), [](Smoke& a, Smoke& b) {
			return a.viewpos.z > b.viewpos.z;
		});

		stack.loadIdentity();

		glDisable(GL_CULL_FACE);

		for (auto smoke : particles) {
			stack.pushCurrent();
			float angle = smoke.id * 2.5f * 360.0f;
			*stack.top() = glm::translate(*stack.top(), glm::vec3(smoke.pos));
			*stack.top() = glm::rotate(*stack.top(), angle, vec3(0.0f, 1.0f, 0.0f));
			*stack.top() = glm::scale(*stack.top(), glm::vec3(spread));
			p.modelmatrix = *stack.top();
			//p.updateUniforms(cam, scene->lights);
			
			p.recalculateMatrices(cam);
			p.updateMatrixUniforms(&shader, cam);
			p.draw(quad);
			stack.pop();
		}

		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE);
}
};	// anonymous namespace to make helper functions static
