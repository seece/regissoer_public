
#include "skybox.h"
#include <gl/gleks.h>
#include "effect_helpers.h"
#include <Assets.h>

using glm::vec3;
using std::string;

void render_skybox(EffectInput in, Scene* scene, vec3 light_dir, vec3 light_col, vec3 dark_col) {
	glDepthMask(GL_FALSE);

	Shader& skybox_shader = assets::shaders["skybox"];
	Pipeline& p = scene->pipeline;
	skybox_shader.program->use();
	p.setShader(&skybox_shader);
	p.updateUniforms(scene->camera, scene->lights);
	glUniform3f(skybox_shader.program->getUniformLocation("light_direction"), light_dir.x, light_dir.y, light_dir.z);
	glUniform3f(skybox_shader.program->getUniformLocation("light_color"), light_col.x, light_col.y, light_col.z);
	glUniform3f(skybox_shader.program->getUniformLocation("dark_color"), dark_col.x, dark_col.y, dark_col.z);
	assets::meshes["quad"].draw();

	glDepthMask(GL_TRUE);
}
