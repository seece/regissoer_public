@echo off
setlocal
set songurl=https://dl.dropboxusercontent.com/u/5753422/music/modules/annanet/mother_to_mother.it

pushd %~dp0
echo Downloading demo song

set buildtools=..\buildtools
set wgetexe=%buildtools%\tools\wget.exe
call %buildtools%\download_tools.bat

%wgetexe% --no-clobber -P data %songurl% 
popd
