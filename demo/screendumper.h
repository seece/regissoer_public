#pragma once

#include <string>

int dumpScreenshot(int width, int height, const std::string& path, bool save_gamut = true);
