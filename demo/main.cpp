#ifdef _DEBUG_MEMORY
#include <vld.h>
#endif
#include <cstdio>
#include <iostream>
#include <cassert>
#include <conio.h>

#include <string>
#include <vector>
#include <memory>

#include "GL/gleks.h"
#include <Windows.h>

#include "window.h"
#include "demo.h"
#include "demoplayer.h"
#include "debugplayer.h"
#include "releaseplayer.h"
#include "demo.h"
#include "song.h"

#include <AssetMap.h>
#include <Assets.h>

#include <shader.h>
#include <system/system.h>
#include <system/filesystem.h>
#include <window.h>
#include <tool/Tool.h>
#include <extra/eks_debug.h>


#include "launcher.h"

int main(int argc, char * argv[]) 
{
	// In Release mode we hide the console window.
	#ifdef _RELEASE
	//ShowWindow( GetConsoleWindow(), SW_HIDE );
	#endif

	eks::logger = std::unique_ptr<LogSink>(new LogSink("log.txt"));
	LOG_INFO("Logger object created.");

	eks::launcher::launcherPointer = launch;

	LOG_INFO("Launcher set, starting.");

	fsys::setAssetSource(fsys::SOURCE_DIR, "data/");
	eks::system::initializeSystem();

	Window::Settings settings;
	settings.width = 1280;
	settings.height = 720;
	settings.fullscreen = false;
	settings.vsync = false;
	settings.title = "REGISSoeoeR III";

	//eks::launcher::inquireLaunchSettings("kakka");
	Window window(settings);
	Song song("mother_to_mother.it", 115.0, Song::SONG_TRACKER);

	// Player must be instantiated before any shaders.
	DebugPlayer player(window);
	player.clearParameters();
	player.addParameter("effect");
	player.addParameter("camx");
	player.addParameter("camy");
	player.addParameter("camz");

	Shader::shaderCache.initialize();

	assets::textures.loadAssets();
	assets::meshes.loadAssets();
	EKS_MEASURE(
		assets::shaders.loadAssets();
	);

	// TODO load materials here

	Demo demo(window, song);
	auto scenes = demo.getSceneMap();
	// TODO load scenes here

	Tool tool(window, demo, player);

	player.loadDemo(&demo);
	player.play();

	while (window.processEvents()) 
	{
		double row = song.getRow();
		player.updateSyncTime(row);
		player.updateSyncParams(row, player.getParameterMap());

		window.draw();
	}

	return 0;
}
